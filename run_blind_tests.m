import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin
import matlab.unittest.plugins.codecoverage.CoberturaFormat

cfg = [];
cfg.package.plus_slurm = true;

obob_init_ft(cfg);

coverageFile = fullfile(pwd, 'coverage.xml');
%suite = TestSuite.fromFile('tests/unittest/tests/testOBOB_new_svs.m');
suite = TestSuite.fromFolder('tests/unittest/tests/');

runner = TestRunner.withTextOutput;
runner.addPlugin(CodeCoveragePlugin.forFolder(fullfile(pwd, 'packages'), 'IncludingSubfolders', true, 'Producing', CoberturaFormat(coverageFile)));

res = runner.run(suite);

if all([res.Passed])
  exit(0);
else
  exit(1);
end %if

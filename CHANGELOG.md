# Changelog
All notable changes to this project will be documented in this file.

## 2018-09-26

### Changed

- svs package is always imported
- computing virtual sensors is now a two-part process. Filters must be calculated with obob_svs_compute_spat_filters and then applied with obob_svs_beamtrials_lcmv.

### Removed

- Removed obob_svs_beamtrials_dics
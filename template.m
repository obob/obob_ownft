function rval = obob_myfunction(cfg, data)

% OBOB_MYFUNCTION is an example function to provide a template for the
% header of functions to be included in obob_ownft.
%
% This is the more detailed description of what the function does. It may
% be quite lenghty, but please try to keep it concise and to the point.
%
% Use as
%   rval = obob_myfunction(cfg, data)
%
% where the first input argument is a configuration structure (see below)
% and the second argument is a timefrequency/timelocked/... data structure.
%
% The configuration structure can contain:
%   cfg.xyz        = Description of cfg option 'xyz'. (default = 'abc'). If
%                    no default is provided, please mention if providing
%                    this option is mandatory.
%
%   cfg.abc        = A second cfg.option.

% Copyright (c) 2010-2016, Your Name(s) here
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

rval = data;

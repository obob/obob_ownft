function cfg = obob_init_ft_function(cfg, tests)
% This function does the actual initialization work. Do not call it
% directly.

% Copyright (c) 2013-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% declare global variables...
global cluster_home_local cluster_home_remote fieldtrip_path obob_cfg

% add private functions...
private_dir = fullfile(fileparts(fileparts(mfilename('fullpath'))), 'private_init');
addpath(private_dir);

% declare packages that do not have to be declared...
packages_forced = {'headmodel', 'plot', 'templates', 'utilities', 'atlas', 'statistics', 'svs'};

% determine where we are...
obob_ft_path = fileparts(fileparts(mfilename('fullpath')));
obob_pkg_path = fullfile(obob_ft_path, 'packages');
obob_unittest_path = fullfile(obob_ft_path, 'tests', 'unittest', 'tests');
home_folder = fullfile('/home', getusername());
testdata_version = '0.08';

% if called without arguments, we assume the default...
if nargin == 0
  cfg = [];
end %if

% check whether the function was called with a string...
if ischar(cfg)  
  if strcmp(cfg, 'test')
    old_warning = warning;
    warning off;
    if isempty(tests)
      tests = obob_unittest_path;
    else
      tests = fullfile(obob_unittest_path, tests);
    end %if
    if license('test', 'Distrib_Computing_Toolbox')
      cd(obob_unittest_path)
      results = runtests(tests, 'UseParallel',false);
    else
      cd(obob_unittest_path)
      results = runtests(tests);
    end %if
    warning(old_warning);
    assignin('base', 'test_results', results);
    return;
  end %if
end %if

% show verbose output? (default: true)
cfg.verbose=ft_getopt(cfg, 'verbose', true);

% check where we are
cfg = obob_getLocation(cfg, ~cfg.verbose);

% set default testfolder
default_testfolder = getenv('OBOB_TESTFOLDER');
if isempty(default_testfolder)
  default_testfolder = fullfile(home_folder, 'obob_testdata');
end %if

% check cfg structure and set defaults if necessary...
cfg.package = ft_getopt(cfg, 'package', []);
cfg.package.gm2 = ft_getopt(cfg.package, 'gm2', false);
cfg.package.unimap = ft_getopt(cfg.package, 'unimap', false);
cfg.package.obsolete = ft_getopt(cfg.package, 'obsolete', false);
cfg.package.svs = ft_getopt(cfg.package, 'svs', false);
cfg.package.hnc_invincible = ft_getopt(cfg.package, 'hnc_invincible', false);
cfg.no_override = ft_getopt(cfg, 'no_override', false);
cfg.experimental = ft_getopt(cfg, 'experimental', false);
cfg.ft_path = ft_getopt(cfg, 'ft_path', fullfile(obob_ft_path, 'external', 'fieldtrip'));
cfg.bct_path = ft_getopt(cfg, 'bct_path', fullfile(obob_ft_path, 'external', 'bct'));
cfg.dont_restore_path = ft_getopt(cfg, 'dont_restore_path', false);
cfg.testdata_folder = ft_getopt(cfg, 'testdata_folder', default_testfolder);

obob_cfg = cfg;

if cfg.verbose
  log=@fprintf;

  % print welcome message...
  obob_welcome();
else
  % do not show output
  log=@(varargin)[];
end

% reset the path...
if cfg.dont_restore_path && cfg.location.outside
  warn_no_restore_path();
else
  restoredefaultpath
end %if

% readd private init dir...
addpath(private_dir);

% check whether we want to redirect the temporary folder...
newtmp = tempdir;

if cfg.location.matserver
  newtmp = fullfile('/home', getusername(), 'temp', 'matlab');
elseif cfg.location.sbg_commodity
  if strcmp(getusername(), 'unknown')
    newtmp = '/mnt/obob/tmp';
  else
    newtmp = fullfile('/home', getusername(), 'temp', 'matlab');
  end %if
end %if

log('I am setting the temporary folder to %s.\n\n', newtmp);
if ~exist(newtmp)
  mkdir(newtmp);
end %if
setenv('TMP', newtmp);
setenv('TEMP', newtmp);
clear tempdir


% clearing tmp
if cfg.location.matserver || cfg.location.hnc_head || cfg.location.hnc_compute
  obob_clear_tmp;
end %if

% initialize fieldtrip...
addpath(obob_cfg.ft_path);
clear ft_defaults
ft_defaults

% add brain connectivity toolbox...
addpath(obob_cfg.bct_path);

% compile final package list...
packages = packages_forced;
cfg_packages = fieldnames(obob_cfg.package);

if ~obob_cfg.no_override
  packages{end+1} = 'ft_override';
end %if

for i = 1:length(cfg_packages)
  if obob_cfg.package.(cfg_packages{i})
    packages{end+1} = cfg_packages{i};
  end %if
end %for

if obob_cfg.location.hnc_head || obob_cfg.location.hnc_compute
  packages{end+1} = 'hnc_invincible';
end %if

% add packages...
for i = 1:length(packages)
  log('Adding package %s.\n', packages{i});
  addpath(fullfile(obob_pkg_path, packages{i}));
  if cfg.experimental && exist(fullfile(obob_pkg_path, packages{i}, 'experimental'), 'dir')
    log('Adding experimental functions as well...\n');
    addpath(fullfile(obob_pkg_path, packages{i}, 'experimental'));
  end %if
  
  if strcmp(packages{i}, 'hnc_condor')
    addpath(fullfile(obob_pkg_path, packages{i}, 'obob_condor_checkpoint_folder'));
  end %if
  
end %for

% finally add the root directory...
addpath(obob_ft_path);

obob_cfg.testdata_version = testdata_version;

cfg = obob_cfg;
log('obob_ownft succesfully initialized. Have fun!\n');

% remove private path...
if ~isempty(which('obob_get_owner'))
  rmpath(private_dir);
end %if
end

function warn_no_restore_path()
warntext = ['\n\n' ...
            '##########################################################\n' ...
            '##########################################################\n\n' ...
            'WARNING: YOU SHOULD NOT DO THIS!!!!\n\n' ...
            'You have used the option:\n' ...
            'cfg.dont_restore_path = true;\n' ...
            'This is extremely dangerous and you absolutly should not use it!\n\n' ...
            'Please delete this line from you configuration option for obob_init_ft now!\n\n' ...
            'If you still want to use this, please be advised that:\n' ...
            '1. You results might be wrong and you might not notice it.\n' ...
            '2. You will get absolutely no support by the OBOB team.\n' ...
            '##########################################################\n' ...
            '##########################################################\n\n'];

fprintf(warntext);
end %function


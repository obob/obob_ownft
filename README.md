# The obob_ownft distribution
Welcome to the obob_ownft distribution.

## What is the obob_ownft distribution?
The obob_ownft distribution is a collection of toolboxes and functions used to analyze MEG and EEG data by the [OBOB Lab](http://www.oboblab.at/) at the [University of Salzburg](https://www.uni-salzburg.at/). The OBOB Lab is lead by Prof. Nathan Weisz and is part of the department of [Physiological Psychology](https://www.uni-salzburg.at/index.php?id=29506). We created this distribution to offer a standardized set of toolboxes and functions to be used by our group and other groups within our department. The OBOB Lab also offers support for other researchers within the department who want to analyze MEG and EEG data.

## What is included in the distribution?
1. A recent version of [FieldTrip](http://www.fieldtriptoolbox.org/).
2. A recent version of the [Brain Connectivity Toolbox](https://sites.google.com/site/bctnet/).
3. A collection of functions created by members of our group and our collaborators.

## Can I use obob_ownft?
Yes, of course. The code in the distribution is published under open source licenses like the GPL2 and GPL3. You are free to use, modify and redistribute the code as long as you follow these licenses.

## Ok, how do I download it?
As downloading seems a bit more complicated for novice users, you can find detailed instructions [here](https://gitlab.com/obob/obob_ownft/wikis/Downloading%20obob_ownft).

## How do I use it?
You can find detailed instructions on the [wiki](https://gitlab.com/obob/obob_ownft/wikis/home).

## How can I receive the latest news on obob_ownft?
You can join the [obob_ownft Announcement Group](https://groups.google.com/d/forum/obob_ownft-announcements). Don't worry: traffic will be super low there as only really important announcements are sent over the list.

## Do you offer support if I work for the department of Physiological Psychology of the Universität Salzburg?
Yes, we do.

## Do you offer support if I work somewhere else?
Unfortunately, we cannot offer support outside of the department because of our limited resources. However, you are of course welcome to take a look at the distribution's [wiki](https://gitlab.com/obob/obob_ownft/wikis/home) where we offer tutorials and documentation.

## Can I contribute?
Of course! This is a collection of open source code so contributions are highly valued! However, if you want to contribute to the external toolboxes, it would be a better idea to go there directly. If you use one of our own functions and you see potential for improvement, we are happy to hear from you!

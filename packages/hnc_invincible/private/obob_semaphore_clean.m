function obob_semaphore_clean()
% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

obob_semaphore_cfg;

if ~needs_sem
  return
end %if

all_files = dir(fullfile(sem_folder, '*'));

all_ids = obob_getrunningjobs();

for i = 3:length(all_files)
  if ~any(all_ids == str2num(all_files(i).name))
    delete(fullfile(sem_folder, all_files(i).name));
  end %if
end %for


end


function obob_submit_function(cfg, f_name, varargin)
% obob_submit_function Submits a function for execution on the
% cluster.
%
% WARNING: This function can only be used if you are logged in to the HNC
% Invincible. If you call it anywhere else (Trentobomber, LNIF cluster,
% your computer) it will fail!
%
% Call this function as:
%     obob_submit_function(cfg, f_name, param1, param2, ..., paramN);
%
% f_name is the name of the function as a string. Please only provide the
% function name here without anything added.
%
% Everything after the function name is optional and represents the input
% values for the function. You can provide as many input parameters as you
% want. However, there are some restrictions to what you can provide as
% input parameters:
%
%   1. Only strings, numbers and matrices are accepted. Do not try to use
%      fieltrip structures or anything else. It will not work!
%   2. The arguments should be VERY SHORT!!! They will be converted to
%      strings for the function call.
%
% cfg is the connfiguration structure (FieldTrip like) featuring the
% following options:
%
%   cfg.mem                   = The amount of RAM required by the job. Try
%                               to be as precise as possible here. If your
%                               job asks for more RAM than you specified
%                               here, the scheduler will kill your job. If
%                               you ask for too much RAM, the execution of
%                               your job will be delayed. You can specify
%                               the amount of RAM with either a number
%                               (e.g., cfg.mem = 1024*1024*1024 asks for
%                               1G) or with a string (e.g., cfg.mem = '1G'
%                               does exactly the same). (default = '2G')
%
%   cfg.name                  = The name of the job. It is highly
%                               recommended to group similar jobs by name.
%                               This will help you organize your jobs.
%                               (default = 'MatlabJob')
%
%   cfg.tryout                = If this is set to true, the job will have a
%                               very high priority. However, only one
%                               tryout job per user can run at the same
%                               time. Please use this sparsely and do not
%                               abuse it! (default = false)
%
%   cfg.longjob               = Normally, jobs are killed after 2h of CPU
%                               time (refer to the wiki if you do not know
%                               the difference between wall time and CPU
%                               time.). If this is set to true, the job may
%                               run without limits. However, the job will
%                               get a lower priority and not all slots can
%                               be used by it. (default = false)
%
%   cfg.logdir                = Specifiy the folder to put the logfile in.
%                               This is extremely handy if you submit loads
%                               of jobs because otherwise they clutter the
%                               folder you submitted the jobs from.
%                               (default = 'logs')
%
%   cfg.java                  = If you need java functions, set this to
%                               true. This will steal ca. 4GB of RAM for
%                               each job you submit. Normally you do not
%                               need it. (default = false)
%
%   cfg.options               = In case you want to add any more command
%                               line options to the qsub command. Normally
%                               this is not needed.
%
%   cfg.local                 = If this is set to true, the computations
%                               are not performed on the cluster but
%                               locally. This defaults to false on the HNC
%                               Invincible and true otherwise.
%
% Example 1:
% Let's suppose, you have a function called "myfunction" that takes no
% arguments, is satisfied with less than 10G of memory and runs less then
% 2h. Then you could call it like this:
%
%         obob_submit_function([], 'myfunction');
%
% Example 2:
% Let's suppose, you have a function called "myfunction_big" that takes
% three arguments, needs 20G of RAM, you know it runs more than 2h and you
% want to give it a fancy name. In order to, e.g., submit the function call
% myfunction_big('subject01', 10, [3 6]); to the cluster, here is what you
% would have to do:
%
%         cfg = [];
%         cfg.name = 'MyBigFunction';
%         cfg.mem = '20G';
%         cfg.longjob = true;
%
%         obob_submit_function(cfg, 'myfunction_big', 'subject01', 10, [3 6]);
%
% The job is then submitted to the cluster and the job ID appears in the
% matlab window. You can then close matlab and your session and come back
% later....

% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

switches = {};
switches{1} = '-cwd';
switches{2} = '-j y';
switches{3} = '-S /bin/bash';
switches{4} = '-b y';

%matlabcmd = '"/usr/bin/time -v matlab -nodesktop -r';

cfg.java = ft_getopt(cfg, 'java', false);
cfg.tryout = ft_getopt(cfg, 'tryout', false);
cfg.longjob = ft_getopt(cfg, 'longjob', false);
cfg.mem = ft_getopt(cfg, 'mem', '2G');
cfg.logdir = ft_getopt(cfg, 'logdir', 'logs');
cfg.name = ft_getopt(cfg, 'name', 'MatlabJob');

loc = obob_getLocation([], true);
oncluster = loc.location.hnc_head | loc.location.hnc_compute;

cfg.local = ft_getopt(cfg, 'local', ~oncluster);

if cfg.local
  fun = str2func(f_name);
  fun(varargin{:});
  return;
end %if

if isfield(cfg, 'options')
  if ~iscell(cfg.options)
    cfg.options = {cfg.options};
  end %if
  for i = 1:length(cfg.options)
    switches{end+1} = cfg.options{i};
  end %for
end %if

if isnumeric(cfg.mem)
  cfg.mem = num2str(cfg.mem);
end %if

if cfg.tryout && cfg.longjob
  error('cfg.tryout and cfg.longjob cannot be both true!');
end %if

matlabcmd = '"matlab -nodesktop';
if ~cfg.java
  matlabcmd = [matlabcmd ' -nojvm'];
end %if

matlabcmd = [matlabcmd ' -r'];

if cfg.tryout
  switches{end+1} = '-l tryout';
  switches{end+1} = '-p 0';
elseif cfg.longjob
  switches{end+1} = '-l longjob';
end %if

if mem2bytes(cfg.mem) >= mem2bytes('40G')
  switches{end+1} = '-R y';
end %if

switches{end+1} = ['-l h_vmem=' cfg.mem];
switches{end+1} = ['-N ' cfg.name];

if ~isempty(cfg.logdir)
  if ~exist(cfg.logdir)
    mkdir(cfg.logdir);
  end %if
  switches{end+1} = ['-o ' cfg.logdir];
end %if

% check whether the function is in the current path...
f_path = fileparts(which(f_name));
add_f_path = false;
if ~strcmp(f_path, pwd)
  add_f_path = true;
end %if

% assemble qsub command....
cmd = 'qsub';

for i = 1:length(switches)
  cmd = [cmd ' ' switches{i}];
end %for

cmd = [cmd ' ' matlabcmd];

cmd = [cmd ' \"try;'];

if add_f_path
  cmd = [cmd 'addpath(''' f_path '''); '];
end %if

cmd = [cmd f_name '('];

for i=1:length(varargin)
  arg = varargin{i};
  if isnumeric(arg)
    cmd = [cmd mat2str(arg)];
  else
    cmd = [cmd '''' arg ''''];
  end %if
  
  if i < length(varargin)
    cmd = [cmd ', '];
  end %if
end %for

cmd = [cmd ');catch err;disp(err.getReport());end;addpath(''/mnt/storage/erc-win2con/cluster_stuff'');obob_hnc_printMaxMem();exit;\""'];

system(cmd);
end


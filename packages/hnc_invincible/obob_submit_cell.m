function obob_submit_cell(cfg, f_name, varargin )
% obob_submit_cell Submits multiple jobs for execution on the cluster using
% any possible combinations of the parameters.
%
% WARNING: This function can only be used if you are logged in to the HNC
% Invincible. If you call it anywhere else (Trentobomber, LNIF cluster,
% your computer) it will fail!
%
% Call this function as:
%     obob_submit_cell(cfg, f_name, param1, param2, ..., paramN);
%
% cfg is the configuration structure. See obob_submit_function for
% available options.
% f_name is the name of the function as a string.
% 
% Everything after the function name are cell arrays of parameters.
% 
% This function is a wrapper around obob_submit_function. The beautiful
% thing about this function is that the param parts are cells and a job is
% created for every possible combination of these parameters.
%
% For Example:
% Lets suppose you want to do a time-frequency analysis on all your
% subjects and 2 different frequency bands. You have a function (calc_tf)
% that takes two arguments:
%   1. The subject ID
%   2. The frequency band
%
% So, for one subject with the ID 'th080980' that you want to analyze
% between 4Hz - 20Hz, you would call the function like this:
%     calc_tf('th080980', [4 20]);
%
% You already know that in order to submit this to the cluster, you would
% have to do:
%     obob_submit_function(cfg, 'calc_tf', 'th080980', [4 20]);
%
% But lets say, we have 6 subjects and 2 frequency bands. We could
% manually repeat the above call to obob_submit_function 12 times and
% would have to take a lot of care if something changes here. Or we could
% do the following with obob_submit_cell:
%     subjects = {'aa123', 'bb345', 'cc567', 'dd9876', 'ee2345', 'ff4566'};
%     freqs = {[4 20], [20 60]};
%
%     obob_submit_cell(cfg, 'calc_tf', subjects, freqs);
%
% This would submit all possible combinations of the two parameters.
%
% Of course, you can also do it with 3, 5, 10, 100 parameters!

% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

args = obob_cellargs(varargin{:});

for i = 1:length(args)
  obob_submit_function(cfg, f_name, args{i}{:});
end %if

end


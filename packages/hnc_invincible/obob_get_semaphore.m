function obob_get_semaphore()
% obob_get_semaphore Try to acquire the system wide semaphore to do heavy
% io operations on the storages.
%
% Call this function as:
%           obob_get_semaphore();
%
% And use the obob_release_semaphore function when the operation has
% finished.
%
% If the function cannot acquire the semaphore it will pause until it can
% acquire one. Please remember that there are no queues and that the
% semaphore is shared amongst all users. Which job gets the next one that
% is free is entirly up to chance!

% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

obob_semaphore_cfg;
obob_semaphore_clean;

if ~needs_sem
  return
end %if

while length(dir(fullfile(sem_folder, '*')))-2 > max_sems
  waitsecs_jittered = waitsecs + (-wait_jitter + (2*wait_jitter)*rand);
  
  fprintf('Could not get semaphore.... Trying again in %fs\n', waitsecs_jittered);
  pause(waitsecs_jittered);
  obob_semaphore_clean;
end %while

fprintf('Acquiring semaphore...\n');

system(['touch ' fullfile(sem_folder, own_id)]);

end


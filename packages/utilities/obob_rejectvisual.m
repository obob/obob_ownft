function data = obob_rejectvisual(cfg, data)
% obob_rejectvisual speeds up ft_rejectvisual by first preprocessing the data and then
% calling ft_rejectvisual. Preprocessing is only used for visualization!
% In addition, you can also add an option to downsample the data
% before doing the preprocessing. To do this add the parameter cfg.downsample to
% the cfg structure (i.e. cfg.downsample = 200 will downsample to 200Hz).
% Use like ft_rejectvisual...

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% first extract preproc options...
if isfield(cfg, 'preproc')
  preproc = cfg.preproc;
  cfg = rmfield(cfg, 'preproc');
else
  preproc = [];
end %if

%% then look for downsample option...
if isfield(cfg, 'downsample')
  downsample = cfg.downsample;
  cfg = rmfield(cfg, 'downsample');
else
  downsample = [];
end %if

%% check for sampleinfo field. if not present, create one...
if ~isfield(data, 'sampleinfo')
  no_sinfo = true;
  data.sampleinfo = [1:length(data.trial); 1:length(data.trial)]';
else
  no_sinfo = false;
end %if

%% add column to trialinfo field to identify trials later...
if ~isfield(data, 'trialinfo')
  data.trialinfo = [];
end %if

data.trialinfo(:, end+1) = 1:length(data.trial);

%% check if we have to do downsampling...
if isempty(downsample)
  tempdata = data;
else
  cfg_tmp = [];
  cfg_tmp.resamplefs = downsample;
  cfg_tmp.detrend = 'no';
  
  tempdata = ft_resampledata(cfg_tmp, data);
end %if

%% check if we have to preproc the data and do if necessary...
if ~isempty(preproc)
  tempdata = ft_preprocessing(preproc, tempdata);
end %if

%% create dummy sampleinfo to identify trials later...
%tempdata.sampleinfo(:, 3) = 1:length(data.trial);

%% now run ft_rejectvisual...
tempdata = ft_rejectvisual(cfg, tempdata);

%% find removed channels....
label_index = match_str(data.label, tempdata.label);

data.label = tempdata.label;
for i=1:length(data.trial)
  data.trial{i} = data.trial{i}(label_index, :);
end %for

%% then find and remove trials...
trial_index = zeros(length(tempdata.trial), 1);
for i=1:length(tempdata.trial)
  trial_index(i) = find(tempdata.trialinfo(i, end) == data.trialinfo(:, end));
end %for

badtrials = 1:length(data.trial);
badtrials(trial_index) = [];

data.trial(badtrials) = [];
data.time(badtrials) = [];
if no_sinfo
  data = rmfield(data, 'sampleinfo');
else
  data.sampleinfo(badtrials, :) = [];
end %if
if isfield(data, 'trialinfo')
  data.trialinfo(badtrials, :) = [];
end %if

if size(data.trialinfo, 2) == 1
  data = rmfield(data, 'trialinfo');
else
  data.trialinfo(:, end) = [];
end %if

%% done...

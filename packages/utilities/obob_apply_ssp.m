function datassped = obob_apply_ssp(cfg,data)

% OBOB_APPLY_SSP, as the name suggests, projects out the (usually) noisy
% SSP vectors from the data.
%
% SSP projectors are created once per year by the Elekta technicians, on
% empty room data, and usually contain typical noise information about the
% MEG system and the shielded room without any brain in (so called
% exogenous noise), i.e. mainly 50 Hz and harmonics, 16 and 2/3 train
% line noise, room vibrations etc ... 
% 
% Use as
%   datassped = obob_apply_ssp(cfg,data)
%
% where the first input argument is a configuration structure (see below)
% and the second argument is a either raw or timelock  data structure.
%
% The configuration structure can contain:
%   cfg.inputfile    = A .fif filename, with the minimum requirement of
%                      having been acquired during the same (maintenance)
%                      year of the data that I want to clean. Mandatory
%
%   data             = raw or timelock data, to be cleaned. Mandatory
%
% 
% Ref (e.g.):
% Uusitalo MA, Ilmoniemi RJ.
% Signal-space projection method. 
% Med. Biol. Eng. 1997, 32: 35-42. 
%
% Copyright (c) 2016, Gianpaolo Demarchi
%
% 20161213: initial implementation
%
% TODO: * make it more robust wrt to the input, i.e. now only MEG data are
%         allowed, would be nice to select it. And only 306 by now. 
%       * make the external input file optional, digging deeper in the
%         data structure (.previous.previous)  to find the projs
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and

%% some init stuff

ft_defaults;
ft_preamble provenance
ft_preamble trackconfig

ft_hastoolbox('mne', 1)

ft_checkconfig(cfg,'required','inputfile');

persistent fileName
persistent hdr

%% read projs from the external file

if ~strcmp(fileName, cfg.inputfile) || isempty(hdr)
  fileName = cfg.inputfile;
  hdr = ft_read_header(fileName);

  %activate the projs
  for j=1:size(hdr.orig.projs,2) % in Salzburg usually 13
      hdr.orig.projs(j).active = 'true';
  end
end %if

%% take only MEG channels...
cfg = [];
cfg.channel = 'MEG*';

data_meg = ft_selectdata(cfg, data);

%% take only non MEG channels
if ~isequal(data.label, data_meg.label)
  cfg = [];
  cfg.channel = {'all', '-MEG*'};

  data_non_meg = ft_selectdata(cfg, data);
else
  data_non_meg = [];
end %if

%% see the help of mne_whatever to check how it works.

[ megproj, nproj,U] = mne_make_projector(hdr.orig.projs,data_meg.label,hdr.orig.bads);

%% apply, i.e. multiply, the ssp to all the data

datatossp = data_meg;
if isfield(datatossp,'avg') % timelock data   
    datatossp.avg = megproj*datatossp.avg;  
elseif isfield(datatossp,'trial') %single trials
    for iTrial=1:length(datatossp.trial)
        datatossp.trial{iTrial} = megproj*datatossp.trial{iTrial};
    end
end

%% reappend data....
if ~isempty(data_non_meg)
  cfg = [];
  cfg.appenddim = 'chan';

  datassped = ft_appenddata(cfg, datatossp, data_non_meg);
else
  datassped = datatossp;
end %if

%% redo timelockanalysis if necessary...
if isfield(datatossp,'avg')
  datassped = ft_timelockanalysis([], datassped);
  datassped.dof = datatossp.dof;
  datassped.var = datatossp.var;
end %if

%% check the order of the channels 

%% for testing 
% hold all
% plot(datatest.trial{1}(100,:))
% plot(datatestssp.trial{1}(100,:))

% datatossp = omission_random_avg;
% datatossp = omission_midminus_avg
% datatossp = omission_midplus_avg
% datatossp = omission_ordered_avg;
% 
% 
% omission_random_avg_ssp =  datatossp;
% omission_midminus_avg_ssp =  datatossp;
% omission_midplus_avg_ssp =  datatossp;
% omission_ordered_avg_ssp =  datatossp;
function [cfg] = obob_getLocation(cfg, quiet)
% CIMEC_GETLOCATION tries to figure out where the function was called from
% and adjusts cfg accordingly. This is a helper function for some internal
% functions.

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

if nargin < 2
  quiet = false;
end %if

% list all possible locations
locations = {'hnc_head', 'hnc_compute', 'matserver', 'matserver2', 'lnif_cluster_head', 'lnif_cluster_node', 'outside', 'sbg_commodity'};

% see whether the location should be faked...
if isfield(cfg, 'location')
  if isfield(cfg.location, 'fake') && cfg.location.fake
    warning('Running with a faked location. This should only be done in debug/development scenarios!');
    
    % add those fields to the cfg.location that the user did not provide...
    for i = 1:length(locations)
      if ~isfield(cfg.location, locations{i})
        cfg.location.(locations{i}) = false;
      end %if
    end %for
    
    return;
  end %if
end %if

loc = [];
for i = 1:length(locations)
  loc.(locations{i}) = false;
end %if

loc.hostname = gethostname();

if strcmp(loc.hostname, 'mat-meg-server')
  loc.matserver = true;
elseif strcmp(loc.hostname, 'mat-meg-serv1')
  loc.matserver2 = true;
elseif strcmp(loc.hostname, 'mat-cluster-weisz')
  loc.hnc_head = true;
elseif strncmp(loc.hostname, 'compute', 6)
  % running on a compute node... lets find out to which cluster we
  % belong...
  if exist('/sssd', 'dir')
    loc.hnc_compute = true;
  else
    loc.lnif_cluster_node = true;
  end %if
elseif strncmp(loc.hostname, 'lnif-cluster', 12)
  loc.lnif_cluster_head = true;
elseif strncmp(loc.hostname, 'cf00', 4)
  loc.sbg_commodity = true;
end %if

cfg.location = loc;

if ~quiet
  if loc.matserver
    fprintf('Running on the MEG Analysis Server.\n');
  elseif loc.matserver2
    fprintf('Running on the MEG Analysise Server 2.\n');
  elseif loc.hnc_head
    fprintf('Running on the HNC Invincible Headnode.\n');
  elseif loc.hnc_compute
    fprintf('Running on an HNC Invincible Compute Node.\n');
  elseif loc.lnif_cluster_node
    fprintf('Running on a LNIF Cluster Compute Node.\n');
  elseif loc.lnif_cluster_head
    fprintf('Running on the LNIF Cluster Headnode.\n');
  elseif loc.sbg_commodity
    fprintf('Running on a Salzburg Commodity Computing Machine.\n');
  else
    cfg.location.outside = true;
    fprintf('Cannot determine where I am.\n');
  end %if
end %if

end


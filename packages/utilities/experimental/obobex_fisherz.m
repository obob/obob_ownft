function value = obobex_fisherz(data, inv)
% obobex_fisherz calculates the fisher z transform on a data vector.
% 
% call as:
%       value = obobex_fisherz(data, inv)
%
% data      = data vector of values to be transformed.
% inv       = if set to true, the inverse transform is performed.
%
% Stephan Moratti 2005
% Ported to Matlab by Thomas Hartmann 2006

if nargin == 1
  inv = false;
end %if

if inv
  value = (exp((2*data))-1)./(exp((2*data))+1);
else
  value = 0.5.*(log(1+data)-log(1-data));
end
        



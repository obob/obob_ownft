function [datarepaired] = obob_fixchannels(cfg, data)

% CIMEC_FIXCHANNELS is a wrapper function that tries to get rid of the
% peculiar behavior of our Neuromag machine. Since we can have a high
% number of bad channels all over blocks/subjects/days of experiment, this
% function "wraps" some clever wizardry around FT_CHANNELREPAIR, finding
% the proper list of neighboouring channel to the ones that have to be
% fixed
%
% Use as
%   datarepaired = obob_fixchannels(cfg, data)
%
% there are no mandatory fields for cfg, since it can take out all the
% relevant information from the data.
% Optional arguments can be:
%
% cfg.distance     = distance in cm to be used to computer the neighbours,
%                    if the method 'distance' is chosen (UNUSED)
% cfg.badchannel   = list of bad channels; default is [], and they will be
%                    automagically detected as the missing channels in
%                    the data structure
% cfg.neigh_method = the method for calculating the distance used by
%                    ft_prepare_neighbours, could be either
%                    'distance', 'triangulation' or 'template'
%                    (default = 'distance') (UNUSED)
% cfg.load_default = if set to 1 (or true), the data.grad field is ignored,
%                    instead default channel neighbour values are loaded
%                    (CIMeC NEUROMAG306)
% cfg.reorder      = if set to 1 (or true), sensors are reordered after
%                    interpolation to match default Neuromag order (default = 1)
% cfg.interp       = interpolation method used (default = 'nearest')be
%                    aware that an intact grad stucture is needed for
%                    spline interpolation

% Copyright (c) 2012-2016, Gianpaolo Demarchi, Philipp Ruhnau & Thomas
% Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.


% defaults
if ~isfield(cfg, 'distance'),    cfg.distance    = 4;   end
if ~isfield(cfg, 'badchannel'),     badChannel   = []; else badChannel=cfg.badchannel;  end
if ~isfield(cfg, 'load_default'), cfg.load_default = true; end % added default loading
if ~isfield(cfg, 'reorder'), reorder = 1; else reorder = cfg.reorder; end % default is to reorder channels
if ~isfield(cfg, 'interp'), interp = 'nearest'; else interp = cfg.interp; end

% TODO: test whethter 'triangulation' gives better results than 'distance'

if isfield(cfg, 'method'), cfg.neigh_method = cfg.method; end % for backwards compatibility
if ~isfield(cfg, 'neigh_method'),    cfg.neigh_method    = 'distance';   end

defaultMethod = cfg.neigh_method;

% if there are bad channels defined, use those, otherwise check look for
% them in the list


% NOTE
% actual present channels are in channels data.cfg.channel
% all channels are stored in data.hdr.label


if ~isempty(badChannel)
  goodChannel = 306 - max(size(badChannel));
else
  goodChannel = max(size(data.label));
end %if


allMEGlabel = data.label(1:goodChannel);

% allMEGlabel represents all the really good channels present in the data,
% not the interpolated ones. This should be kept somewhere ... but I don't
% really know how to do the bookkeeping; we could either spit out another
% cfg, or find another way.

% get out the list of all channels, from the header

gradY.label = data.hdr.label(1:3:306);
gradX.label = data.hdr.label(2:3:306);
mag.label = data.hdr.label(3:3:306);

if isfield(data, 'grad') % needed in case no grad field, otherwise aborts, PR
  gradY.tra = data.grad.tra(1:length(gradY.label), :);
  gradX.tra = data.grad.tra(1:length(gradX.label), :);
  mag.tra = data.grad.tra(1:length(mag.label), :);
end %if

% and from there I've to get out my good guys
% intersecting them with the ones that I've actually

[~,loc] = ismember(gradY.label,allMEGlabel);
gradY.goodLabel = gradY.label(find(loc));

[~,loc] = ismember(gradX.label,allMEGlabel);
gradX.goodLabel = gradX.label(find(loc));

[~,loc] = ismember(mag.label,allMEGlabel);
mag.goodLabel = mag.label(find(loc));


if isempty(badChannel)
  % search manually for missing channels
  missingGradY = setdiff(gradY.label,allMEGlabel);
  missingGradX = setdiff(gradX.label,allMEGlabel);
  missingMag = setdiff(mag.label,allMEGlabel);
  
else
  % get them from the list passed
  missingGradY = gradY.label(ismember(gradY.label,badChannel));
  missingGradX = gradX.label(ismember(gradX.label,badChannel));
  missingMag = mag.label(ismember(mag.label,badChannel));
end %if

% take the exact position of the sensor

if isfield(data,'grad') && cfg.load_default ~= 1
  
  % position = data.grad.coilpos(1:102,:); %formerly wrongly chanpos;
  
  % the indexing for coilpos seems to be right again, but as all channel
  % triplets are essentially at the same position, thus, taking only
  % grad1
  position = data.grad.chanpos(1:3:306,:);
  
  
  gradY.chanpos = position; % formerly position(1:102,:);
  gradX.chanpos = position;
  mag.chanpos  = position;
  
  cfg=[];
  cfg.grad=gradY;
  cfg.method=defaultMethod;
  neighbGradY=ft_prepare_neighbours(cfg);
  
  cfg=[];
  cfg.grad=gradX;
  cfg.method=defaultMethod;
  neighbGradX=ft_prepare_neighbours(cfg);
  
  cfg=[];
  cfg.grad=mag;
  cfg.method=defaultMethod;
  neighbMag=ft_prepare_neighbours(cfg);
  
  isgrad=1;
  
else
  disp('') % tell the user that defaults are loaded
  disp('Loading default neighbour values for CIMeC MEG')
  disp('')
  
  load Cimec_MEG_neigh.mat %file should be stored in path
  if ~isfield(data, 'grad')
    data.grad=data.hdr.grad; %needs to be added for ft_channelrepair if not present
    isgrad = 0;
  else
    isgrad = 1; % do not remove original grad
  end % if
end %if


cfg=[];

if isempty(badChannel)
  cfg.missingchannel =[ missingGradY ; missingGradX ; missingMag];
  if isempty(cfg.missingchannel)
    % no channels to repair...
    datarepaired = data;
    return
  end %if
else
  cfg.badchannel =[ missingGradY ; missingGradX ; missingMag];
end %if

cfg.neighbours     = [ neighbGradY neighbGradX neighbMag];  %neighbourhoodstructure, see also FT_PREPARE_NEIGHBOURS
cfg.method = interp;
datarepaired = ft_channelrepair(cfg, data);

% Add another field in datarepaired, to keep track of the originally good
% channels

datarepaired.origLabel = allMEGlabel;

if isgrad==0
  datarepaired=rmfield(datarepaired,'grad');
end %if

% is there still any difference?
% data.hdr.label(find(~ismember(data.hdr.label(1:1:306),datarepaired.label)))

% bring channels in default order if wanted
if reorder == 1
  datarepaired = obob_reorder_channels(datarepaired);
end

end %function

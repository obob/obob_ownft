function [idx, idx_inside] = obob_coord2voxnr(voxpos, grid)

% obob_coord2voxnr finds the index of a given coordinate inside a source grid/struct
%
% useful when you want to find the idex of the maximum or minimum (power/
% stats...) effect identified with ft_sourceplot and want to extract date 
% for that voxel only (time series/time-freq data)
%
% Use as:
%   [idx, idx_inside] = obob_coord2voxnr(voxpos, grid)
%
% input:
% voxpos - vector [x y z] coordinates
% grid   - structure containing .pos field with 3d coordinates of all grid
%          points (if also containing .inside the additional output
%          idx_inside will refer to an inside brain voxel, i.e., if you
%          have a stat or power structure only containing inside points
%          this should be your choice)
%
% output:
%
% idx        - index of closest point to input voxpos in the grid.pos
% idx_inside - index of closest inside point to input voxpos in the 
%              grid.pos, is only computed when the grid has an inside field
%
% Example:
% load mni_grid_1_5_cm_889pnts % template grid with 1.5cm resolution
% voxpos = [-2 1 5.5] % e.g., point with max power effect from ft_sourceplot
% [idx, idx_inside] = obob_coord2voxnr(voxpos, template_grid)
% idx =
%      1237 % index in template_grid.pos
%
% idx_inside =
%       785 % index of idx from above in template_grid.inside

% 20140724 - initial implementation, PR


% get input and get in right format
voxpos = voxpos(:)'; % make sure it's a row
positions = grid.pos;

% subtract voxel positions from all positions
difMat = positions - repmat(voxpos, length(positions), 1);


% find coordinates closest to voxpos by
% 1) squaring the difference (to get positive values)
% 2) sum for each point
% 3) find the minimum in that vector
[minval, idx] = min(sum(difMat.^2, 2));

% check if more than one min value (probably when coordinate given is on
% the border between two
nmin = sum(sum(difMat.^2, 2)==minval);
if nmin > 1
  warning('coord2voxpos:multipleVoxels', ...
    ['There is more than one point closest to your coordinates, probably the point is on the border between two voxels.'...
    '\nConsider using a different point'])
end

% in case it is a grid, look for inside field and find the right voxel 
if isfield(grid, 'inside')
  if islogical(grid.inside) % newer ft versions changed .inside to boolean
    idx_inside = find(find(grid.inside)==idx);
  else % older version had indices
    idx_inside = find(grid.inside==idx);
  end
else
  idx_inside = [];
end

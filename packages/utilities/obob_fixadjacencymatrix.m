function mvnew = obob_fixadjacencymatrix(conn)
% CIMEC_FIXADJACENCYMATRIX adjusts the adjacency matrix that is the output of
% ft_connectivityanalysis by removing double pairs of channels (symmetric connectivity metric)
% and adding the direction on the channel labels when the connectivity metric is directional (-->).
%
% Use this function as:
%  mvnew = obob_fixadjacencymatrix(conn)
%
% conn: connectivity fieldtrip structure.
% mvnew: connectivity structure with one chan dimension eliminated. It can then be an iput in ft_freqstatistics.
%
% If the connectivity metric is bi-directional it gives channel combinations (eg. chan1-->chan2 and chan2-->chan1).
% In case it is symmetrical, it keeps only one pair (chan1--chan2).
% At the moment, the following metrics are supported: pdc, plv and coh.

% Copyright (c) 2013-2016, Chrysa Lithari & Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% initialization
ft_defaults;
ft_preamble provenance

cfg = [];
cfg.labelcmb = {conn.label, conn.label};
[labcmb, labind] = ft_channelcombination('all', conn.label);

% check the spectrum type which determines the combination of channels (directed or no).
% supported directed metrics: pdc
% supported undirected metrics: plv, coh
if isfield(conn, 'cohspctrm')
  labels = labind;
  connchar = '--';
  conn.spctrm = conn.cohspctrm;
elseif isfield(conn, 'plvspctrm')
  labels = labind;
  connchar = '--';
  conn.spctrm = conn.plvspctrm;
elseif isfield(conn, 'pdcspctrm')
  labels = cat(1,labind,fliplr(labind)); % combine bi-directional channel labels
  connchar = '-->';
  conn.spctrm = conn.pdcspctrm;
else
  error('not supported connectivity metric')
end %if

% check for dimension of time
if strcmpi(conn.dimord,'chan_chan_freq_time')
  tmpspec = zeros(size(labels,1),length(conn.freq),length(conn.time));
  tmplab = cell(size(labels,1),1);
  for i = 1:size(labels)
    tmplab(i) = {[cell2mat(conn.label(labels(i,2))) connchar cell2mat(conn.label(labels(i,1)))]};
    tmpspec(i,:,:) = squeeze(conn.spctrm(labels(i,2),labels(i,1),:,:));
  end %for
  dimord = 'chan_freq_time';
elseif strcmpi(conn.dimord,'chan_chan_freq')
  tmpspec = zeros(size(labels,1),length(conn.freq));
  tmplab = cell(size(labels,1),1);
  for i = 1:size(labels)
    tmplab(i) = {[cell2mat(conn.label(labels(i,2))) connchar cell2mat(conn.label(labels(i,1)))]};
    tmpspec(i,:) = squeeze(conn.spctrm(labels(i,2),labels(i,1),:));
  end %for
  dimord = 'chan_freq';
else
  error('dimord has to be chan_chan_freq_time or chan_chan_freq')
end %if
mvnew = conn;
mvnew.label = tmplab;
mvnew.dimord = dimord;

% return the spectrum in correct type
if isfield(mvnew, 'cohspctrm')
  mvnew = rmfield(mvnew, {'cohspctrm' 'spctrm'});
  mvnew.cohspctrm = tmpspec;
elseif isfield(mvnew, 'plvspctrm')
  mvnew = rmfield(mvnew, {'plvspctrm' 'spctrm'});
  mvnew.plvspctrm = tmpspec;
elseif isfield(mvnew, 'pdcspctrm', 'spctrm')
  mvnew = rmfield(mvnew, {'pdcspctrm' 'spctrm'});
  mvnew.cohspctrm = tmpspec;
end %if

% clean up...
ft_postamble provenance




function outdata = obob_apply_to_ft_structs(cfg, data)
% obob_apply_to_ft_structs searches indata for FieldTrip structures. For
% every FieldTrip structure it finds, cfg.function is called.
%
% Call as:
%   outdata = obob_apply_to_ft_structs(cfg, data)
%
% where the first argument is a cfg structure and the second argument is a
% variable the contains one or more FieldTrip structures. These FieldTrip
% structures can be contained in structure fields (like data.visual,
% data.auditory...) and/or in cell arrays (like data.visual{1},
% data.visual{2}). There is no limit to how deep you can nest these
% structures (like: data.source.visual{1}, data.sensor.auditory{3})
%
% The cfg structure takes on mandatory argument:
%   cfg.function     = A handle to the function that should be executed for
%                      each FieldTrip structure that is discovered in data.
%                      The function normally has one parameter, which is
%                      the current FieldTrip structure that needs to be
%                      processed by that function. As it is common for
%                      FieldTrip functions to require a cfg structure
%                      alongside the data argument, the following approach
%                      is recommended:
%                      % create the cfg structure common to all calls to
%                        the function:
%                      cfg_fun = [];
%                      cfg_fun.hpfilter = 'yes';
%                      cfg_fun.hpfreq = 1;
%
%                      % create the cfg structure for obob_apply_to_ft_structs
%                      cfg = [];
%                      cfg.function = @(x) ft_preprocessing(cfg_fun, x);
%
%                      We have now created a so-called "anonymous"
%                      function, i.e., a function that does not have a name
%                      and assigned it to cfg.function. This function takes
%                      one argument 'x' and all it does is call
%                      ft_preprocessing using the 'x' and the cfg_fun
%                      structure that we have created before.
%
% The cfg structure take the following optional arguments:
%    cfg.parallel     = If true, it tries to parallelize using Matlab's
%                       Parallel Computing Toolbox. It will only
%                       parallelize on the local machine and not on the
%                       cluster! Default_ false
%
%    cfg.stop_fun     = Normally, obob_apply_to_ft_struct uses ft_checkdata
%                       in order to determine whether the current data
%                       is of the correct type to call the cfg.function on.
%                       If this is not what you want, you can define your
%                       own function here. It should take exactly one
%                       argument (being the data) and return true if
%                       cfg.function should be called and false otherwise.

% Copyright (c) 2017-2018, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

ft_checkconfig(cfg, 'required', 'function');

cfg.private = ft_getopt(cfg, 'private', {});
cur_fieldnames = ft_getopt(cfg.private, 'cur_fieldnames', {});
stop_fun = ft_getopt(cfg, 'stop_fun', []);
run_parallel = ft_getopt(cfg, 'run_parallel', false);
fun = cfg.function;

if run_parallel
  n_workers = Inf;
else
  n_workers = 0;
end %if

if isempty(stop_fun)
  test_for_stop_fun = @(is_ft_struct, data) is_ft_struct;
else
  test_for_stop_fun = @(is_ft_struct, data) stop_fun(data);
end %if

if isstruct(data) && length(data) > 1
  parfor (i = 1:length(data), n_workers)
    outdata(i) = obob_apply_to_ft_structs(cfg, data(i));
  end %for
  return;
end %if

if iscell(data) && length(data) > 1
  parfor (i = 1:length(data), n_workers)
    outdata{i} = obob_apply_to_ft_structs(cfg, data{i});
  end %for
  return;
end %if


is_ft_struct = false;
try
  tmp = ft_checkdata(data, 'datatype', {'timelock', 'freq', 'raw', 'comp', 'spike', 'source'});
  is_ft_struct = true;
catch
end

if test_for_stop_fun(is_ft_struct, data)
  
  if ~contains(func2str(fun), 'cur_fieldnames')
    outdata = fun(data);
  else
    outdata = fun(data, cur_fieldnames);
  end %if
  return;
end %if

if isempty(data) || is_ft_struct
  outdata = data;
  return;
end %if

if isstruct(data)
  allfields = fieldnames(data);
  for i = 1:length(allfields)
    cur_field = allfields{i};

    these_field_names = [cur_fieldnames; {cur_field}];
    cfg.private.cur_fieldnames = these_field_names;
    
    outdata.(cur_field) = obob_apply_to_ft_structs(cfg, data.(cur_field));
  end %for
end %if

if ~exist('outdata', 'var')
  outdata = data;
end %if

end


function testdata_folder = obob_get_testdata_folder(update)
% obob_get_testdata_folder returns the location of the testdata.
%
% Call as:
%   testdata_folder = obob_get_testdata_folder(update)
%
% If 'update' is true, the latest testdata will be downloaded. The default
% is false.
%
% The default testdata folder will be /home/you_username/obob_testdata.
% This can be overwritten using the testdata_folder cfg option of
% obob_init_ft.
%

% Copyright (c) 2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

global obob_cfg

if nargin < 1
  update = false;
end %if

testdata_folder = obob_cfg.testdata_folder;

if ~exist(testdata_folder, 'dir')
  mkdir(testdata_folder);
end %if

if length(dir(testdata_folder)) <= 2
  warning('Testdata folder is empty. Downloading testdata');
  download_testdata();
end %if

current_version = strtrim(fileread(fullfile(testdata_folder, 'version.txt')));

if ~strcmp(current_version, obob_cfg.testdata_version) && ~update
  warning('The testdata is not up-to-date. Please run: obob_get_testdata_folder(true) to update.')
end %if

if update
  rmdir(testdata_folder, 's');
  mkdir(testdata_folder);
  download_testdata();
end %if

end

function download_testdata()
global obob_cfg

url = sprintf('https://gitlab.com/obob/obob_ownft_testdata/-/archive/v%s/obob_ownft_testdata-v%s.zip', obob_cfg.testdata_version, obob_cfg.testdata_version);
zip_fname = sprintf('%s.zip', tempname);
temp_folder = tempname();

fprintf('Downloading testdata. Please wait...\n');
options = weboptions('Timeout', Inf);
websave(zip_fname, url, options);
fprintf('Extracting files. Please wait...\n');
mkdir(temp_folder);
unzip(zip_fname, temp_folder);
delete(zip_fname);
unzipped_folder = fullfile(temp_folder, sprintf('obob_ownft_testdata-v%s', obob_cfg.testdata_version));
movefile(fullfile(unzipped_folder, '*'), obob_cfg.testdata_folder);
end %function


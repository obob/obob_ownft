function [ data ] = obob_rm_cfg( data )
% obob_rm_cfg removes all cfg structures from the input. This can be
% extremely useful because sometime FieldTrip puts data into the cfg
% structures, making the data structures very big.
%
% Use as:
%     data = obob_rm_cfg(data);

% Copyright (c) 2016-2017, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

if isempty(data)
  return;
end %if

if isstruct(data)
  if isfield(data, 'cfg')
    data = rmfield(data, 'cfg');
  end %if
  
  all_fields = fieldnames(data);
  for i = 1:length(all_fields)
    cur_field = all_fields{i};
    
    if ~isempty(data.(cur_field)) && length(data.(cur_field)) > 1
      for j = 1:length(data.(cur_field))
        data.(cur_field)(j) = obob_rm_cfg(data.(cur_field)(j));
      end %for
    else
      data.(cur_field) = obob_rm_cfg(data.(cur_field));
    end %if
  end %for
end %if


end


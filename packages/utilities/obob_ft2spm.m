function [D] = obob_ft2spm(cfg,data)
% OBOB_FT2SPM converts Fieldtrip data structures to SPM file format
% Run spm_eeg_ft2spm + take care of additional settings necessary to run DCM analysis 
% with SPM12.
%
% Use this function as:
%   D = obob_ft2spm(cfg,data)
%
% INPUTS:
%---------
%  data  = fieldtrip data structure with single trial specification
%
% The configuration has the following options:
%---------------------------------------------
%
%  cfg.spmfilename = string with full path and filename of output SPM matfile (.mat)
%  cfg.senstype    = 'MEG' / 'EEG'  / or 'LFP' (if your data are virtual sensor sources time-course)
%  cfg.condlabels  = cell array of conditions labels (must be same number of trials)
%  cfg.sensfile    = string with full path and filename of file where sensors information are defined
%                    (something that can be read by ft_read_sens and ft_read_headshape - i.e. fiffile or dsfile for MEG)
%  cfg.mrifile     = NifTi format aMRI segmented or not (string with full path and filename of mrifile)
%  cfg.interactivecoreg   = false (default) or true --> automatic or manual coregistration & forward model (0 = take default fiducials and use headshape)
%
% OUTPUT:
%--------
% D  - spm meeg object 
%
%

%% do some initialization...
%----------------------------
ft_checkconfig(cfg,'required',{'spmfilename' 'senstype'});

% set the defaults
%------------------
if numel(cfg.condlabels) ~= numel(data.trial)
  error('cfg.condlabels dimensions dont match with trials number');
end
allowed_senstypes={'MEG','EEG','LFP'};
if isempty(strmatch(cfg.senstype,allowed_senstypes,'exact'))
  error('cfg.senstype must be one of: %s', strjoin(allowed_senstypes,', '))
end

LFP = strcmp(cfg.senstype,'LFP');

condlabels             = ft_getopt(cfg, 'condlabels', num2cell(1:numel(data.trial)));
cfg.mrifile            = ft_getopt(cfg, 'mrifile', []);
cfg.interactivecoreg   = ft_getopt(cfg, 'interactivecoreg', false);

if ~isfield(data,'hdr')
  data.hdr = ft_read_header(cfg.sensfile);% setup field hdr for Elekta compatibility in spm_eeg_ft2spm
end

%% Add Path spm12 (and remove it at the end)
%------------------------------------------
% find current obob_init_ft path
%---------------------------------
basefolder = which('obob_init_ft.m');
basepath   = fileparts(basefolder);

% Set SPM12 paths
%---------------
foldernameSPM = 'spm12';
SPMpath   = [basepath '/packages/headmodel/' foldernameSPM];
SPMpath2  = [SPMpath '/matlabbatch'];
addpath(SPMpath);
addpath(SPMpath2);

cleanupObj = onCleanup(@()restore_env(SPMpath,SPMpath2));

%% Convert to SPM format
%------------------------
D = spm_eeg_ft2spm(data, cfg.spmfilename);

%% Set condition labels
%-------------------------
for c = 1:numel(condlabels)
  D = conditions(D, c, condlabels{c});
end

%% Post-processing
%------------------
if LFP
  % set channels labels to LFP
  %-----------------------------
  D = chantype(D,1:nchannels(D),'LFP');
  
else
  
  % Read sensors and fiducials from the first dataset
  %----------------------------------------------------
  switch cfg.senstype
    case 'EEG'
      
      D = sensors(D, cfg.senstype, ft_convert_units(ft_read_sens(cfg.sensfile,'senstype',cfg.senstype), 'mm'));
      D = fiducials(D, ft_convert_units(ft_read_headshape(cfg.sensfile,'senstype',cfg.senstype), 'mm'));
      
      S = [];
      S.task = 'project3D';
      S.modality = 'EEG';
      S.updatehistory = 1;
      S.D = D;
      D = spm_eeg_prep(S);
      
    case 'MEG'
      S = [];
      S.task = 'loadmegsens';
      S.source = cfg.sensfile;
      S.D = D;
      D = spm_eeg_prep(S);
      
    otherwise
      assert(false,'only EEG or MEG sensors are supported');
  end
end

%% Save the dataset
%--------------------
save(D);

if ~LFP  
  %% Compute the forward model if needed
  %--------------------------------------
  meshres = 3; % mesh resolution: 1 = coarse/ 2 = normal / 3 = fine

  if ~isempty(cfg.mrifile)
    disp('**************');
    disp(['... Read individual aMRI = ' cfg.mrifile]);
    disp('**************');
    
    sMRI = cfg.mrifile; % for cfg.interactivecoreg = true
    matlabbatch{1}.spm.meeg.source.headmodel.meshing.meshes.mri = {cfg.mrifile}; % for cfg.interactivecoreg = false
  else
    disp('**************');
    disp('--> No individual aMRI = Use TEMPLATE');
    disp('**************');
    
    sMRI = 1; % for cfg.interactivecoreg = true
    matlabbatch{1}.spm.meeg.source.headmodel.meshing.meshes.template = 1;% for cfg.interactivecoreg = false
  end
      
  
  if cfg.interactivecoreg
  
     
      % Coregister & Forward model (user interface)
      %--------------------------------------------
      disp('**************');
      disp('--> Use the interface to define Fiducials & Forward model');
      disp('**************');
      
      D = spm_eeg_inv_mesh_ui(D, 1, sMRI, meshres);
      D = spm_eeg_inv_datareg_ui(D, 1);
      D = spm_eeg_inv_forward_ui(D, 1);
      
      save(D);
      
  else
      
      matlabbatch{1}.spm.meeg.source.headmodel.D = {fullfile(D.path, D.fname)};
      matlabbatch{1}.spm.meeg.source.headmodel.val = 1;
      matlabbatch{1}.spm.meeg.source.headmodel.comment = '';      
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(1).fidname = 'Nasion';
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(1).specification.select = 'nas';
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(2).fidname = 'LPA';
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(2).specification.select = 'lpa';
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(3).fidname = 'RPA';
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.fiducial(3).specification.select = 'rpa';
      matlabbatch{1}.spm.meeg.source.headmodel.meshing.meshres = meshres;
      matlabbatch{1}.spm.meeg.source.headmodel.coregistration.coregspecify.useheadshape = 1;
      
      switch cfg.senstype
        case 'EEG'
          matlabbatch{1}.spm.meeg.source.headmodel.forward.eeg = 'EEG BEM';
        case 'MEG'
          matlabbatch{1}.spm.meeg.source.headmodel.forward.meg = 'Single Shell';
      end
      
      %spm('defaults','EEG');
      spm_jobman('run', matlabbatch);
      
      % Plot Coregistration
      %---------------------
      D = spm_eeg_load(fullfile(D.path, D.fname));
      spm_eeg_inv_checkdatareg(D, 1);
  end
  
end

%% Remove path spm12
%---------------------
% clean
%-------
  function restore_env(newpath,newpath2)
    rmpath(newpath);
    rmpath(newpath2);
  end

end



function [ data ] = obob_append_tf( varargin )

% OBOB_APPEND_TF appends time-frequency and frequency data from datasets
% with different time and freq bins.
%
% The function takes an arbitrary number of freq-structures and appends
% them, as long as the number of channels and the number of trials match.
% It is supposed to be used to concatenate data that has been generated in
% different cluster jobs in order to keep jobs small.
%
% Example: You have a call like this:
%
% cfg = [];
% cfg.method = 'mtmconvol';
% cfg.foi = 2:50;
% cfg.toi = -1:.05:1;
% cfg.tapsmofrq = ones(size(cfg.foi))*6;
% cfg.t_ftimwin = ones(size(cfg.foi))*.4;
% cfg.output = 'pow';
%
% data_tf_whole = ft_freqanalysis(cfg, data);
%
% Instead of running this analysis on the whole time-freq spectrum, you can
% calculate it in smaller chunks:
%
% cfg = [];
% cfg.method = 'mtmconvol';
% cfg.foi = 2:25;
% cfg.toi = -1:.05:0;
% cfg.tapsmofrq = ones(size(cfg.foi))*6;
% cfg.t_ftimwin = ones(size(cfg.foi))*.4;
% cfg.output = 'pow';
%
% data_tf_part{1} = ft_freqanalysis(cfg, data);
%
% cfg = [];
% cfg.method = 'mtmconvol';
% cfg.foi = 26:50;
% cfg.toi = -1:.05:0;
% cfg.tapsmofrq = ones(size(cfg.foi))*6;
% cfg.t_ftimwin = ones(size(cfg.foi))*.4;
% cfg.output = 'pow';
%
% data_tf_part{2} = ft_freqanalysis(cfg, data);

% cfg = [];
% cfg.method = 'mtmconvol';
% cfg.foi = 2:25;
% cfg.toi = .05:.05:1;
% cfg.tapsmofrq = ones(size(cfg.foi))*6;
% cfg.t_ftimwin = ones(size(cfg.foi))*.4;
% cfg.output = 'pow';
%
% data_tf_part{3} = ft_freqanalysis(cfg, data);
%
% cfg = [];
% cfg.method = 'mtmconvol';
% cfg.foi = 26:50;
% cfg.toi = .05:.05:1;
% cfg.tapsmofrq = ones(size(cfg.foi))*6;
% cfg.t_ftimwin = ones(size(cfg.foi))*.4;
% cfg.output = 'pow';
%
% data_tf_part{4} = ft_freqanalysis(cfg, data);
%
% At the end, you could use:
%
% data_tf_allparts = obob_append_tf(data_tf_part{:});
%
% The result is the same.

% Copyright (c) 2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.


% define possible datafields
datafields = {'powspctrm', 'fourierspctrm', 'crsspctrm'};

% create stub data
data = keepfields(varargin{1}, {'label', 'trialinfo', 'dimord', 'labelcmb'});

time = [];
freq = [];

% check for fields and dimensions in the data
if isfield(data, 'labelcmb')
  haslabelcmb = true;
else
  haslabelcmb = false;
end %if

dimtok = tokenize(varargin{1}.dimord, '_');
hasfreq = any(ismember(dimtok, 'freq'));
hastime = any(ismember(dimtok, 'time'));
hasrpt = any(ismember(dimtok, 'rpt'));
hasrpttap = any(ismember(dimtok, 'rpttap'));
haschan = any(ismember(dimtok, 'chan'));

% recreate dimord to match the input and have the proper order
dimords = {};
if hasrpt
  dimords{end+1} = 'rpt';
end %if

if hasrpttap
  dimords{end+1} = 'rpttap';
end %if

if haschan
  dimords{end+1} = 'chan';
end %if

if hasfreq
  dimords{end+1} = 'freq';
end %if

if hastime
  dimords{end+1} = 'time';
end %if

dimord = [];
for i = 1:length(dimords)
  dimord = [dimord dimords{i} '_'];
end %if
dimord(end) = [];

% apply checkdata on the first input
varargin{1} = ft_checkdata(varargin{1}, 'dimord', dimord, 'datatype', 'freq');

% get labels and possible labelcmb
labels = varargin{1}.label;

if haslabelcmb
  labelcmbs = varargin{1}.labelcmb;
end %if

% check if all labels agree and adjust all dimords
for i = 1:length(varargin)
  varargin{i} = ft_checkdata(varargin{i}, 'dimord', dimord, 'datatype', 'freq');
  if ~isequal(labels, varargin{i}.label)
    error('labels mismatch');
  end %if
  
  if haslabelcmb
    if ~isequal(labelcmbs, varargin{i}.labelcmb)
      error('labelcmbs mismatch');
    end %if
  end %if  
end %for

% get the location of all dimensions
dimtok = tokenize(dimord, '_');
chandim = find(ismember(dimtok, 'chan'));
rptdim = find(ismember(dimtok, {'rpt', 'rpttap'}));
timedim = find(ismember(dimtok, 'time'));
freqdim = find(ismember(dimtok, 'freq'));

% check whether the number of trials matches in all inputs
if hasrpt || hasrpttap
  % check what datafield to use to look for trials....
  tmp = find(ismember(datafields, fieldnames(varargin{1})));
  field = datafields{tmp(1)};
  nrpt = size(varargin{1}.(field), rptdim);
  
  for i = 1:length(varargin)
    if size(varargin{i}.(field), rptdim) ~= nrpt
      error('number of trials does not match.');
    end %if
  end %for
end %if

% concatenate time and freq fields and put them into the output data
for i = 1:length(varargin)
  if hastime
    time = [time varargin{i}.time];
  end %if
  
  if hasfreq
    freq = [freq varargin{i}.freq];
  end %if
end %for

time = sort(unique(time));
freq = sort(unique(freq));

if hastime
  data.time = time;
end %if

data.freq = freq;

% loop over the datafields and concatenate the data
for i = 1:length(datafields)
  if ismember(datafields{i}, fieldnames(varargin{1}))
    curfield = datafields{i};
    
    % create the final output field of the current data field
    tmpsize = size(varargin{1}.(curfield));
    if hastime
      tmpsize(timedim) = length(time);
    end %if
    if hasfreq
      tmpsize(freqdim) = length(freq);
    end %if
    
    data.(curfield) = zeros(tmpsize, 'like', varargin{1}.(curfield));
    
    % loop over all inputs
    for j = 1:length(varargin)
      freq_ind = [];
      time_ind = [];
      
      % get the indices for the time and freq within the output data
      if hasfreq
        [~, freq_ind] = ismember(varargin{j}.freq, freq);
      end %if
      if hastime
        [~, time_ind] = ismember(varargin{j}.time, time);
      end %if
                
      % build indices...
      idx = {};
      if hasrpt || hasrpttap
        idx{rptdim} = 1:nrpt;
      end %if
      
      if hasfreq
        idx{freqdim} = freq_ind;
      end %if
      
      if hastime
        idx{timedim} = time_ind;
      end %if
      
      idx{chandim} = 1:tmpsize(chandim);
      
      % insert data
      data.(curfield)(idx{:}) = varargin{j}.(curfield);
    end %for
    
  end %if
end %for

% check for cumtapcnt and cumsumcnt fields. need to be handled seperately
if isfield(varargin{1}, 'cumtapcnt')
  if size(varargin{1}.cumtapcnt, 2) > 1
    data.cumtapcnt = zeros(size(varargin{1}.cumtapcnt, 1), length(freq));

    for j = 1:length(varargin)
      [~, freq_ind] = ismember(varargin{j}.freq, freq);
      data.cumtapcnt(:, freq_ind) = varargin{j}.cumtapcnt;
    end %for
  else
    data.cumtapcnt = varargin{1}.cumtapcnt;
  end %if
end %if

if isfield(varargin{1}, 'cumsumcnt')
  if size(varargin{1}.cumsumcnt, 2) > 1
    data.cumsumcnt = zeros(size(varargin{1}.cumsumcnt, 1), length(freq));

    for j = 1:length(varargin)
      [~, freq_ind] = ismember(varargin{j}.freq, freq);
      data.cumsumcnt(:, freq_ind) = varargin{j}.cumsumcnt;
    end %for
  else
    data.cumsumcnt = varargin{1}.cumsumcnt;
  end %if
end %if
end


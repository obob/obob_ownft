function timelock = obob_svs_timelockbaseline(cfg, timelock)

% A simple virtual sensor (svs)
% obob_svs_timelockbaseline performs baseline normalization on averaged evoked data with absolute or 
% relative measures. Useful for source data reconstructed with
% svs_beamtrials_lcmv to, e.g., deal with the center bias.
%
% Importantly, by defaults absolute values are computed before 
% normalization.
%
% This function is mainly for plotting of source time series (svs_beamtrials_lcmv)
%
% Call as:
% timelock = obob_svs_timelockbaseline(cfg, timelock)
%
% Input:
%   timelock         = time locked data struct containing an avg field
%                      (e.g., after ft_timelockanalysis)
%   cfg.baseline     = [start end] period used for normalization
%   cfg.baselinetype = 'absolute', 'relative' or 'relchange' 
%   cfg.keeppolarity = true or false; default: false (i.e., compute abolute values) 
%
% Output:
% timelock = time locked data struct, baseline normalized

% Copyright (c) 2014-2016, Nathan Weisz & Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.
%%

% do some initialization...
ft_defaults
ft_preamble provenance
ft_preamble trackconfig

% check for required input
ft_checkconfig(cfg, 'required', {'baseline' 'baselinetype'});
% input
baseline=cfg.baseline;
baselinetype=cfg.baselinetype;
timeVec=timelock.time;
% defaults
keeppolarity = ft_getopt(cfg, 'keeppolarity', false, 0); 

% check if data fit
if ~ismatrix(timelock.avg) % check if avg field has two dimensions
  error('timelock matrix should have two dimensions (chan,time)');
end

%% take absolute values
if ~keeppolarity
  timelock.avg=abs(timelock.avg);
end%if

%% baseline normalization
% get baseline time window
baselineTimes = (timeVec >= baseline(1) & timeVec <= baseline(2));

% compute mean of time/frequency quantity in the baseline interval,
% ignoring NaNs, and replicate this over time dimension
meanVals = repmat(nanmean(timelock.avg(:,baselineTimes), 2), [1 size(timelock.avg, 2)]);

% compute the respective baseline normalization
if (strcmp(baselinetype, 'absolute'))
  timelock.avg = timelock.avg - meanVals;
elseif (strcmp(baselinetype, 'relative'))
  timelock.avg = timelock.avg ./ meanVals;
elseif (strcmp(baselinetype, 'relchange'))
  timelock.avg = (timelock.avg - meanVals) ./ meanVals;
else
  error('unsupported method for baseline normalization: %s', baselinetype);
end

% clean up...
ft_postamble provenance
%ft_postamble trackconfig

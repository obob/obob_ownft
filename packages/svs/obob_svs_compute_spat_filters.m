function filt = obob_svs_compute_spat_filters(cfg, data)
% Compute spatial filters using lcmv for virtual sensor computation.
%
% Call as:
%   filt = obob_svs_compute_spat_filters(cfg, data)
%
% Input:
% obligatory:
%   cfg.grid     = source grid that must contain the leadfield (in grid.leadfield).
%                  If cfg.parcel is set, the grid and the parcel must correspond
%   data         = The data to use for the computation of the spatial
%                  filter. This must be the result of ft_timelockanalysis
%                  with cfg.covariance = 'yes';
%
%   Sensor positions (grad or elec) can be specified in the cfg or data
%   structure.
%
% optional:
%   cfg.parcel         = A parcellation structure if you want to get
%                        parcellated output. You can either load the template
%                        (parcellations_3mm.mat) or create one with
%                        obob_svs_create_parcellation. Otherwise leave empty.
%   cfg.parcel_avg     = How to reduce the activity of all voxels within one
%                        parcel to a single time course. Can be: 'avg_filters'
%                        (default). This averages the spatial filters within
%                        each parcel and then applies them to the sensor data.
%                        'svd_sources' first calculates the sensor time-course
%                        of all voxels. It then runs an svd (a kind of PCA) on
%                        the activity within each parcel and retains all
%                        components it needs to explain 95% of the variance.
%                        These components are then averaged.
%   cfg.regfac         = regularization factor to be passed on to source analysis
%                        (e.g. '5%'). (optional)
%   cfg.fixedori       = Whether to use fixed or free orientation sources.
%                        Default = 'yes'.
%
% Output:
% filt           = the spatial filters. Use obob_svs_beamtrials_lcmv to
%                  apply them to your data.

% Copyright (c) 2014-2017, Nathan Weisz, Julia Frey, Anne Hauswald & Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% check input
ft_checkconfig(cfg, 'required', 'grid');

if ~isfield(cfg.grid, 'leadfield')
  error('obob:svs:nolf', 'You must supply a leadfield in the grid.');
end %if

regfac = ft_getopt(cfg, 'regfac', []);
fixedori = ft_getopt(cfg, 'fixedori', 'yes');
grid = ft_getopt(cfg, 'grid');
parcel=ft_getopt(cfg, 'parcel', []);
parcel_avg = ft_getopt(cfg, 'parcel_avg', 'avg_filters');

avg_filter = strcmp(parcel_avg, 'avg_filters');

if ~isfield(data, 'cov')
  error('obob:svs:nocov', 'No covariance provided in data');
end %if

if ~isempty (parcel)
  parcel = cfg.parcel.parcel_array;
  for idx_parcel = 1:length(parcel)
    if ~strcmp(parcel{idx_parcel}.unit, 'm')
      error('Your parcellation structure is not in meters!');
    end %if
  end %for
end
%% do lcmv beamforming (better: compute filter on averaged data)

cfg=[];
cfg.method          = 'lcmv';
cfg.headmodel.unit        = 'm'; % th: dirty hack to make this work. as we always provide leadfields, we do not need a vol...
cfg.sourcemodel            = grid;
cfg.lcmv.projectnoise    = 'yes';
cfg.lcmv.keepfilter = 'yes';
cfg.lcmv.fixedori   = fixedori;
cfg.lcmv.lambda     = regfac;

lcmvall=ft_sourceanalysis(cfg, data);

%%
cmpfilter = lcmvall.avg;
% here end of the filter calculation
% do parcel 2 filt
if ~isempty (parcel) && avg_filter
  cfg_l = [];
  cfg_l.parcel = parcel;
  cfg_l.filter = lcmvall.avg;
  cmpfilter  = obob_svs_filt2parcel(cfg_l);
  cmpfilter.label = lcmvall.avg.label;
end

%% collect output
filt.grid = grid;
filt.spatfilt = cmpfilter;
filt.parcel = parcel;
filt.parcel_avg = parcel_avg;
end


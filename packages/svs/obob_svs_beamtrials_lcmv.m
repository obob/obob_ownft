function data_source = obob_svs_beamtrials_lcmv(cfg, data)

% A simple virtual sensor (svs)
% obob_svs_beamtrials_lcmv uses a broad-band user specified lcmv filter to project single trial 
% time series into source space.
%
% Call as:
%   data_source = obob_svs_beamtrials_lcmv(cfg, data)
%
% Input:
% obligatory:
%   cfg.spatial_filter     = Spatial filters as returned by
%                            obob_svs_compute_spat_filters
%   data                   = ft structure after preprocessing
%
%   Sensor positions (grad or elec) can be specified in the cfg or data
%   structure.
%
% optional:
%   cfg.parcel_svd_var     = Variance threshold for the svd_sources approach.
%                            Defaults to 0.95 (i.e. 95%).
%
%   cfg.nai                = If this is set to true or 'yes', the output of the
%                            projection will be normalized by the noise estimate at
%                            the voxels, effectively calculating the neural activity
%                            index. default = false.
%
% Output: 
% data_source    = a preproc structure with source level time series at 
%                  each grid point. can be e.g. passed on to 
%                  ft_timelockanalysis or ft_freqanalysis, i.e. used like 
%                  a sensor level structure.

% Copyright (c) 2014-2017, Nathan Weisz, Julia Frey, Anne Hauswald & Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% check whether cfg.spatial_filter is missing
if ~isfield(cfg, 'spatial_filter')
  error('obob:svs:deprecated', ['Using obob_svs_beamtrials_lcmv without computing spatial filters ' ...
                                  'is deprecated and does not work anymore!\n' ...
                                  'Please use obob_svs_compute_spat_filters!'])
end %if

%% defaults
ft_checkconfig(cfg, 'required', 'spatial_filter');

nai = ft_getopt(cfg, 'nai', false);
parcel_svd_var = ft_getopt(cfg, 'parcel_svd_var', 0.95);
spatial_filter = cfg.spatial_filter;
parcel_avg = spatial_filter.parcel_avg;
parcel = spatial_filter.parcel;
grid = spatial_filter.grid;

if ~contains(parcel_avg, {'avg_filters', 'svd_sources'})
  error('cfg.parcel_avg must be either avg_filters of svd_sources');
end %if

avg_filter = strcmp(parcel_avg, 'avg_filters');


if ischar(nai)
  if strcmpi(nai, 'yes')
    nai = true;
  else
    nai = false;
  end %if
end %if

if nai
  warning(sprintf(['WARNING! Calculating the Neural Activity Index is a new feature and should thus be considered HIGHLY EXPERIMENTAL!\n' ...
    'Please use it with extreme care and check your results.\n' ...
    'There is also NO GUARANTEE that this feature is already stable in the sense\n' ...
    'that its results might be different in a while!\n\n' ...
    'YOU HAVE BEEN WARNED!']));
end %if

sens = ft_fetch_sens(cfg, data);
if ~strcmp(sens.unit, 'm')
  error('Your sensor definition is not in meters!');
end %if

if ft_senstype(sens, 'eeg')
  data.elec = sens;
else
  data.grad = sens;
end %if

%% check whether the channels match
if ~isequal(sort(spatial_filter.spatfilt.label), sort(data.label))
  error('obob:svs:chan_mismatch', 'Channels in spatial filter and data do not match');
end %if

%% check whether leadfield channel order and data sensor order match, if not abort for now
% get sensor indeces of data in lf and lf in data
[~, idA, idB] = intersect(data.label, grid.cfg.channel);
% compare order
lf_data_match = isequal(idA, idB);

if ~lf_data_match % if labels do not match try to find the reason and report
  
  if numel(data.label) ~= numel(grid.cfg.channel)
    error('The number of channels in your data and leadfield are different. Check whether cfg.channel contains the right input.')
  end
  % check if sorted leadfield labels would fit (likely ft_selectdata
  % problem)
  [~, idA, idB] = intersect(data.label, sort(grid.cfg.channel));
  lf_data_sort_match = isequal(idA, idB);
  if lf_data_sort_match
    error(['It seems your data sensors are ordered alphabetically. Try using obob_reorder_channels to undo this and submit again. '...
      'If the channel order in the data and in the leadfield don''t match the output of this function is wrong!'])
  else
    error('There is an unknown mismatch between your data sensors and the leadfield sensors')
  end
end

%% apply filter from average lcmv to single trials
cmpfilter = spatial_filter.spatfilt;

beamfilts = cat(1,cmpfilter.filter{:});

if ~isempty (parcel) && avg_filter
    all_noisecoeffs = cmpfilter.noise{:};
else 
    all_noisecoeffs = cmpfilter.noise(grid.inside);
end

% create output structure
%------------------------
data_source = data;

nrpt = length(data.trial); % n of trials

% init progress report
%----------------------
fprintf('using precomputed filters\n')
ft_progress('init', 'text');

if ~isempty (parcel) && ~avg_filter
  all_sensor_data = horzcat(data.trial{:});
  % preallocate the trials...
  data_source.trial = cellfun(@(x) zeros(length(parcel), size(data.trial{1}, 2)), data_source.trial, 'UniformOutput', false);
  data_source.label = {};
  for idx_parcel = 1:length(parcel)
    ft_progress(idx_parcel/length(parcel), 'scanning parcel %d of %d', idx_parcel, length(parcel));
    cur_parcel = parcel{idx_parcel};
    cur_beam_filt = cat(1,cmpfilter.filter{cur_parcel.roi_mask(:) == 1});
    
    % project sensor data to all sources belonging to the parcel
    cur_src_data = cur_beam_filt * all_sensor_data;
    
    % do a pca on that data
    [u, s, ~]=svd(cur_src_data,'econ');
    s = cumsum(diag(s.^2))./sum(diag(s.^2));
    keep = s <= parcel_svd_var;
    
    % keep only those components that are needed to explain parcel_svd_var
    % variance.
    cur_parcel_final_data = mean(u(:,keep)' * cur_src_data, 1);
    
    % put data in trials...
    samples_per_trial = size(data.trial{1}, 2);
    for idx_trial = 1:length(data_source.trial)
      first_sample = 1 + (idx_trial-1)*samples_per_trial;
      last_sample = first_sample + samples_per_trial - 1;
      data_source.trial{idx_trial}(idx_parcel, :) = cur_parcel_final_data(1, first_sample:last_sample);
    end %for
    data_source.label{idx_parcel} = cur_parcel.roi_name;
  end %for
else
  for i=1:length(data.trial)

    % update progress report
    %------------------------
    ft_progress(i/nrpt, 'scanning repetition %d of %d', i, nrpt);

    % multiply trials with filter
    %-----------------------------
    data_source.trial{i} = beamfilts*data.trial{i};

     if nai% TO DO
       data_source.trial{i} = data_source.trial{i} ./ repmat(all_noisecoeffs, 1, size(data_source.trial{i}, 2));
     end %if

  end
end %if
% close progress report
%----------------------
ft_progress('close');

%% create a pseudo preprocessing structure for the output
% labels
%-------
if ~isempty (parcel) && avg_filter
    %data_source.label    = cellstr(num2str([1:length(cmpfilter.parlabel)]'));
    data_source.label = cmpfilter.parlabel;
elseif isempty(parcel)
    data_source.label = cellstr(num2str([1:sum(grid.inside)]'));
end

% remove fields that do not fit
%-------------------------------
data_source = rmfield(data_source, 'cfg');

if isfield(data_source, 'grad')
  data_source = rmfield(data_source, 'grad');
end %if

if isfield(data_source, 'elec')
  data_source = rmfield(data_source, 'elec');
end %if

function outglobal=obob_gm2_global_measure(cfg, sconn)
% GM2_GLOBAL_MEASURES - calculates specified global graph mapping measures.
%
% Use as
%   [outglobal] = gm2_global_measure(cfg, sconn)
%
% Input:
%
% cfg.measure     = global gm measure, string; supported options:
%                   'density', 'efficiency', 'clustering', 'charpath'
%                   (characteristic path length),
%                   'disconnection' (proportion disconnected nodes)
%                   'smallworld'(-edness), 'modularity'
% cfg.latency     = latency boundaries [start end] (optional)
% cfg.frequency   = frequency boundaries [start end] (optional)
% cfg.nmodularity = optional for modularity: number of modularity
%                   calculations (default 50)
%
% sconn           = connectivity structure containing adjacency matrix
%                  (sconn.adjmatspctrm) as chan x chan x freq (x time)
%
%

% Copyright (c) 2015-2016, Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% check cfg options
if ~isfield(cfg, 'measure')
  help gm2_global_measure
  error('This function needs an input field cfg.measure. See help above')
else
  measure = cfg.measure;
end

% defaults
cfg.frequency = ft_getopt(cfg, 'frequency', 'all');
cfg.latency = ft_getopt(cfg, 'latency', 'all');
cfg.nmodularity = ft_getopt(cfg, 'nmodularity', 50);

%% select data
cfg_tmp = [];
cfg_tmp.frequency = cfg.frequency;
cfg_tmp.latency = cfg.latency;
sconn=ft_selectdata(cfg_tmp, sconn);

%% initialize output and preallocate memory

outglobal=[]; %initialize output structure
outglobal.label={measure}; % label of output 'channel', for global gm the same as measure
outglobal.measure = measure; % gm measure used
outglobal.freq=sconn.freq; %freqs

%preallocate memory for output graph measures
if strcmpi(sconn.dimord, 'chan_chan_freq')
  outglobal.dimord='chan_freq';
elseif strcmpi(sconn.dimord, 'chan_chan_freq_time')
  outglobal.dimord='chan_freq_time';
  %make 3D from4D ... after BCT call reshape back!
  sconn.adjmatspctrm=reshape(sconn.adjmatspctrm,[length(sconn.label),length(sconn.label),length(sconn.freq)*length(sconn.time)]);
end %if

% output matrix
outmat = zeros(1,size(sconn.adjmatspctrm,3));

%% Call BCT
% all measures here are binary undirected
for ii=1:size(sconn.adjmatspctrm,3)
  switch measure
    case 'density'
      [outmat(1,ii)]= density_und(sconn.adjmatspctrm(:,:,ii));
    case 'clustering'
      [outmat(1,ii)]= mean(clustering_coef_bu(sconn.adjmatspctrm(:,:,ii)));
    case 'efficiency'
      [outmat(1,ii)]= efficiency_bin(double(sconn.adjmatspctrm(:,:,ii)));
    case 'charpath'
      tmpD=distance_bin(double(sconn.adjmatspctrm(:,:,ii)));
      [outmat(1,ii)]= charpath(tmpD); clear tmpD;
    case 'disconnection'
      tmpD=distance_bin(double(sconn.adjmatspctrm(:,:,ii)));
      tmp = (tmpD~=Inf).*abs(eye(size(tmpD,1))-1); tmp=sum(tmp);
      [outmat(1,ii)] = length(find(tmp==0)) / length(tmp);
    case 'smallworld'
      % for smallworldedness we need to calculate clustering, pathlength
      % and their random network pairs. calculation happens below
      randMat = makerandCIJ_und(size(sconn.adjmatspctrm(:,:,ii),1),sum(sum(sconn.adjmatspctrm(:,:,ii)))/2); %divide due to symmetric matrix
      
      tmpD=distance_bin(double(sconn.adjmatspctrm(:,:,ii)));
      [plengthvec(ii) tmp]= charpath(tmpD); clear tmp*
      
      tmpD=distance_bin(randMat);
      [ranplengthvec(ii) tmp]= charpath(tmpD); clear tmp*
      
      clustervec(ii)=mean(clustering_coef_bu(sconn.adjmatspctrm(:,:,ii)));
      randclustervec(ii)=mean(clustering_coef_bu(randMat));
    case 'modularity'
      % modularity can change from run to run due to heuristics in the
      % algorithm. see the function for reference
      % here as a default we run 50 calculations and average (see also
      % Chennu et al., 2014, Plos One)
      nrep = cfg.nmodularity;
      % give some feedback, so we roughly know in which loop we are
      fprintf('Estimating modularity\n')
      ft_progress('init', 'text');
      for i = 1:nrep
        % update progress
        ft_progress(i/nrep, 'Modularity calculation #%d of %d', i, nrep);
        
        [~, temp]= modularity_und(sconn.adjmatspctrm(:,:,ii));
        outmat(1,ii) = outmat(1,ii) + temp; % running sum
      end %for
      % average (devide by # of repetitions)
      outmat(1,ii) = outmat(1,ii) / nrep;
      % close progress report
      ft_progress('close');
    otherwise
      error('Unknown graph mapping measure. Check your input.')
  end %switch
end %for

% extra step needed for smallworldedness
if strcmp(measure, 'smallworld')
  outmat=(clustervec./randclustervec)./(plengthvec./ranplengthvec);
end %if

%% reshape data back if time-frequency

if strcmpi(sconn.dimord, 'chan_chan_freq_time')
  outmat=reshape(outmat,[1,length(sconn.freq),length(sconn.time)]);
  outglobal.time=sconn.time; % time. only assign if time-freq data
end %if

%% finalize output struct

outglobal.(measure) = outmat;
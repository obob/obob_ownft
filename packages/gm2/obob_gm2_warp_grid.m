function [individual_grid, vol]=obob_gm2_warp_grid(template_grid, mri, coordsys)
% obob_gm2_warp_grid warps the mni grid obtained by obob_gm2_make_mnigrid
% to an individual mri.
%
% Use as
%   [individual_grid, vol]=obob_gm2_warp_grid(template_grid, mri, coordsys)
%
% where the first input argument is the template grid obtained by
% obob_gm2_make_mnigrid, the second argument is the individual,
% coregistered MRI and the third argument is the target coordinate system.

% Copyright (c) 2010-2016, The OBOB group
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.


%%
cfg = [];
%cfg.downsample = 2;
cfg.coordsys = coordsys;
seg = ft_volumesegment(cfg, mri);

%%
cfg = [];
cfg.method = 'singleshell';
vol = ft_prepare_headmodel(cfg, seg);

vol=ft_convert_units(vol, 'cm');

%%
mri.coordsys=coordsys;

cfg = [];
cfg.coordsys=coordsys;
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = template_grid;
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.mri            = mri;
individual_grid               = ft_prepare_sourcemodel(cfg);

%%
% figure;
% ft_plot_vol(vol, 'edgecolor', 'none'); alpha 0.4;
% ft_plot_mesh(individual_grid.pos(individual_grid.inside,:));





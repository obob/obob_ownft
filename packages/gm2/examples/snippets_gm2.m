% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

clear all
restoredefaultpath
addpath('/data/fieldtrip/')
ft_defaults
addpath('/home/nathan/Matlab/gm2/')

%% STEP 1: CREATE TEMPLATE GRID

cfg          = [];
cfg.gridres = 2; %to speed up calculations
cfg.plot='yes';
template_grid=gm2_make_mnigrid(cfg);

%% STEP 2: WARP GRID IN INDIVIDUAL SPACE

load /obobstation/2/backupanne120206/home/anne/Daten/Autismus/Daten/raw/AJ/MuGrasp/source_aux_files/mri.mat
[individual_grid vol]=gm2_warp_grid(template_grid, mri, '4d');

%% STEP 3: LOAD DATA AND RUN FFT WITH OUTPUT FOURIER

load /obobstation/2/backupanne120206/home/anne/Daten/Autismus/Daten/raw/AJ/ASWA_FFT/aswa.mat

cfg=[];
cfg.foi=[2:5:30];
cfg.tapsmofrq=2; %THIS WILL BE AUTOMATICALLY CHECKED FOR BEAMFORMER FILTER

freq=gm2_calcsens_fourier(cfg, aswa);

%% STEP 4: GET FOURIER COEFFICIENT IN SOURCE SPACE (OPTIMIZED DICS FILTERS)


cfg=[];
cfg.grid = individual_grid; %if not leadfield then this will be computed on the fly
cfg.vol=vol;
cfg.regfac = '5%';

sfreq=gm2_calcsource_fourier(cfg, freq, aswa);

%% EXCURSION: GET POWER SPECTRUM
cfg=[];
%cfg.keeptrials='yes'; %optional

spow=ft_freqdescriptives(cfg, sfreq);
plot(spow.freq,mean(zscore(spow.powspctrm')'))

%% STEP 5: CALCULATE SOURCE CONNECTIVITY

cfg=[];
cfg.method='icoh';
sconn=gm2_calcsource_conn(cfg, sfreq);

sconn.cohspctrm=abs(sconn.cohspctrm);

plot(spow.freq,squeeze(mean(mean(sconn.cohspctrm))))

%% STEP 6: MAKE ADJACENY MATRIX

sconn.adjmatspctrm = (sconn.cohspctrm > .03);
sconn.adjmatspctrm = double(sconn.adjmatspctrm); % make it double, mainly for efficiency

%% STEP 7: CALCULATE GLOBAL GRAPH THEORETICAL VALUES
addpath('/home/nathan/subversion/tinnitus/matlab/Toolboxes/2010-11-16 BCT')

%output structures are not fieldtrip, but take into consideration frequency
cfg=[];
%Density (fraction of connections to all possible connections)
kdense=gm2_global_density(cfg, sconn);

%path length, global efficiency, clustering
pathglobal=gm2_global_charpath(cfg, sconn);

%small worldedness
smglobal=gm2_global_smallworld(cfg,sconn);

%% STEP 8: CALCULATE LOCAL GRAPH THEORETICAL VALUE HERE DEGREE

cfg=[];
sdeg=gm2_local_degree(cfg,sconn);

cfg=[];
seff=gm2_local_efficiency(cfg,sconn);

cfg=[];
sbet=gm2_local_betweenness(cfg,sconn);


%%
plotpar='betweenness';
template = ft_read_mri('/data/fieldtrip/external/spm8/templates/T1.nii');

cfg=[];
cfg.sourcegrid=template_grid; %use MNI template!!
cfg.mri=template;
cfg.parameter=plotpar;
cfg.foilim = [18 18];

if strcmpi(plotpar, 'degrees')
    sgmI=gm2_make_source(cfg, sdeg);
elseif strcmpi(plotpar, 'efficiency')  
    sgmI=gm2_make_source(cfg, seff);
elseif strcmpi(plotpar, 'betweenness')  
    sgmI=gm2_make_source(cfg, sbet);
end

%%

sgmI.param=sgmI.(plotpar) .* (sgmI.(plotpar) > max(sgmI.(plotpar)(:))*.85);

cfg=[];
cfg.method='ortho';
cfg.funparameter='param';
cfg.interactive='yes';
cfg.projmethod='nearest';
cfg.camlight='no';
% cfg.funcolorlim=[max(sdegI.degree(:))/2 max(sdegI.degree(:))];
% cfg.opacitylim=cfg.funcolorlim;

ft_sourceplot_old(cfg, sgmI);









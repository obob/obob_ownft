function sgraphpar=obob_gm2_make_source(cfg, sgraphpar)
% obob_gm2_make_source takes on any gm structure with local graph theoretical parameter.
% Creates structure analogous to one following ft_sourceanalysis.
% Interpolation on MR is optional.
%
% Use as:
%   sgraphpar=obob_gm2_make_source(cfg, sgraphpar)
%
% where the first input argument is a configuration structure (see below)
% and the second argument is a the output of obob_gm2_local_measure.
%
% The configuration structure can contain:
%   cfg.sourcegrid  = source grid 
%
%   cfg.parameter   = local graph theoretical parameter you want in source
%                     space
%
%   cfg.foilim      = freqency range 
%
%   cfg.mri         = (optional) mri corresponding to sourcegrid; this option
%                      will automatically output a sourceinterpolated struct!

% Copyright (c) 2014-2016, Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%%
sgrid=cfg.sourcegrid;
interparam=cfg.parameter;
foilim = cfg.foilim;

if isfield(cfg, 'mri')
    mri=cfg.mri;
    sinter=1;
else
    sinter=0;
end


%%
f1 = nearest(sgraphpar.freq, foilim(1));
f2 = nearest(sgraphpar.freq, foilim(2));
tmp=mean(sgraphpar.(interparam)(:, f1:f2),2);

%%
sgraphpar=sgrid;
sgraphpar.(interparam)=NaN(size(sgraphpar.pos,1),1);
sgraphpar.(interparam)(sgraphpar.inside)=tmp;


if sinter == 1
cfg=[];
cfg.parameter=interparam;
sgraphpar = ft_sourceinterpolate(cfg, sgraphpar, mri);
end



function template_grid=obob_gm2_make_mnigrid(cfg)
% obob_gm2_make_mnigrid creates a standard grid in the mni coordinate
% system.
%
% Use as
%   template_grid=obob_gm2_make_mnigrid(cfg)
%
% where the first input argument is a configuration structure (see below).
%
% The configuration structure can contain:
%   cfg.gridres    = The resolutin of the grid in cm. (default = 1)
%
%   cfg.plot       = Set to 'yes' if you want to plot the result. (default
%                  = 'no')

% Copyright (c) 2010-2016, The OBOB group
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

if isfield(cfg, 'gridres')
    gridres=cfg.gridres;
else
    gridres=1;
end

if isfield(cfg, 'plot')
    doplot=cfg.plot;
else
    doplot='no';
end

%%
load template_mni

cfg = [];
cfg.grid.xgrid  = -20:gridres:20;
cfg.grid.ygrid  = -20:gridres:20;
cfg.grid.zgrid  = -20:gridres:20;
cfg.grid.unit   = 'cm';
cfg.grid.tight  = 'yes';
cfg.inwardshift = -1.5;
cfg.vol        = template_vol;
template_grid  = ft_prepare_sourcemodel(cfg);

%%
 
% make a figure with the template head model and dipole grid
if strcmpi(doplot ,'yes')
figure
hold on
ft_plot_vol(template_vol);
ft_plot_mesh(template_grid);
end
function outlocal=obob_gm2_local_measure(cfg, sconn)

% GM2_LOCAL_MEASURES - calculates specified local graph mapping measures.
%
% Use as:
%   [outlocal] = gm2_local_measure(cfg, sconn)
%
% Input:
%
% cfg.measure  = local gm measure, string; supported options:
%                (node) 'degrees', 'efficiency', 'clustering', 
%                (node) 'betweenness' 
% cfg.latency   = latency boundaries [start end] (optional)
% cfg.frequency = frequency boundaries [start end] (optional)
%
% sconn        = connectivity structure containing adjacency matrix
%                (sconn.adjmatspctrm) as chan x chan x freq (x time)
%

% Copyright (c) 2015-2016, Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% check cfg options
if ~isfield(cfg, 'measure')
  help gm2_global_measure
  error('This function needs an input field cfg.measure. See help above')
else
  measure = cfg.measure;
end

% defaults
cfg.frequency = ft_getopt(cfg, 'frequency', 'all');
cfg.latency = ft_getopt(cfg, 'latency', 'all');

%% select data
cfg_tmp = [];
cfg_tmp.frequency = cfg.frequency;
cfg_tmp.latency = cfg.latency;
sconn=ft_selectdata(cfg_tmp, sconn);

%% initialize output and preallocate memory

outlocal=[]; %initialize output structure
outlocal.label=sconn.label; % label of output 'channel', 
outlocal.measure = measure; % gm measure used
outlocal.freq=sconn.freq; %freqs

%preallocate memory for output graph measures
if strcmpi(sconn.dimord, 'chan_chan_freq')
  outlocal.dimord='chan_freq';
elseif strcmpi(sconn.dimord, 'chan_chan_freq_time')
  outlocal.dimord='chan_freq_time';
  %make 3D from4D ... after BCT call reshape back!
  sconn.adjmatspctrm=reshape(sconn.adjmatspctrm,[length(sconn.label),length(sconn.label),length(sconn.freq)*length(sconn.time)]);
end %if

% output matrix
outmat=zeros(length(sconn.label), size(sconn.adjmatspctrm,3));

%% Call BCT 
% all measures here are binary undirected
for ii=1:size(sconn.adjmatspctrm,3)
  switch measure
    case 'degrees'
      [outmat(:,ii)]= degrees_und(sconn.adjmatspctrm(:,:,ii));
    case 'clustering'
      [outmat(:,ii)]= clustering_coef_bu(sconn.adjmatspctrm(:,:,ii));
    case 'efficiency'
      [outmat(:,ii)]= efficiency_bin(sconn.adjmatspctrm(:,:,ii),1); 
    case 'betweenness'
      [outmat(:,ii)]= betweenness_bin(sconn.adjmatspctrm(:,:,ii)); 
    otherwise
      error('Unknown graph mapping measure. Check your input.')
  end %switch
end %for

%% reshape data back if time-frequency

if strcmpi(sconn.dimord, 'chan_chan_freq_time')
  outmat=reshape(outmat,[length(sconn.label),length(sconn.freq),length(sconn.time)]);
  outlocal.time=sconn.time; % time. only assign if time-freq data
end %if

%% finalize output struct

outlocal.(measure) = outmat;

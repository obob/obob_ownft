function randconn=obob_gm2_makerandgraph(cfg, sconn)

% GM2_MAKERANDGRAPH - Wrapper function calling makerandCIJ_und.
%
% Use as:
%   [randconn] = gm2_makerandgraph(cfg, sconn)
%
% where the first input argument is a configuration structure (see below)
% and the second argument is a connectivity structure containing an
% adjacency matrix.
%
% The configuration structure can contain:
%   cfg.toi        = latency boundaries [start end] (optional)
%
%   cfg.foi        = frequency boundaries [start end] (optional)
%
%   sconn          = connectivity structure containing adjacency matrix
%                    (sconn.adjmatspctrm) as chan x chan x freq (x time)
%

% Copyright (c) 2010-2016, Nathan Weisz & Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% Check cfg options
% defaults
cfg.frequency = ft_getopt(cfg, 'frequency', 'all');
cfg.latency = ft_getopt(cfg, 'latency', 'all');

%% select data
cfg_tmp = [];
cfg_tmp.frequency = cfg.frequency;
cfg_tmp.latency = cfg.latency;
sconn=ft_selectdata(cfg_tmp, sconn);

%% create output structure and preallocate memory

randconn=[]; %initialize output structure
randconn.label=sconn.label; %label of output 'channel'
randconn.freq=sconn.freq; % freq
randconn.measure = 'randomnetwork'; % keep consistent with local function

%preallocate memory for output graph measures
if strcmpi(sconn.dimord, 'chan_chan_freq')
  randconn.dimord='chan_chan_freq';
elseif strcmpi(sconn.dimord, 'chan_chan_freq_time')
  randconn.dimord='chan_chan_freq_time';
  
  %make 3D from4D ... after BCT call reshape back!
  sconn.adjmatspctrm=reshape(sconn.adjmatspctrm,[length(sconn.label),length(sconn.label),length(sconn.freq)*length(sconn.time)]);
end

% output matrix
outmat=zeros(length(sconn.label), length(sconn.label), size(sconn.adjmatspctrm,3));

%% Call BCT

% binary undirected
for ii=1:size(sconn.adjmatspctrm,3)
  % get n of edges in actual data
  nedges=round(length(find(sconn.adjmatspctrm(:,:,ii)==1))/2);
  % create random network for (time-)freq bin
  [outmat(:,:,ii)]= makerandCIJ_und(length(sconn.label), nedges);
end

%% reshape data back if time-frequency

if strcmpi(sconn.dimord, 'chan_chan_freq_time')
    outmat=reshape(outmat,[length(sconn.label),length(sconn.label),length(sconn.freq),length(sconn.time)]);
    randconn.time=sconn.time;
end

%% finalize output struct

randconn.adjmatspctrm=outmat;



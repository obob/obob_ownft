% Copyright (c) 2013-2016, Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates. 

close all
clear all


%% plot a mask to see whether it is in the right location
clear all
load mni_grid_1_cm_2982pnts
% e.g., auditory ROI
cfg = [];
cfg.roi = {'Heschl_L' 'Temporal_Sup_L' 'Heschl_R' 'Temporal_Sup_R'};
cfg.atlas = fullfile(fileparts(which('obob_init_ft')), 'external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii');

grid = obob_lookup_roi(cfg, template_grid);

% use the mask as fake power 
grid.pow = double(grid.roi_mask);

%% plot - use flat patch plotting function

load standard_mri

cfg = [];
cfg.funparameter = 'pow';
cfg.maskparameter = 'roi_mask';
cfg.hemisphere = 'rh'; % or 'lh'
cfg.mri = mri;

f = obob_plot_surfacepatch(cfg, grid);

%% plotting using ft functions
%% interpolate to high res
load standard_mri

cfg = [];
cfg.parameter = {'pow' 'roi_mask'};

src = ft_sourceinterpolate(cfg, grid, mri);
src.coordsys = 'mni';

%% plot
cfg = [];
cfg.funparameter = 'pow';
cfg.method = 'surface';
cfg.maskparameter='roi_mask';
cfg.crosshair = 'no';%

cfg.colorbar = 'no';
cfg.surffile  = 'flat_patch_pial_rh.mat';% need the fitting pial!!!
cfg.surfinflated = 'flat_patch_rh.mat';


ft_sourceplot(cfg, src);

%% spot the difference
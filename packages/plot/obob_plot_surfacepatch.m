function [flat_patch] = obob_plot_surfacepatch(cfg, data)

% CIMEC_PLOT_SURFACEPATCH plots functional data on an MNI surface patch imported from freesurfer
% widely similar to ft_sourceplot, yet color of sulci/gyri is done a bit
% differently (freesurfer syle)
%
% Use as
%   [flat_patch] = obob_plot_surfacepatch(cfg, data)
%
% where the first input argument is a configuration structure (see below)
% and the second argument is a struct of functional fieldtrip source data either before or after
% interpolation to MNI brain 
%
% The configuration structure can contain:
%   cfg.funparameter  = string, parameter to plot
%
%   cfg.maskparameter = string, parameter to mask funparameter
%
%   cfg.mri           = mri in MNI space, to interpolate data on (patch is 
%                       also MNI), if empty it is assumed that interpolation
%                       has been done already
%
%   cfg.hemisphere    = 'lh'/'rh' hemisphere to plot flat patch, default: 'lh'
%
%   cfg.surfacepatch  = filename or surface patch structure, set only if you
%                       know what you are doing
%                       default: ['flat_patch' cfg.hemisphere '.mat']
%
%   cfg.funcolormap   = m by 3 colormap or one of the matlab colormaps, 
%                       default: jet 
%
%   cfg.funcolorlim   = [max min], functional data color limit default: maxabs
%
%   cfg.anacolor      = color gyri/sulci, 1 - dark/light grey, 2 - red/green,
%                       3 - smooth transition from light to dark grey;  default: [1]
%
%   cfg.colorbar      = 1 for colorbar default: 0
% 

% Copyright (c) 2013-2016, Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% input and defaults
param = ft_getopt(cfg, 'funparameter', 'pow');
mask = ft_getopt(cfg, 'maskparameter');
hemi = ft_getopt(cfg, 'hemisphere', 'lh');
mri = ft_getopt(cfg, 'mri');
surf = ft_getopt(cfg, 'surfacepatch', ['flat_patch_' hemi '.mat']);
anacolor = ft_getopt(cfg, 'anacolor', 1);
fun_color = ft_getopt(cfg, 'funcolormap', jet);
fun_lim = ft_getopt(cfg, 'funcolorlim');
col_bar = ft_getopt(cfg, 'colorbar', 0);

% get all params for interpolation
int_params = {param};
if ~isempty(mask)
  int_params{end+1} = mask;
end %if

%% load relevant data if necessary
if ischar(surf) % if filename
  % load the cimec flat patch
  load(surf);
  try
    surf = bnd; % bnd is fieldtrip convention, thus saved like that
  catch
    error('Your input surface patch does not contain a bnd structure, which is necessary')
  end
end %if

% check whether format right
if any(~isfield(surf, {'pnt' 'mni' 'tri' 'curv'}))
  error('Your input flat cortex patch does not have the right format for this function')
end % if

%% interpolate input functional data to high res
if ~isempty(mri)
  cfg = [];
  cfg.parameter = int_params;
  src = ft_sourceinterpolate(cfg, data, mri);
  src.coordsys = 'mni';
else
  src = data;
end

%% interpolate on patch
% modify patch data
surf.pnt_patch = surf.pnt;% pnt are the 2D points, put elsewhere
surf.pnt = surf.mni; % move the mni points to pnt and interpolate functional data 

cfg = [];
cfg.parameter = int_params;
src = ft_sourceinterpolate(cfg, src, surf);
src.coordsys = 'mni';

%% color for gyri/sulci
% we are simply changing the curv values (which represent the curvature of
% the cortex and thus separate between gyrus and sulcus) to a binary color
% map with either dark gray or red for sulci and light gray or green for
% gyri

% border in the default patch between gyri/sulci is 0
thresh = 0; 
color = zeros(length(surf.curv),3);
switch anacolor
  case 1 %grey levels
    col1 = [.4 .4 .4]; % sulci
    col2 = [.7 .7 .7]; % gyri
    col3 = [0 0 0]; % center
  case 2 % red/green
    col1 = [1 0 0]; % sulci
    col2 = [0 1 0]; % gyri
    col3 = [0 0 0]; % center
  case 3
  % a smooth light-to-dark brown/grey version
  cortex_light = [0.781 0.762 0.664];
  cortex_dark  = [0.781 0.762 0.664]/2;
  % the curvature determines the color of gyri and sulci
  color = surf.curv(:) * cortex_dark + (1-surf.curv(:)) * cortex_light;
end %switch

if any(ismember(anacolor, [1 2])) % only if 3 color definitions present
  color(surf.curv>thresh,:) =  repmat(col1, sum(surf.curv>thresh), 1);
  color(surf.curv<thresh,:) =  repmat(col2, sum(surf.curv<thresh), 1);
  color(surf.curv==thresh,:) =  repmat(col3, sum(surf.curv==thresh), 1);
end
%% plot patch (on the 2D coordinates)
figure;
flat_patch.h1 = patch('Vertices', surf.pnt_patch, 'Faces', src.tri, ...
  'FaceVertexCData', color, 'FaceColor', 'interp', 'EdgeColor', 'none');

hold on
axis   off;
axis vis3d;
axis equal;

flat_patch.h2 = patch('Vertices', surf.pnt_patch, 'Faces', src.tri, 'FaceVertexCData', src.(param) ,...
  'FaceColor', 'interp', 'EdgeColor', 'none');

if ~isempty(mask)
  alpha(flat_patch.h2, src.(mask))
end %if

%% colormap and limits for functional data
if isempty(fun_lim) % maxabs
  fun_lim = max(abs(src.(param)));
  fun_lim = [-fun_lim fun_lim];
end %if
% color lims
caxis(fun_lim);
% color map
colormap(fun_color);
% colorbar
if col_bar
  colorbar
end %if

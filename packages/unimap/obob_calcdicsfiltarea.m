function [ grid ] = obob_calcdicsfiltarea(cfg, freq )
% CIMEC_CALCDICSFILT calculates the spatial filter using the DICS method at
% every timepoint and frequency bin in the freq data. It accepts freq data
% with either a CSD matrix or fourier spectra.
%
% Use this function as:
%    grid = obob_calcdicsfilt(cfg, freq)
%
% where grid is the resulting grid, containing the leadfields and the
% spatial filter. freq is the result of ft_freqanalysis called with a
% time-frequency method and cfg.output set to either 'powandcsd' or
% 'fourier'. In the first case, the data has to be processed with
% obob_convertcsd first!
%
% The configuration structure currently features the following options:
%
%   cfg.lf                = The leadfield structure.
%
%   cfg.frequency         = The limits of the frequencies of interest
%                           (example: cfg.frequency = [10 20];).
%
%   cfg.time              = The limits of the time-points of interest
%                           (example: cfg.time = [-.4 2.1];).
%
%   cfg.lambda            = The regularization parameter. Can be a constant
%                           value or can be given in percent of the trace
%                           of the CSD matrix. (default = 0).
%
%   cfg.realfilter        = If set to 'yes', use only the real part of the
%                           CSD matrix for filter calculation. This highly
%                           recommended as it speeds up calculation
%                           incredibly on shows nicer results. (default =
%                           'yes').
%
%   cfg.combineori        = The method used to combine the two/three
%                           orientations of the leadfield matrices. The
%                           best one is the 'fixedori' method that uses the
%                           power of the data to choose the best
%                           orientation. Alternativly you can use 'average'
%                           which takes the mean of the orientations, which
%                           is not recommended. (default = 'fixedori')
%

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% do some initialization...
ft_defaults
ft_preamble provenance
ft_preamble trackconfig

ft_checkconfig(cfg, 'lf', 'required');
ft_checkconfig(cfg, 'frequency', 'required');
ft_checkconfig(cfg, 'time', 'required');

cfg.lambda = ft_getopt(cfg, 'lambda', 0);
cfg.realfilter = ft_getopt(cfg, 'realfilter', 'yes');
cfg.feedback = ft_getopt(cfg, 'feedback', 'text');
cfg.combineori = ft_getopt(cfg, 'combineori', 'fixedori');

cfg.debug = ft_getopt(cfg, 'debug', 'no');

% check for labels in leadfield. if not present, make dummy labels...
hasdummylabels = false;
if ~isfield(cfg.lf, 'label_unimap');
  cfg.lf.label_unimap.name = containers.Map;
  % exclude outside voxels...
  % exclude all outside voxels...
  cfg.lf.pos = cfg.lf.pos(cfg.lf.inside, :);
  cfg.lf.leadfield = cfg.lf.leadfield(cfg.lf.inside);
  cfg.lf.inside = ones(1, length(cfg.lf.leadfield));
  for i=1:length(cfg.lf.pos)
    cfg.lf.label_unimap.name(num2str(i, '%06d')) = i;
  end %for
  hasdummylabels = true;
end %if

grid.pos = [];
grid.dim = cfg.lf.dim;
grid.inside = [];
grid.unit = cfg.lf.unit;
grid.label.name = {};
grid.label.mni_pos = [];
grid.label.all_pos = {};
grid.filter = {};

timeidx = nearest(freq.time, cfg.time(1)):nearest(freq.time, cfg.time(2));
grid.time = freq.time(timeidx);

freqidx = nearest(freq.freq, cfg.frequency(1)):nearest(freq.freq, cfg.frequency(2));
grid.freq = freq.freq(freqidx);

if length(timeidx) > 1 && length(freqidx) == 1
  grid.dimord = 'chan_time';
elseif length(timeidx) == 1 && length(freqidx) > 1
  grid.dimord = 'chan_freq';
elseif length(timeidx) > 1 && length(freqidx) > 1
  grid.dimord = 'chan_freq_time';
end %if

if ~hasdummylabels
  sides = {'left', 'right'};
  all_labels = keys(cfg.lf.label_unimap.name);
  cfg_tmp = [];
  for k=1:length(all_labels)
    for l=1:length(sides)
      cfg_tmp.labelstruct = cfg.lf.label_unimap;
      cfg_tmp.labels = all_labels{k};
      cfg_tmp.side = sides{l};
      idx{k, l} = obob_labelselection(cfg_tmp);
    end %for
  end %for
else
  sides = {'all'};
  all_labels = keys(cfg.lf.label_unimap.name);
  for k=1:length(all_labels)
    idx{k, 1} = cfg.lf.label_unimap.name(all_labels{k});
  end %for
end %if

ft_progress('init', cfg.feedback, 'Calculating filters...');

if strcmpi(cfg.debug, 'yes')
  tmp_filt = rand(3, length(freq.label));
end %if

for i=1:length(timeidx)
  for j=1:length(freqidx)
    
    ft_progress((j + (i-1)*length(freqidx))/length(timeidx)*length(freqidx), 'Calculating filter %d/%d for time: %f freq: %f\n', (j + (i-1)*length(freqidx)), length(timeidx) * length(freqidx), freq.time(timeidx(i)), freq.freq(freqidx(j)));
    if ~strcmpi(cfg.debug, 'yes')
      if isfield(freq, 'crsspctrm')
        csd = squeeze(freq.crsspctrm(timeidx(i), freqidx(j), :, :));
      else
        tmp = squeeze(freq.fourierspctrm(:, :, freqidx(j), timeidx(i)))';
        csd = (tmp * ctranspose(tmp)) / size(freq.fourierspctrm, 1);
        clear tmp;
      end %if
      if any(isnan(csd))
        fprintf('Skipping time %f, freq %f because CSD matrix is incomplete...\n', freq.time(timeidx(i)), freq.freq(freqidx(j)));
        continue;
      end %if
           
      if ischar(cfg.lambda) && cfg.lambda(end)=='%'
        ratio = sscanf(cfg.lambda, '%f%%');
        ratio = ratio/100;
        lambda = ratio * trace(csd)/size(csd,1);
      else
        lambda = cfg.lambda;
      end
      
      % estimate noise power.
      isrankdeficient = (rank(csd)<size(csd, 1));
      
      if isrankdeficient
        noise = lambda;
      else
        noise = svd(csd);
        noise = noise(end);
        noise = max(noise, lambda);
      end %if
      
      % calculate inverse csd...
      
      if strcmp(cfg.realfilter, 'yes')
        invCSD = pinv(real(csd + lambda * eye(size(csd))));
      else
        invCSD = pinv(csd + lambda * eye(size(csd)));
      end %if
    end %if
    
    this_filt = {};
    this_noise = {};
    
    for k=find(cfg.lf.inside(:)' == 1)
      if ~strcmpi(cfg.debug, 'yes')
        lf = cfg.lf.leadfield{k};
        tmp_filt = pinv(lf' * invCSD * lf) * lf' * invCSD;
        tmp_noise = noise * lambda1(tmp_filt * ctranspose(tmp_filt));
      end %if
      if strcmp(cfg.combineori, 'average')
        tmp_filt = mean(tmp_filt);
      elseif strcmp(cfg.combineori, 'fixedori')
        [u, s, v] = svd(real(tmp_filt * csd * ctranspose(tmp_filt)));
        maxpowori = u(:, 1);
        eta = s(1, 1) ./ s(2, 2);
        
        lf = lf * maxpowori;
        
        tmp_filt = pinv(lf' * invCSD * lf) * lf' * invCSD;
        tmp_noise = noise * lambda1(tmp_filt * ctranspose(tmp_filt));
      end %if
      this_filt{end+1} = tmp_filt;
      this_noise{end+1} = tmp_noise;
    end %for
    
    counter = 1;
    for k=1:length(all_labels)
      for l=1:length(sides)
        if isempty(idx{k, l})
          continue;
        end %if
        grid.label.name{counter} = [all_labels{k} '@' sides{l}];
        all_pos = cfg.lf.pos(idx{k, l}, :);
        grid.pos(counter, :) = mean(all_pos, 1);
        grid.label.all_pos{counter} = all_pos;
        if isfield(cfg.lf.label_unimap, 'mni_pos')
          all_orig_pos = cfg.lf.label_unimap.mni_pos(idx{k, l}, :);
          grid.label.mni_pos(counter, :) = mean(all_orig_pos, 1);
        end %if
        
        % add it to the inside field...
        grid.inside(counter) = size(grid.pos, 1);
        
        all_filt = this_filt(idx{k, l});
        tmp_filt = cat(1, all_filt{:});
        res_filt = mean(tmp_filt, 1);
        grid.filter{counter, j, i} = res_filt;
        
        all_noise = this_noise(idx{k, l});
        tmp_noise = cat(1, all_noise{:});
        res_noise = mean(tmp_noise, 1);
        grid.noise{counter, j, i} = res_noise;
        
        counter = counter + 1;
      end %for
    end %for
    
  end %for
end %for

ft_progress('close');

% clean up...
ft_postamble provenance
ft_postamble trackconfig

end

function [s, ori] = lambda1(x)
% determine the largest singular value, which corresponds to the power along the dominant direction
[u, s, v] = svd(x);
s   = s(1);
ori = u(:,1);
end


function [ src ] = obob_calcdicspow( cfg, freq)
% CIMEC_CALCDICSPOW beams the power at all time-frequency bins in the
% data using the supplied spatial filters.
%
% Use as:
%    src = obob_calcdicspow(cfg, freq)
%
% where freq is a time-frequency structure calculated with ft_freqanalysis
% using either cfg.output = 'powandcsd' or 'fourier'. In the first case,
% the data has to be processed with obob_convertcsd.
%
% The configuration structure currently features the following options:
%
%   cfg.lf                = The grid containing the leadfield and the
%                           spatial filters.

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% do some initialization...
ft_defaults
ft_preamble provenance
ft_preamble trackconfig

ft_checkconfig(cfg, 'lf', 'required');

cfg.output = ft_getopt(cfg, 'output', 'pow');
cfg.trials = ft_getopt(cfg, 'trials', 'all');

if ischar(cfg.trials) & strcmp(cfg.trials, 'all')
  cfg.trials = 1:size(freq.fourierspctrm, 1);
end %if

dopow = strcmp(cfg.output, 'pow');
dofourier = strcmp(cfg.output, 'fourier');

src.freq = cfg.lf.freq;
src.time = cfg.lf.time;
src.dimord = cfg.lf.dimord;

if ~iscell(cfg.lf.label.name)
  src.label = {};
  for i=1:size(cfg.lf.filter, 1)
    src.label{i} = ['Source ' num2str(i)];
  end %for
else
  src.label = cfg.lf.label.name;
end %if

if dopow
  power = zeros(size(cfg.lf.filter, 1), length(src.time), length(src.freq));
elseif dofourier
  power = complex(zeros(length(cfg.trials), size(cfg.lf.filter, 1), length(src.freq), length(src.time)));
  src.dimord = 'rpttap_chan_freq_time';
  src.cumtapcnt = freq.cumtapcnt;
end %if

ft_progress('init', 'text', 'Calculating power...');
for i=1:length(src.time)
  curtime = src.time(i);
  timeidx = nearest(freq.time, curtime);
  for j=1:length(src.freq)
    curfreq = src.freq(j);
    freqidx = nearest(freq.freq, curfreq);
    
    ft_progress((j + (i-1)*length(src.freq))/length(src.freq)*length(src.time), 'Calculating power %d/%d for time: %f freq: %f\n', (j + (i-1)*length(src.freq)), length(src.time) * length(src.freq), src.time(i), src.freq(j));
    if dopow
      if isfield(freq, 'crsspctrm')
        csd = squeeze(freq.crsspctrm(timeidx, freqidx, :, :));
      else
        tmp = squeeze(freq.fourierspctrm(cfg.trials, :, freqidx, timeidx))';
        csd = (tmp * ctranspose(tmp)) / size(freq.fourierspctrm, 1);
      end %if
      if ~any(isnan(csd))
        for k=1:size(power, 1)
          this_filt = cfg.lf.filter{k, j, i};
          tmp_csd = this_filt * csd * ctranspose(this_filt);
          power(k, j, i) = lambda1(tmp_csd);
        end %for
      else
        for k=1:size(power, 1)
          power(k, j, i) = nan;
        end %for
      end %if
    elseif dofourier
      tmp = squeeze(freq.fourierspctrm(cfg.trials, :, freqidx, timeidx))';
      
      if ~any(isnan(tmp))
        tmp_filt = [cfg.lf.filter{:, j, i}];
        tmp_filt = transpose(reshape(tmp_filt, [length(freq.label) size(power, 2)]));
        tmpfourier = tmp_filt * tmp;
        power(:, :, j, i) = transpose(tmpfourier);
      else
        power(:, :, j, i) = nan;
      end %if
    end %if
  end %for
end %for
ft_progress('close');

if dopow
  src.powspctrm = power;
elseif dofourier
  src.fourierspctrm = power;
end %if

% clean up...
ft_postamble provenance
ft_postamble trackconfig

end

function [s, ori] = lambda1(x)
% determine the largest singular value, which corresponds to the power along the dominant direction
[u, s, v] = svd(x);
s   = s(1);
ori = u(:,1);
end
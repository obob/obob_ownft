function [ grid_warped ] = obob_warpgrid( cfg )
%CIMEC_WARPGRID warps the grid specified in cfg.grid to an individual mri.
%
% IMPORTANT ADVISE: It is ABSOLUTELY necessary to take the utmost care of
% preparing the individuals MRI!!!! This function expects an MRI that has
% gone through the following steps (IN ORDER):
%
%     1. ft_volumereslice
%     2. ft_volumesegment (only needed for headmodels that need a segmented
%        MRI)
%     3. ft_volumerealign (a.k.a. co-registration)
%
% Take extreme care of the output of these three functions. Check the
% output of this procedure and make sure that, e.g. the segmented parts
% are, where you expect them to be.
%
%                        Always Remember
%                   Garbage In -> Garbage Out
%
%
% So, after having read all this, use it as
%     [grid_warped] = % obob_warpgrid(cfg);
%
% The configuration structure features the following:
%
%   cfg.mri               = The individual, volumeresliced, (segmented) and
%                           realigned MRI.
%
%   cfg.grid              = The original grid. This one gets morphed to
%                           individual brain.
%
%   cfg.nonlinear         = Do nonlinear morphing. This is highly
%                           recommended. (default = 'yes')

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% do some initialization...
ft_defaults
ft_preamble provenance
ft_preamble trackconfig

% check config structure...
ft_checkconfig(cfg, 'required', 'mri');
ft_checkconfig(cfg, 'required', 'grid');

cfg.nonlinear = ft_getopt(cfg, 'nonlinear', 'yes');

% morph the grid...
cfg_tmp = [];
cfg_tmp.grid.warpmni = 'yes';
cfg_tmp.grid.template = cfg.grid;
cfg_tmp.grid.nonlinear = cfg.nonlinear;
cfg_tmp.mri = cfg.mri;

grid_warped = ft_prepare_sourcemodel(cfg_tmp);
if isfield(cfg.grid, 'label')
  grid_warped.label = cfg.grid.label;
end %if

% clean up...
ft_postamble provenance
ft_postamble trackconfig



end


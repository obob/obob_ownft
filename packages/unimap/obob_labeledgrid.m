function [ grid ] = obob_labeledgrid(cfg)
% CIMEC_LABELEDGRID creates a dipole grid on a sourcemodel and introduces a
% new field that connects the grid points to the labels according to an
% atlas.
%
% Please be advised that this only makes sense on sourcemodels that are
% aligned to the atlas used. In short, this means:
%
%    1. Use a standard volume.
%    2. Normalize the headmodel.
%
% This function is written for the first use case. The resulting grid can
% later be morphed on an individual headmodel using (???).
%
% Use this function as:
%    grid = obob_labeledgrid(cfg)
%
% The configuration structure currently features the following options:
%
%   cfg.atlas             = Atlas to be used for label lookup. This can
%                           either be the filename of an atlas or an
%                           already prepared atlas (via the
%                           ft_prepare_atlas function).
%                           This function uses the fieldtrip functions for
%                           atlas lookup. So all atlases that work with
%                           fieldtrip should work with this function.
%                           (default = which('TTatlas+tlrc.BRIK'))
%
%   cfg.resolution        = Resolution of the grid that is to be laid
%                           through the sourcemodel in cm. (default = 1)
%
%   cfg.vol               = Forward Headmodel. This should be aligned to
%                           the atlas specified in cfg.atlas.
%
% The following are advanced options. Normally, you do not need to set
% them as defaults are ok.
%
%   cfg.queryrange        = Queryrange for the atlas lookup. If you use a
%                           very coarse grid, you might want to increase
%                           this one. (default = 1)
%
%   cfg.inputcoord        = Input coordinate system. Normally, these are
%                           mni coordinates. If you use talairach
%                           coordinates, set this to 'tal'. (default =
%                           'mni')
%
%   cfg.inwardshift       = "Inwardshift" in cm. Refer to
%                           ft_prepare_sourcemodel for details. The default
%                           value of -1.5 adds a little layer (of 1.5cm) of
%                           additional gridpoints to the grid. This is in
%                           most cases a sensible default. (default = -1.5)
%

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% do some initialization...
ft_defaults
ft_preamble provenance
ft_preamble trackconfig

% check config structure...
ft_checkconfig(cfg, 'required', 'vol');

cfg.atlas = ft_getopt(cfg, 'atlas', which('TTatlas+tlrc.BRIK'));
cfg.resolution = ft_getopt(cfg, 'resolution', 1);
cfg.queryrange = ft_getopt(cfg, 'queryrange', 1);
cfg.inputcoord = ft_getopt(cfg, 'inputcoord', 'mni');
cfg.inwardshift = ft_getopt(cfg, 'inwardshift', -1.5);

% now me make the grid...
cfg_tmp = [];
cfg_tmp.grid.xgrid = min(cfg.vol.bnd.pnt(:, 1))*1.2:cfg.resolution:max(cfg.vol.bnd.pnt(:, 1))*1.2;
cfg_tmp.grid.ygrid = min(cfg.vol.bnd.pnt(:, 2))*1.2:cfg.resolution:max(cfg.vol.bnd.pnt(:, 2))*1.2;
cfg_tmp.grid.zgrid = min(cfg.vol.bnd.pnt(:, 3))*1.2:cfg.resolution:max(cfg.vol.bnd.pnt(:, 3))*1.2;
cfg_tmp.grid.tight = 'yes';
cfg_tmp.inwardshift = cfg.inwardshift;
cfg_tmp.vol = cfg.vol;

grid = ft_prepare_sourcemodel(cfg_tmp);

% now we look if the atlas has to be prepared...
if ischar(cfg.atlas)
  cfg_tmp = [];
  cfg_tmp.atlas = cfg.atlas;
  cfg.atlas = ft_prepare_atlas(cfg_tmp);
end %if

% prepare label field of grid...
grid.label.name = containers.Map;
for i=1:length(cfg.atlas.descr.name)
  grid.label.name(cfg.atlas.descr.name{i}) = [];
end %for

grid.label.mni_pos = grid.pos;

% now we scan all the gridpoints to find the labels...
for i=1:length(grid.pos)
  temp = atlas_lookup(cfg.atlas, grid.pos(i, :)*10, 'inputcoord', cfg.inputcoord, 'queryrange', cfg.queryrange);
  
  for j=1:length(temp)
    temp2 = grid.label.name(temp{j});
    temp2(end+1) = i;
    grid.label.name(temp{j}) = temp2;
  end %for
end %for

% clean up...
ft_postamble provenance
ft_postamble trackconfig

end


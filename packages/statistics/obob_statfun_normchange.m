function [s] = obob_statfun_normchange(cfg, dat, design)

% obob_statfun_normchange computes the normalized change between two conditions;
% For conditions A and B, the calculation is (A - B) / (A + B). This is a statfun and should not be called directly.
%
% This statfun can only be used with monte carlo method
% if using 'clusterstat', data has to be thresholded from data (see
% example).
%
% might be specifically useful for source space testing 
%
%   Use this function by calling 
%   [stat] = ft_XXXstatistics(cfg, data1, data2)
%   with the following configuration options
%     cfg.method           = 'montecarlo'
%     cfg.statistic        = 'obob_statfun_normchange';
%     cfg.correctm         = 'cluster'; %or any other correctm. p-values then estimated from permutation
%     cfg.clusterthreshold = 'nonparametric_common';% or 'nonparametric_individual';
%	  cfg.lognorm = 'yes'; %or 'no'; (default: yes)
%
%   design specification:
%     cfg.ivar  = row number of the design that contains the labels of the conditions that must be 
%                 compared (default=1). The labels range from 1 to 2.
%
% See FT_FREQSTATISTICS, FT_TIMELOCKSTATISTICS, FT_SOURCESTATISTICS,
% and FT_STATISTICS_MONTECARLO for more details

% Ref (e.g.):
% Spaak, E., de Lange, F. P., & Jensen, O. (2014). 
% Local entrainment of alpha oscillations by visual stimuli causes cyclic modulation of perception. 
% J Neurosci, 34(10), 3536-3544. 

% Copyright (c) 2014-2016, Nathan Weisz, Philipp Ruhnau & Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.


%%
% check for correct inputs when method 'montecarlo' AND 'cluster' specified
% for correction

if isfield(cfg, 'method') & strcmp(cfg.method, 'analytic')
    error('obob_ownft:wrong_method', ['Prob cannot be determined using cfg.method = ''analytic''.\n']);
end

if isfield(cfg, 'correctm') & strcmp(cfg.correctm, 'cluster')
    if ~isfield(cfg, 'method') || ~strcmp(cfg.method, 'montecarlo')
      error('obob_ownft:wrong_method', ['This function can only be used with the monte carlo approach.\nCurrent method is: ' cfg.method]);
    end %if
    
    if ~isfield(cfg, 'clusterthreshold') || ~any([strcmp(cfg.clusterthreshold, 'nonparametric_common') strcmp(cfg.clusterthreshold, 'nonparametric_individual')])
       error('obob_ownft:missing_method', 'To use this function you need to specify cfg.clusterthreshold to estimate the threshold\n(either ''nonparametric_common'' or ''nonparametric_individual''');
    end %if
end

% check for incompatible options
cfg.computecritval='no';
cfg.computeprob='no';

if isfield(cfg, 'lognorm') && strcmpi(cfg.lognorm, 'yes')
  dat=log10(dat);
end

%%
selA = find(design(cfg.ivar,:)==1); % select condition 1 or A
selB = find(design(cfg.ivar,:)==2); % select condition 2 or B
dfA  = length(selA);
dfB  = length(selB);

if (dfA+dfB)<size(design, 2)
  % there are apparently replications that belong neither to condition 1, nor to condition 2
  warning('inappropriate design, it should only contain 1''s and 2''s');
end
% compute the averages ...
avgA = mean(dat(:,selA), 2);
avgB = mean(dat(:,selB), 2);
%...and then the normalized change
s_tmp = (avgA - avgB) ./ abs(avgA + avgB); %use abs because log can be negative

% the stat field is used in STATISTICS_MONTECARLO 
s = [];
s.stat = s_tmp;


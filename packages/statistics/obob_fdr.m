function pfdr=obob_fdr(pvals)

% CIMEC_FDR is a wrapper function to use matlabs mafdr function.
%
% Use as:
%   [pfdr] = obob_fdr(pvals);
%
% INPUT:
% pvals = vector or matrix (n-dim) with p-values obtained from some test.
%
% OUTPUT:
%   vector or matrix with same dimensionality with fdr-adjusted
%   (Benjami-Hochberg) p-values.
%

% Copyright (c) 2012-2016, Nathan Weisz & Philipp Ruhnau
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

dim = size(pvals);
pfdr = reshape(mafdr(pvals(:),'BHFDR', true), dim);




function newgradstruc=obob_adjust_tra(gradstruc)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% THIS FUNCTION IS HARMFULL TO YOUR DATA. YOU MUST NOT USE IT!!!!
% It is kept here for historical purposes and just throws an error message.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% obob_adjust_tra Takes gradiometer structure (e.g. data.grad) and replaces for all planar 
% gradiometers 1 --> 1/0.017 and -1 --> -1/0.017.
%
% ATTENTION ONCE FIELDTRIP UPDATES THE TRAs FOR NEUROMAG THIS FUNCTION WILL
% BECOME UNNECCESSARY. CHECK THE TRA FIELDS AFTER UPDATING FT.
%
% Use as
%   newgradstruc=obob_adjust_tra(gradstruc)
%
% where the first input argument is a grad structure.

% Copyright (c) 2010-2016, the OBOB group
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

error('obob_adjust_tra must not be used anymore with recent fieldtrip versions!!');

%%

[gradlabels maglabels indgrad indmag]=obob_selchans(gradstruc.label);

%%
tmp=gradstruc.tra(indgrad,:);
unique(tmp(:))

checktra=find(unique(tmp(:))==1)+find(unique(tmp(:))==-1);

%%
newgradstruc=gradstruc;
if ~isempty(checktra)
    disp('Correcting tra matrix')
    newgradstruc.tra(indgrad,:)=newgradstruc.tra(indgrad,:)/.017;
else
    disp('tra matrix appears to be adjusted')
end


function [varargout]=obob_mri4elekta(varargin)
%CIMEC_MRI4ELEKTA This function does not exist anymore. Please use
%obob_coregister

error(['The obob_mri4elekta function does not exist anymore. Please use the obob_coregister function from now on.' ...
  'If you supply an empty cfg.mrifile, that function will do exactly what obob_mri4elekta did.']);


end


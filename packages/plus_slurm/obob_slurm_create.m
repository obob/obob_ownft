function condor_struct = obob_slurm_create(cfg)
% obob_condor_create Configures the jobcluster to be run on the HNC Condor
% cluster.
%
% Call as:
%       condor_struct = obob_condor_create(cfg);
%
% Submitting jobs to the HNC Condor is a three step process:
% 1. Configure the parameters for the jobcluster, like how much memory you
%    need. (obob_condor_create)
% 2. Define the jobs to run. (obob_condor_addjob or
%    obob_condor_addjob_cell)
% 3. Submit the jobcluster. (obob_condor_submit)
%
% This function returns a new structure called "condor_struct". This structure
% is used by the other two functions. Basically, the create and addjob
% functions put information about what you want submitted into the
% condor_struct and the submit function then uses this function to do the
% actual submission. Please note that you do not have to bother with what
% is actually in the condor_struct. The structure is entirly internal.
%
% Please only be aware that obob_condor_addjob take the condor_struct as an
% input AND an output.
%
%
% ATTENTION: All files that you use (like functions, data etc.) need to be
% on the /mnt/obob share. Otherwise, the execution nodes cannot find it!
%
%
% cfg is a FieldTrip like config structure that provides the following
% options:
%
%    cfg.mem          = The amount of RAM required to run one job in the
%                       jobcluster. This needs to be a string (so something
%                       in quotes). cfg='200' means 200 megabyte. You can
%                       also use suffixes like "T", "G", "M" and "K" to
%                       request terabytes, gigabytes, megabytes or
%                       kilobytes. default = '2G';
%
%    cfg.cpus         = The amount of CPUs you want. Some operations can
%                       benefit from having more CPUs at their disposal.
%                       default = 1.
%
%    cfg.request_time = Maximum duration of a single job in minutes.
%                       default = 10.
%
%    cfg.exclude_nodes= Comma separated list of nodes to exclude.
%
%    cfg.java         = If you need java functions, set this to
%                       true. This will steal ca. 4GB of RAM for
%                       each job you submit. Normally you do not
%                       need it. default = false
%
%    cfg.jobsdir      = Folder to put all the jobs in. This one needs to be
%                       on the shared filesystem (so somewhere under
%                       /mnt/obob). default = 'jobs';
%
%    cfg.inc_jobsdir  = If this is set to true (default), cfg.jobsdir is
%                       the parent folder for all the jobs folders. Each
%                       time a job is submitted, a new folder is created in
%                       the cfg.jobsdir folder that contains all the
%                       necessary files and a folder called "log"
%                       containing the log files. If cfg.inc_jobsdir is set
%                       to false, the respective files are put directly
%                       under cfg.jobsdir. In this case, cfg.jobsdir must
%                       either be empty or not exist at all to avoid any
%                       side effects.
%
%    cfg.initial_dir  = Working directory on the execution machines.
%                       Default: current folder
%
%    cfg.matlab_exec  = Full path to matlab executable. Default: ['/usr/local/bin/matlab' version('-release')]
%
%    cfg.do_submit    = true (default) actually submits the jobs. false
%                       only creates all necessary files.
%
%
%
% Example:
% Let's suppose, we have a function called "myfunction" that takes two
% arguments: a and b. The function requires 4GB of RAM to run.
% We want this function to run on the HNC Condor.
%
% cfg = [];
% cfg.mem = '4G';
% 
% condor_struct = obob_condor_create(cfg);
%
% %We will create two jobs of the function. The first time it will run with
% %a=2; b=4. The second time with a=5; b=10;
% condor_struct = obob_condor_addjob(condor_struct, 'myfunction', 2, 4);
% condor_struct = obob_condor_addjob(condor_struct, 'myfunction', 5, 10);
%
% obob_condor_submit(condor_struct);

% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% initialize cfg

cfg.mem = ft_getopt(cfg, 'mem', '2G');
cfg.java = ft_getopt(cfg, 'java', false);
cfg.jobsdir = ft_getopt(cfg, 'jobsdir', 'jobs');
cfg.request_time = ft_getopt(cfg, 'request_time', 10);
cfg.inc_jobsdir = ft_getopt(cfg, 'inc_jobsdir', true);
cfg.cpus = ft_getopt(cfg, 'cpus', 1);
cfg.matlab_exec = ft_getopt(cfg, 'matlab_exec', fullfile(matlabroot, 'bin', 'matlab'));
cfg.initial_dir = ft_getopt(cfg, 'initial_dir', pwd);
cfg.exclude_nodes = ft_getopt(cfg, 'exclude_nodes', []);
cfg.do_submit = ft_getopt(cfg, 'do_submit', true);

%% setup condor_struct
condor_struct.cfg = cfg;
condor_struct.jobs = {};


end


function run_job(job_folder)
% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% load job data
slurm_task_id = getenv('SLURM_ARRAY_TASK_ID');
slurm_job_id = getenv('SLURM_ARRAY_JOB_ID');
mem_requested = str2double(getenv('SLURM_MEM_PER_NODE')) / 1024;

fname = fullfile(job_folder, 'slurm', sprintf('job%s.mat', slurm_task_id));

load(fname);

% if needed, add the path
if ~isempty(job_info.fun_path)
  addpath(job_info.fun_path);
end %if

fprintf('\n\n\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================');
fprintf('\n\n');
fprintf('Now running the function %s\n', job_info.f_name);
fprintf('Parameters:\n');
fprintf('%s\n', toString(job_info.args, 'compact'));
fprintf('\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n\n');

% run the function
start_tic = tic;
fun = str2func(job_info.f_name);

if is_classy_job(job_info.f_name)
  fun_obj = fun();
  fun_obj.set_arguments(job_info.args{:})
  fun_obj.run_private();
else
  fun(job_info.args{:});  
end %if
job_duration = toc(start_tic) / 60;

% display stats
proc_info = fileread('/proc/self/status');
tmp = regexp(proc_info, 'VmHWM:\s+(?<mem>\d+)', 'names');
mem_used = str2double(tmp.mem) / 1024 / 1024;

mem_toomuch = 100 * (mem_requested - mem_used) / mem_used;
% 
fprintf('\n\n\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================');
fprintf('\n\n');
fprintf('Your job ran for %.2f minutes\n', job_duration);
fprintf('Your job asked for %.2fGB of RAM\n', mem_requested);
fprintf('Your job used a maximum of %.2fGB of RAM\n\n', mem_used);
fprintf('You overestimated you memory usage by %.2f%%.\n', mem_toomuch);
fprintf('\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n\n');

end


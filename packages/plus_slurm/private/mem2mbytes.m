function [ mbytes ] = mem2mbytes( string )
%MEM2KBYTES converts a memory description as '10G' or '200M' to a number
% representing megabytes....

% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

if ~ischar(string)
  error('input must be a string.');
end %if

unit = string(end);
if ~isempty(str2num(unit))
  mbytes = str2num(string) / 1024 / 1024;
  return
end %if

string = string(1:end-1);

if strcmpi(unit, 'g')
  mbytes = str2num(string) * 1024;
elseif strcmpi(unit, 'm')
  mbytes = str2num(string);
elseif strcmpi(unit, 'k')
  mbytes = str2num(string) / 1024;
else
  error('unrecognized unit.');
end %if

end
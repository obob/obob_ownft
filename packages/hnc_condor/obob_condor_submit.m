function obob_condor_submit(condor_struct)
% obob_condor_submit Submits the jobcluster to the HNC Condor.
%
% Just supply the condor_struct that has been created with
% obob_condor_create and filled with obob_condor_addjob and the jobs will
% be submitted.
%
% For an example and further information, please refer to the help of the
% obob_condor_create and obob_condor_addjob functions.

% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

%% set condor_submit binary location
condor_submit = sprintf('LD_LIBRARY_PATH="" %s', condor_struct.cfg.condor_submit_path);

%% get cfg out of the struct...
cfg = condor_struct.cfg;

%% initialize empty submit file....
subfile = [];

%% convert jobsdir to absolute path if necessary....
if ~strcmp(cfg.jobsdir(1), '/')
  cfg.jobsdir = fullfile(pwd, cfg.jobsdir);
end %if

%% generate an internal job-number...
if cfg.inc_jobsdir
  % initialize jobsdir
  if ~exist(cfg.jobsdir, 'dir')
    mkdir(cfg.jobsdir);
  end %if

  % first we count how many things are in the directory....
  n = length(dir(cfg.jobsdir))-1;

  % now we check whether that folder already exists....
  while true
    job_folder = fullfile(cfg.jobsdir, sprintf('%03d', n));
    if ~exist(job_folder, 'dir')
      mkdir(job_folder);
      break;
    end %if
    n = n + 1;
  end %while
else
  job_folder = cfg.jobsdir;
  if exist(job_folder, 'dir')
    % check whether folder is empty
    if length(dir(job_folder)) > 2
      error('Your cfg.jobsfolder is not empty. Please choose a different folder or delete it.');
    end %if
  else
    mkdir(job_folder);
  end %if
end %if

mkdir(fullfile(job_folder, 'log'));
mkdir(fullfile(job_folder, 'condor'));

%% assemble the important information for the job and put it in a matfile...
for i = 1:length(condor_struct.jobs)
  job_info = condor_struct.jobs(i);

  [fun_path, ~, ~] = fileparts(which(job_info.f_name));
  if strcmp(fun_path(1), '/')
    job_info.fun_path = fun_path;
  else
    job_info.fun_path = [];
  end %if
   
  if is_classy_job(job_info.f_name)    
    job_class = str2func(job_info.f_name);
    job_object = job_class();
    job_object.set_arguments(job_info.args{:});
    
    if ~job_object.shall_run_private()
      continue;
    end %if   
  end %if

  save(fullfile(job_folder, 'condor', sprintf('job%d', i)), 'job_info');
end %for

%% copy the run_job function to the jobs folder....
[mpath, ~, ~] = fileparts(mfilename('fullpath'));
copyfile(fullfile(mpath, 'run_functions', '*'), fullfile(job_folder, 'condor'));
copyfile(fullfile(mpath, 'private', '*'), fullfile(job_folder, 'condor', 'private'));
copyfile(fullfile(mpath, 'obob_condor_job.m'), fullfile(job_folder, 'condor'));

%% prepare sub file....
matlabexec = cfg.matlab_exec;
matlabargs = {};
matlabargs{end+1} = '-nodesktop';
if cfg.cpus == 1
  matlabargs{end+1} = '-singleCompThread';
end %if 

if ~cfg.java
  matlabargs{end+1} = '-nojvm';
end %if

quotes = '''''';

matlabcommand = {};
matlabcommand{end+1} = 'try;';
matlabcommand{end+1} = ['addpath(' quotes fullfile(job_folder, 'condor') quotes ');'];
matlabcommand{end+1} = ['run_job(' quotes '$(filename)' quotes ');'];
matlabcommand{end+1} = 'catch err;disp(err.getReport());end;exit;';

matlabargs{end+1} = ['-r '''];
for i =1:length(matlabcommand)
  matlabargs{end} = [matlabargs{end} ' ' matlabcommand{i}];
end %for

matlabargs_final = [];
for i = 1:length(matlabargs)
  matlabargs_final = [matlabargs_final ' ' matlabargs{i}];
end %for
matlabargs_final = [matlabargs_final ''''];

subfile = {};
subfile{end+1} = sprintf('executable\t\t= %s', matlabexec');
subfile{end+1} = sprintf('arguments\t\t= "%s"', matlabargs_final);
subfile{end+1} = sprintf('log\t\t= %s', fullfile(job_folder, 'log', 'log'));
subfile{end+1} = sprintf('output\t\t= %s', fullfile(job_folder, 'log', 'out.$(Process)'));
subfile{end+1} = sprintf('error\t\t= %s', fullfile(job_folder, 'log', 'error.$(Process)'));
subfile{end+1} = sprintf('run_as_owner\t\t= True');
subfile{end+1} = sprintf('Requirements = Arch == "%s" && OpSys == "%s"', cfg.arch, cfg.opsys);
subfile{end+1} = sprintf('initialdir = %s', cfg.initial_dir);

% check for adjust_mem
if cfg.adjust_mem
  mem_mb = round(mem2mbytes(cfg.mem));
  subfile{end+1} = sprintf('request_memory\t\t= ifThenElse(MemoryUsage =!= UNDEFINED, (ifThenElse(MemoryUsage > %d, (MemoryUsage), %d)) * 20 / 17, %d)', mem_mb, mem_mb, mem_mb);
  subfile{end+1} = sprintf('periodic_hold\t\t= ifThenElse(MemoryUsage =!= UNDEFINED, ( MemoryUsage >= ( ( RequestMemory ) * 20 / 14 ) ), False)');
  subfile{end+1} = sprintf('periodic_release\t\t= (JobStatus == 5) && ((CurrentTime -EnteredCurrentStatus) > 20) && (HoldReasonCode == 3)');
else
  subfile{end+1} = sprintf('request_memory\t\t= %s', cfg.mem);
end %if


subfile{end+1} = sprintf('request_cpus\t\t= %d', cfg.cpus);
subfile{end+1} = sprintf('environment\t\t="PATH=/usr/bin HOME=/tmp/%s/"', char(java.util.UUID.randomUUID.toString));

if ~isempty(cfg.owner)
  subfile{end+1} = sprintf('+Owner\t\t= "%s"', cfg.owner);
end %if

subfile{end+1} = ['queue filename matching files ' fullfile(fullfile(job_folder, 'condor'), '*.mat')];

%% write subfile...
fid = fopen(fullfile(fullfile(job_folder, 'condor'), 'submit.sub'), 'wt');
for i = 1:length(subfile)
  fprintf(fid, '%s\n', subfile{i});
end %for

fclose(fid);

%% call submit...
cmd = sprintf('%s %s', condor_submit, fullfile(job_folder, 'condor', 'submit.sub'));
system(cmd);

end


classdef statdb < handle
  % Allows access to the stat database
  
  % Copyright (c) 2016, Thomas Hartmann
  %
  % This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
  %
  %    obob_ownft is free software: you can redistribute it and/or modify
  %    it under the terms of the GNU General Public License as published by
  %    the Free Software Foundation, either version 3 of the License, or
  %    (at your option) any later version.
  %
  %    obob_ownft is distributed in the hope that it will be useful,
  %    but WITHOUT ANY WARRANTY; without even the implied warranty of
  %    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  %    GNU General Public License for more details.
  %
  %    You should have received a copy of the GNU General Public License
  %    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
  %
  %    Please be aware that we can only offer support to people inside the
  %    department of psychophysiology of the university of Salzburg and
  %    associates.
  
  properties (Access = protected)
    dbid;
    dbname;
  end
  
  properties (Access = protected, Constant = true)
    table_name = 'condor_stats';
  end %properties
  
  methods (Access = public)
    function obj = statdb(dbname)
      obj.dbname = dbname;
      obj.dbid = mksqlite('open', obj.dbname);
      
      mksqlite(obj.dbid, sprintf(['CREATE TABLE IF NOT EXISTS %s(' ...
        'ID INTEGER PRIMARY KEY AUTOINCREMENT,' ...
        'DATETIME TEXT NOT NULL,', ...
        'USER TEXT NOT NULL,' ...
        'CLUSTERID INTEGER NOT NULL,' ...
        'PROCID INTEGER NOT NULL,' ...
        'USEDMEM FLOAT NOT NULL,' ...
        'REQMEM FLOAT NOT NULL,' ...
        'OVEREST FLOAT NOT NULL);'], obj.table_name));
    end %function
    
    function delete(obj)
      mksqlite(obj.dbid, 'close');
    end %function
    
    function addentry(obj, datetime, user, clusterid, procid, usedmem, reqmem, overest)
      mksqlite(obj.dbid, sprintf(['INSERT INTO %s ' ...
        '(DATETIME, USER, CLUSTERID, PROCID, USEDMEM, REQMEM, OVEREST) ' ...
        'VALUES(''%s'', ''%s'', %d, %d, %f, %f, %f);'], ...
        obj.table_name, datetime, user, clusterid, procid, usedmem, reqmem, overest));
    end %function
    
    function export_csv(obj, fname)
      data_tmp = mksqlite(obj.dbid, sprintf('SELECT * FROM %s;', obj.table_name));
      data_export = cell2table(struct2cell(data_tmp)', 'VariableNames', {'ID', 'Datetime', 'User', 'ClusterID', 'ProcID', 'UsedMem', 'ReqMem', 'OverEst'});
      writetable(data_export, fname);
    end %function
    
  end
  
end


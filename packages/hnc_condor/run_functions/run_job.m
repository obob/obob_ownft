function run_job(fname)
% Copyright (c) 2015-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% set db path...
dbpath = '/mnt/obob/misc/condor_db/condor_jobs.sqlite3';

% get jobad
jobadfn = getenv('_CONDOR_JOB_AD');

% load job data
load(fname);

% if needed, add the path
if ~isempty(job_info.fun_path)
  addpath(job_info.fun_path);
end %if

% get cluster and procid
clusterid = str2num(parse_jobad(jobadfn, 'ClusterId'));
procid = str2num(parse_jobad(jobadfn, 'ProcId'));

fprintf('\n\n\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================');
fprintf('\n\n');
fprintf('Now running the function %s\n', job_info.f_name);
fprintf('Parameters:\n');
fprintf('%s\n', toString(job_info.args, 'compact'));
fprintf('\n');
fprintf('This job has the following ID: %d.%d\n\n', clusterid, procid);
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n\n');

% run the function
fun = str2func(job_info.f_name);

if is_classy_job(job_info.f_name)
  fun_obj = fun();
  fun_obj.set_arguments(job_info.args{:})
  fun_obj.run_private();
else
  fun(job_info.args{:});  
end %if

% display stats
mem_used = str2num(get_classad('MemoryUsage', clusterid, procid))/1024;
mem_requested = str2num(get_reqmem(clusterid, procid))/1024;

if mem_used < 0.5
  mem_used = mem_requested;
end %if

owner = get_classad('Owner', clusterid, procid);

mem_toomuch = 100 * (mem_requested - mem_used) / mem_used;

fprintf('\n\n\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================');
fprintf('\n\n');
fprintf('Your job asked for %.2fGB of RAM\n', mem_requested);
fprintf('Your job used a maximum of %.2fGB of RAM\n\n', mem_used);
fprintf('You overestimated you memory usage by %.2f%%.\n', mem_toomuch);
fprintf('\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n');
fprintf('=========================================================\n\n');

% now we safe it to a database...
% first, restore default path to be sure we do not have any other paths
% here...
restoredefaultpath;

% now find out where we live....
[mypath, ~, ~] = fileparts(mfilename('fullpath'));

% readd paths
addpath(mypath);
addpath(fullfile(mypath, 'mksqlite'));

db=statdb(dbpath);
db.addentry(datestr(datetime, 31), owner, clusterid, procid, mem_used, mem_requested, mem_toomuch);

end


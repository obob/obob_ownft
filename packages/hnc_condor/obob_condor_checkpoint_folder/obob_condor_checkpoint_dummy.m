classdef obob_condor_checkpoint_dummy < obob_condor_checkpoint

% This is a private subclass of the checkpoint system. Do not use.

% Copyright (c) 2010-2016, Your Name(s) here
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

  
  methods (Access = protected)
    % just a stub to make the constructor protected.
    function obj = obob_condor_checkpoint_dummy(filename)
      
    end %function
  end %methods
  
  
  methods (Static = true)
    function init(cp_var)
      
      % call constructor and return the class.
      cp = obob_condor_checkpoint_dummy([]);
      assignin('caller', cp_var, cp);
    end %function
    
    
  end %methods
  
  methods (Access = public)
    % This implementation basically checks whether the input is valid. It
    % always returns true.
    function rval = subsref(obj, S)
      if ~strcmp(S.type, '()')
        error('Class can only be called with ().');
      end %if
      teststage = S.subs{1};
      
      if teststage < 1
        error('Checkpoint values must be > 0.');
      end %if
      rval = true;
    end %function
  end
  
end


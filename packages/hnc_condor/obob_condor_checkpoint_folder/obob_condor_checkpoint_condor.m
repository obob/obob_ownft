classdef obob_condor_checkpoint_condor < obob_condor_checkpoint

% This is a private subclass of the checkpoint system. Do not use.

% Copyright (c) 2010-2016, Your Name(s) here
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.
  
  properties (SetAccess = protected)
    filename
    tempfilename
    stage
  end %properties
  
  methods (Access = protected)
    % sets up the basic properties and deletes the tempfile if there is
    % still one.
    function obj = obob_condor_checkpoint_condor(filename)
      obj.filename = filename;
      [pathstr, name, ext] = fileparts(filename);
      obj.tempfilename = fullfile(pathstr, ['temp_' name ext]);
      obj.stage = 0;
      
      if exist(obj.tempfilename, 'file')
        delete(obj.tempfilename);
      end %if
      
    end %function
  end %methods
  
  methods
    % the destructor. deletes checkpoint files when matlab exits gracefully
    function delete(obj)
      if exist(obj.tempfilename, 'file')
        delete(obj.tempfilename);
      end %if
      
      if exist(obj.filename, 'file')
        delete(obj.filename);
      end %if
    end %function
    
  end %methods
  
  methods (Static = true)
    function init(cp_var)
      
      % get jobad filename...
      jobadfn = getenv('_CONDOR_JOB_AD');
      
      % extract clusterid an procid
      [clusterid, procid] = obob_condor_checkpoint_condor.jobad_parser(jobadfn);
      
      % construct filename
      filename = fullfile(obob_condor_checkpoint.cp_folder, sprintf('CP_%d.%d.mat', clusterid, procid));
      
      % see if checkpoint file exists. Load it if yes.
      loaded = false;
      if exist(filename, 'file');
        try
          evalin('caller', sprintf('load(''%s'')', filename));
          loaded = true;
        catch
          delete(filename);
        end %try
      end %if
      
      % If loading of the checkpoint file fails, create a new object and
      % pass it back.
      if ~loaded
        cp = obob_condor_checkpoint_condor(filename);
        assignin('caller', cp_var, cp);
      end %if
      
      [pathstr, name, ext] = fileparts(filename);
      tempfilename = fullfile(pathstr, ['temp_' name ext]);
      if exist(tempfilename, 'file')
        delete(tempfilename);
      end %if
    end %function
    
    function [ clusterid, procid ] = jobad_parser( jobad )
      % gets the jobs classad file and parses it for clusterid and procid
      
      fid = fopen(jobad, 'rt');
      tline = fgetl(fid);
      while ischar(tline)
        [tok, remain] = strtok(tline);
        if strcmp(tok, 'ClusterId')
          clusterid = str2num(remain(3:end));
        elseif strcmp(tok, 'ProcId')
          procid = str2num(remain(3:end));
        end %if
        
        tline = fgetl(fid);
      end %while
      
      fclose(fid);
      
    end
    
    
  end %methods
  
  methods (Access = public)
    % in order to provide a good interface, we abuse the array referencing
    % convention because it looks like calling a function in matlab. what
    % this method must do is:
    % 1. return true if the following code should be executed and save the
    % current status.
    % 2. return false if the following code should not be executed. do not
    % save in this case!
    function rval = subsref(obj, S)
      % check if input is valid
      if ~strcmp(S.type, '()')
        error('Class can only be called with ().');
      end %if
      teststage = S.subs{1};
      
      if teststage < 1
        error('Checkpoint values must be > 0.');
      end %if
      
      % check whether we are at a new checkpoint
      if teststage >= obj.stage
        obj.stage = teststage;
        whos_structure = evalin('caller', 'whos');
        if ~obj.canSave(whos_structure)
          warning('Cannot save checkpoint...');
        else
          evalin('caller', sprintf('save(''%s'', ''*'', ''-v7.3'');', obj.tempfilename));
          movefile(obj.tempfilename, obj.filename);
        end %if
        rval = true;
      else
        rval = false;
      end %if
    end %function
  end
  
end


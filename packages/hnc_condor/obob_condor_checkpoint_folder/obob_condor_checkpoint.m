classdef (Abstract) obob_condor_checkpoint < handle

% OBOB_CONDOR_CHECKPOINT provides so-called checkpointing to cluster jobs.
%
% When jobs are run on the cluster, there is no guarantee that they will
% actually finish. The machine, the job is running on might crash or the
% scheduler decides that the job needs to stop running on a given machine.
% In both cases, the jobs get reinserted into the queue so they will start
% running again after a some minutes. This is not a problem for jobs
% running just a few minutes or hours. But if you run jobs that take a very
% long time, like 5 hours or even days, this behavior can be quite
% frustrating and time consuming.
% This is where checkpoints come in handy. Checkpoints basically save the
% current state of your job at crucial steps. If the job restarts, it will
% load the previously saved state and start running again.
%
% Putting checkpoints in your jobs is rather easy. However, please be
% advised that the whole Matlab session is saved to our storage. So, if you
% have 10 jobs running and each of them uses 50GB of RAM, they will take up
% half a terabyte (500GB!) of disk space when checkpointing. And this
% number will be doubled at times because old checkpoints are deleted only
% when new checkpoints have been safely written.
%
% So, please use this system with caution!!!
%
% Here is how it works:
%
% 1. At the beginning of your function, include a call like this:
%
%    cp_init = obob_condor_checkpoint.getInit();
%    cp_init('cp');
%
%    This will create a new variable in the workspace called 'cp'. You can
%    call it whatever you like.
%
% 2. Now you need to put the rest of your code into 'if cp(x)' clauses like
%    this:
%
% %% do first checkpoint stuff
% if cp(1)
%   clear temp;
%   fprintf('doing first checkpoint stuff\n');
%   temp = rand(2000, 1);
% end %if
% 
% %% do second checkpoint.....
% if cp(2)
%   x = 2;
%   fprintf('doing second checkpoint stuff\n');
%   temp = temp * 2;
% end %if
% 
% %% do third checkpoint
% if cp(3)
%   fprintf('doing third checkpoint stuff\n');
% end %if
%
% 3. If you look at the code, you will notice:
%
% 3.1. Every block of code is enclosed in an if clause. You cannot have
% code outside of the if clauses!
%
% 3.2. The numbers cp is called with are strictly incrementing.
%
% That's it. Now you have checkpoints. Everything else is done
% automatically. The system also checks whether the function actually runs
% on the cluster. If it does not, the code still works but does not create
% checkpoints.

% Copyright (c) 2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

  
  properties (Constant = true)
    cp_folder = '/mnt/obob/tmp/condor_checkpoints';
  end %properties
   
  methods (Access = protected)
    % just a stub to make the constructor protected.
    function obj = obob_condor_checkpoint()
    end %function
    
    % this function evaluates if a checkpoint can safely be saved.
    % in order to checkpoint, the total number of data must not exceed 32GB
    % and there must be at least 700GB of space left on the storage
    function rval = canSave(obj, whos_structure)
      mem_sum = sum([whos_structure.bytes]);
      mem_gb = mem_sum / 1024 / 1024 / 1024;
      
      [~, df] = system('df | grep /mnt/obob');
      df_text = textscan(df, '%s');
      free_num = str2num(df_text{1}{4});
      free_num = free_num / 1024 / 1024;
      
      rval = false;
      if free_num > 700 && mem_gb <= 32
        rval = true;
      end %if
    end %function
    
  end %methods
   
  methods (Static = true)
    function fun = getInit()
      if ~isempty(getenv('_CONDOR_JOB_AD'))
        fun = @obob_condor_checkpoint_condor.init;
      else
        fun = @obob_condor_checkpoint_dummy.init;
      end %if
    end %function
  end %methods
  
  methods (Static = true, Abstract)
    
    init(cp_var)  
    
  end %methods
  
  methods (Access = public, Abstract = true)
    % in order to provide a good interface, we abuse the array referencing
    % convention because it looks like calling a function in matlab. what
    % this method must do is:
    % 1. return true if the following code should be executed and save the
    % current status.
    % 2. return false if the following code should not be executed. do not
    % save in this case!
    rval = subsref(~, ~)
  end
  
end


function cfg = obob_init_ft(cfg, tests)
% obob_init_ft initializes the official OBOB FieldTrip distribution and
% the official and tested functions provided by the MEG team. If you run
% into any problems, please contact us providing all error messages you
% received.
%
% You can call this function as:
%     obob_init_ft;
%
% This will initialize the OBOB FT distribution with the default settings.
% In normal cases, this will be all you would ever have to do. The function
% will detect automatically, if you are on some cluster headnode, computing
% node or whatever and initialize more functions accordingly.
%
% Please be aware that the initialization includes resetting the matlab
% path. Please use the init functions of the respective toolboxes or
% addpath to include what you need.
%
% However, you can also call the function as:
%     obob_init_ft(cfg);
%
% You would do this in order to provide additional configuration options.
% If, e.g., you would want to use a different FieldTrip distribution or
% some special or experimental functions, this is what you would want to
% use.
%
% You can also call the function requesting a return value:
%     obob_cfg = obob_init_ft();
% or:
%     obob_cfg = obob_init_ft(cfg);
%
% This will return the configuration structure.
%
% Configuration options can be as follows:
%
%   cfg.package.gm2           = Set to true if you want to use the GraphMapping2
%                               toolbox. (default = false)
%
%   cfg.package.unimap        = Set to true if you want to use the Universal
%                               Mapping toolbox. (default = false)
%
%   cfg.package.svs           = Set to true if you want to use the simple
%                               virtual sensor functions. (default = false)
%
%   cfg.package.hnc_invincible= Set to true if you want to use the HNC
%                               Invincible related functions. This package
%                               gets added automatically when you are on
%                               the cluster. You can also use these
%                               functions on other computers but the code
%                               will be executed locally instead.
%
%   cfg.package.obsolete      = Set to true if you want to use some otherwise
%                               obsolete functions. (default = false)
%
%   cfg.experimental          = Set to true if you want to use experimental
%                               functions. (default = false)
%
%   cfg.no_override           = By default, some fieldtrip functions are exchanged
%                               for custom functions. If you do not want this
%                               behavior, you can turn it off here by setting this
%                               option to true. (default = false)
%
%   cfg.ft_path               = The path of the FieldTrip distribution used. By
%                               default, the official version is used. If you want
%                               to use a different one, you can specify it here.
%                               (default = [])
%
%   cfg.bct_path              = The path to the Brain Connectivity Toolbox
%                               you want to use. By default, the official
%                               one is zsed. If you want to use a different
%                               one, you can specify it here. (default = [])
%
%   cfg.verbose               = Show verbose output. When set to false,
%                               no output is printed to the commmand
%                               window. (default=true).
%
%   cfg.testdata_folder       = Path to the testdata. If this is not set,
%                               it will default to 'obob_testdata' in your
%                               home folder. If the folder is not present
%                               or the data in there is missing our
%                               outdated, it will be downloaded
%                               automatically.

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

% set some constants...
local_obob_ownft = {};
local_obob_ownft{1} = '/home/thomas.hartmann/obob_ownft_local';

obob_init_ft_folder = 'obob_init_ft_function';

% add private functions...
private_dir = fullfile(fileparts(mfilename('fullpath')), 'private_init');
addpath(private_dir);
addpath(fullfile(fileparts(mfilename('fullpath')), 'packages', 'utilities'));

% check for cfg....
if nargin == 0
  cfg = [];
end %if

if nargin < 2
  tests = [];
end %if

% determine where we are...
cfg_tmp = obob_getLocation([], true);
on_remote = obob_isOnRemote();

if (cfg_tmp.location.matserver || cfg_tmp.location.matserver2) && on_remote
  fprintf('Trying to divert you to the locally stored obob_ownft distribution...\n\n');
  for i = 1:length(local_obob_ownft)
    if exist(fullfile(local_obob_ownft{i}, obob_init_ft_folder))
      fprintf('Diverting to %s\n', fullfile(local_obob_ownft{i}, obob_init_ft_folder));
      addpath(fullfile(local_obob_ownft{i}, obob_init_ft_folder));
      cfg = obob_init_ft_function(cfg);
      if nargout == 0
       clear cfg;
      end %if
      return
    end %if
  end %for
  
  fprintf('Diversion failed...\n');
end %if

addpath(fullfile(fileparts(mfilename('fullpath')), obob_init_ft_folder));
cfg = obob_init_ft_function(cfg, tests);

if nargout == 0
  clear cfg;
end %if

%% remove private path...
if ~isempty(which('obob_get_owner'))
  rmpath(private_dir);
end %if

end


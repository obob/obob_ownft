%% clear...
clear all global
close all

%% init....
cfg = [];
cfg.package.svs = true;

obob_init_ft(cfg);

%% load some testdata....
testfolder = 'tests/unittest/tests/data';

load(fullfile(testfolder, 'data_preproc.mat'));
load(fullfile(testfolder, 'headmodel.mat'));
load(fullfile(testfolder, 'test_mri_seg.mat'));

%% do some converting....
data.grad = ft_convert_units(data.grad, 'm');
mni_grid = ft_convert_units(mni_grid, 'm');

%% take a look at the data...
cfg = [];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 35;
cfg.preproc.bsfilter = 'yes';
cfg.preproc.bsfreq = [16 18];

data_tl = ft_timelockanalysis(cfg, data);

%% look at mags....
cfg = [];
cfg.channel = 'MEGMAG';
cfg.layout = 'neuromag306mag.lay';

figure; ft_multiplotER(cfg, data_tl);

%% get lfs for old svs....
cfg = [];
cfg.grid = grid;
cfg.vol = vol;
cfg.normalize = 'yes';

lf = ft_prepare_leadfield(cfg, data);

%% do old svs...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 35;
cfg.grid = lf;
cfg.fixedori = 'yes';

svs = obob_svs_beamtrials_lcmv(cfg, data);

%% postproc svs...
std_mri = load('standard_mri');

cfg = [];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 35;
cfg.preproc.bsfilter = 'yes';
cfg.preproc.bsfreq = [16 18];

svs_tl = ft_timelockanalysis(cfg, svs);

cfg = [];
cfg.baseline = [-.3 -.1];
cfg.baselinetype = 'relchange';

svs_tl_bl = obob_svs_timelockbaseline(cfg, svs_tl);

cfg = [];
cfg.sourcegrid = mni_grid;
cfg.parameter = 'avg';
cfg.latency = [0.1 0.14];
cfg.mri = std_mri.mri;

svs_interpolated = obob_svs_virtualsens2source(cfg, svs_tl_bl);

%% plot....
cfg = [];
cfg.funparameter = 'avg';

ft_sourceplot(cfg, svs_interpolated);

%% do it with parcellation...
% first create the parcellation...

%cfg = [];
%cfg.resolution = 3e-3;

%parcellation = obob_svs_create_parcellation(cfg);

load parcellations_3mm.mat

%% morph template grid to mri....
cfg = [];
cfg.mri = mri;
cfg.grid.warpmni = 'yes';
cfg.grid.template = parcellation.template_grid;
cfg.grid.nonlinear = 'yes';
cfg.grid.unit = 'm';

subject_grid = ft_prepare_sourcemodel(cfg);

%% create leadfield...
cfg = [];
cfg.grid = subject_grid;
cfg.vol = vol;

lf = ft_prepare_leadfield(cfg, data);

%% do svs with parcels...
cfg = [];
cfg.grid = lf;
cfg.parcel = parcellation;
cfg.lpfilter = 'yes';
cfg.lpfreq = 35;
cfg.fixedori = 'yes';
cfg.parcel_avg = 'avg_filters';

svs_parcel_th.avg_filters = obob_svs_beamtrials_lcmv(cfg, data);

%% do svs with parcels...
cfg = [];
cfg.grid = lf;
cfg.parcel = parcellation;
cfg.lpfilter = 'yes';
cfg.lpfreq = 35;
cfg.fixedori = 'no';
cfg.parcel_avg = 'svd_sources';

svs_parcel_th.svd_sources = obob_svs_beamtrials_lcmv(cfg, data);

%% postproc svs...
std_mri_seg = load('standard_mri_segmented');

cfg = [];
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 35;
cfg.preproc.bsfilter = 'yes';
cfg.preproc.bsfreq = [16 18];

cfg2 = [];
cfg2.function = @(x) ft_timelockanalysis(cfg, x);

svs_parcel_th_tl = obob_apply_to_ft_structs(cfg2, svs_parcel_th);

cfg = [];
cfg.baseline = [-.3 -.1];
cfg.baselinetype = 'absolute';
cfg.keeppolarity = true;

cfg2 = [];
cfg2.function = @(x) obob_svs_timelockbaseline(cfg, x);

svs_parcel_th_tl_bl = obob_apply_to_ft_structs(cfg2, svs_parcel_th_tl);

cfg = [];
cfg.sourcegrid = parcellation.parcel_grid;
cfg.parameter = 'avg';
cfg.latency = [0.1 0.14];
cfg.mri = std_mri_seg.mri_seg.bss;

cfg2 = [];
cfg2.function = @(x) obob_svs_virtualsens2source(cfg, x);

svs_parcel_th_interpolated = obob_apply_to_ft_structs(cfg2, svs_parcel_th_tl_bl);
svs_parcel_th_interpolated.avg_filters.coordsys = 'spm';
svs_parcel_th_interpolated.svd_sources.coordsys = 'spm';

%% plot....
load atlas_parcel333.mat

cfg = [];
cfg.funparameter = 'avg';
cfg.maskparameter = 'brain_mask';
cfg.atlas = atlas;

ft_sourceplot(cfg, svs_parcel_th_interpolated.avg_filters);
ft_sourceplot(cfg, svs_parcel_th_interpolated.svd_sources);

%% plot...
cfg = [];
cfg.layout = parcellation.layout;
cfg.xlim = [-.2 .5];

figure; ft_multiplotER(cfg, svs_parcel_th_tl_bl.avg_filters);
figure; ft_multiplotER(cfg, svs_parcel_th_tl_bl.svd_sources);

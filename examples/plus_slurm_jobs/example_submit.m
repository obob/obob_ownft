%% clear
clear all global
close all

%% init obob_ownft
addpath('../../');

cfg = [];
cfg.package.plus_slurm = true;

obob_init_ft(cfg);

%% set variables...
argument1 = {1, 3, 6, 7};
argument2 = {2, 3};

%% prepare submit
cfg = [];
cfg.mem = '200M';
cfg.do_submit = false;

slurm_struct = obob_slurm_create(cfg);

slurm_struct = obob_slurm_addjob_cell(slurm_struct, 'example_job', argument1, argument2);

%% submit
obob_slurm_submit(slurm_struct);
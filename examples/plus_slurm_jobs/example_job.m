classdef example_job < obob_slurm_job
  methods
    function run(obj, argument1, argument2)      
      result = argument1 + argument2;
      
      pause(80);
      
      save(obj.get_output_filename(argument1, argument2), 'result');
      
    end %function
    
    function run = shall_run(obj, argument1, argument2)
      run = ~exist(obj.get_output_filename(argument1, argument2), 'file');
    end %function
    
    function fname = get_output_filename(obj, argument1, argument2)
      fname = fullfile('/mnt/obob/tmp', sprintf('test_%d_%d.mat', argument1, argument2));
    end %function
    
  end %methods
end


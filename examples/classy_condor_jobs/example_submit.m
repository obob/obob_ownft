%% clear
clear all global
close all

%% init obob_ownft
addpath('/mnt/obob/staff/thartmann/git/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% set variables...
argument1 = {1, 3, 6, 7};
argument2 = {2, 3};

%% prepare submit
cfg = [];
cfg.mem = '2G';
cfg.adjust_mem = true;

condor_struct = obob_condor_create(cfg);

condor_struct = obob_condor_addjob_cell(condor_struct, 'example_job', argument1, argument2);

%% submit
obob_condor_submit(condor_struct);
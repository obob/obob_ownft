function obob_welcome()
% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

welcomestring = ['\n\nWelcome to the OBOB_OWNFT Distribution!\n' ...
  'Please make yourself familiar with our new package system.\n' ...
  'Also check out the new cfg options to fine tune your experience!\n\n' ...
  'For help, please type:\n' ...
  '\t help obob_init_ft\n\n' ...
  'in the Matlab command prompt.\n' ...
  'In case of problems and bugs, write the MEG team a nice email and we will be happy to help you.\n\n\n'];

fprintf(welcomestring);


end


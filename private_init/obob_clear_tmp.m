function obob_clear_tmp(limit_days)
%OBOB_CLEAR_TMP deletes all clearly fieldtrip related stuff older than
%limit_days from the temporary folder.

% Copyright (c) 2012-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

if nargin < 1
  limit_days = 1;
end %if

globs = {'*.hdr', '*.img'};

for i = 1:length(globs)
  allfiles = dir(fullfile(tempdir, globs{i}));
  
  for j = 1:length(allfiles)
    if (now - allfiles(j).datenum) > limit_days
      if strcmp(obob_get_owner(fullfile(tempdir, allfiles(j).name)), getusername())
        try
          delete(fullfile(tempdir, allfiles(j).name));
        catch
        end %try
      end %if
    end %if
  end %for
end %for


end


%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% next we add the folder in which our functions are...
addpath('functions');

%% so, lets initialize some variables. for instance, we need to specify,
% where the preprocessed data lies and where the clean data should go...

indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/02cleaned';
outdir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/03timelocked'; % CHANGE THIS!!!

%% get all subjects...
all_files = dir(fullfile(indir, '*.mat'));

%% now we call the function to read in our data...
for i = 1:length(all_files)
  [~, subject] = fileparts(all_files(i).name);
  tutorial_03_timelockanalysis(subject, indir, outdir);
end %for
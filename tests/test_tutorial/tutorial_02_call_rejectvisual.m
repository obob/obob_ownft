%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% so, lets initialize some variables. for instance, we need to specify,
% where the preprocessed data lies and where the clean data should go...

indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/01preprocessed';
outdir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/02cleaned'; % CHANGE THIS!!!

%% lets define, which subject to do...
subject = 'rt';

%% load the data...
load(fullfile(indir, subject));

%% perform reject visual on all runs...
cfg = [];
cfg.downsample = 200;
cfg.preproc.lpfilter = 'yes';
cfg.preproc.lpfreq = 40;
cfg.layout = 'neuromag306mag.lay';
cfg.method = 'summary';
cfg.showlabel = 'yes';
cfg.channel = 'MEG';
cfg.magscale = 25;

for i=1:length(data)
  data{i} = cimec_rejectvisual(cfg, data{i});
end %for

%% concatenate data...
data = ft_appenddata([], data{:});

%% save data...
mkdir(outdir);
save(fullfile(outdir, subject), 'data', '-v7.3');
%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% next we add the folder in which our functions are...
addpath('functions');

%% so, lets initialize some variables. for instance, we need to specify,
% where the preprocessed data lies and where the clean data should go...

indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/02cleaned';
outdir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/05freqanalysis'; % CHANGE THIS!!!

%% now we call the function to read in our data...
subject = 'rt';
tutorial_05_freqanalysis(subject, indir, outdir);
%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% so, lets initialize some variables. for instance, we need to specify,
% where the preprocessed data lies and where the clean data should go...

indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/03timelocked';

%% load data...
subject = 'el';
load(fullfile(indir, subject));

%% now we plot both conditions...
cfg = [];
cfg.baseline = [-.5 0];
cfg.baselinetype = 'relative';
cfg.interactive = 'yes';
cfg.layout = 'neuromag306mag.lay';

ft_multiplotER(cfg, data_timelocked.left, data_timelocked.right);

%% we can use ft_math to get the difference....
cfg = [];
cfg.parameter = 'avg';
cfg.operation = 'subtract';

data_dif = ft_math(cfg, data_timelocked.left, data_timelocked.right);

%% plot it...
cfg = [];
%cfg.baseline = [-.5 0];
%cfg.baselinetype = 'relative';
cfg.interactive = 'yes';
cfg.layout = 'neuromag306mag.lay';

ft_multiplotER(cfg, data_dif);
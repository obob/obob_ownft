%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% set variables...
headmodel_dir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/07headmodels';
subject = 'el';
raw_dir = '/mnt/storage/tier1/natwei/Shared/thartmann/cimec_ownft_new/tutorial/data/raw';

all_datafiles = dir(fullfile(raw_dir, subject, '*.fif'));

%% use cimec_coregister to coregister the MRI to the headshape...
cfg = [];
cfg.mrifile = fullfile(raw_dir, subject, 'mri', 'Image0001.dcm');
cfg.headshape = fullfile(raw_dir, subject, all_datafiles(1).name);

mri = cimec_coregister(cfg);

%% load segmented standard brain...
load mni_seg

%% segment individual mri...
mri_seg = ft_volumesegment([], mri);

%% put segmentation back into mri....
mri.gray = mri_seg.gray;
mri.white = mri_seg.white;
mri.csf = mri_seg.csf;

%% plot coregistered mri...
cfg = [];
cfg.funparameter = 'white';
cfg.interactive = 'yes';

ft_sourceplot(cfg, mri);

%% make headmodel for individual brain...
cfg = [];
cfg.method = 'singleshell';
vol = ft_prepare_headmodel(cfg, mri);

%% make headmodel for segmented mni brain...
cfg = [];
cfg.method = 'singleshell';

mni_headmodel = ft_prepare_headmodel(cfg, mni_seg);

%% make grid on mni brain...
minbnd = min(mni_headmodel.bnd.pnt);
maxbnd = max(mni_headmodel.bnd.pnt);
cfg = [];

cfg.grid.unit = 'mm';
cfg.vol = mni_headmodel;
cfg.xgrid = minbnd(1)*1.2:10:maxbnd(1)*1.2;
cfg.ygrid = minbnd(2)*1.2:10:maxbnd(2)*1.2;
cfg.zgrid = minbnd(3)*1.2:10:maxbnd(3)*1.2;
mni_grid = ft_prepare_sourcemodel(cfg);
mni_grid = ft_convert_units(mni_grid, 'mm');

%% plot grid....
figure;
ft_plot_vol(mni_headmodel, 'edgecolor', 'none');
alpha(.4);
hold on;
ft_plot_mesh(mni_grid.pos(mni_grid.inside, :));

%% morph grid to individual mri...
cfg = [];
cfg.grid.warpmni = 'yes';
cfg.grid.template = mni_grid;
cfg.mri = mri;
cfg.grid.nonlinear = 'yes';

grid_subject = ft_prepare_sourcemodel(cfg);
grid_subject = ft_convert_units(grid_subject, 'mm');

%% plot grid....
figure;
ft_plot_vol(vol, 'edgecolor', 'none');
alpha(.4);
hold on;
ft_plot_mesh(grid_subject.pos(grid_subject.inside, :));

%% save headmodel...
mkdir(headmodel_dir);
save(fullfile(headmodel_dir, subject), 'grid_subject', 'mni_grid', 'vol', 'mni_headmodel', 'mri');

%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% init variables...
indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/08lcmv_nomri';

subject = 'el';

%% load data
load(fullfile(indir, subject));
load mni_mri;

%% first calculate the neural activity index for all...
all_conditions = fieldnames(data_projected);

for i = 1:length(all_conditions)
  data_projected.(all_conditions{i}).avg.nai = data_projected.(all_conditions{i}).avg.pow ./ data_projected.(all_conditions{i}).avg.noise;
end %for

%% first look at the pure erp...
condition = 'right';
data_relchange = data_projected.(condition);
data_relchange.avg.nai = (data_projected.(condition).avg.nai - data_projected.baseline.avg.nai) ./ data_projected.baseline.avg.nai;

cfg = [];
cfg.parameter = 'avg.nai';

source = ft_sourceinterpolate(cfg, data_relchange, mni_mri);
source.avg.mask = source.avg.nai >= quantile(source.avg.nai(:), .9);

%% plot..
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'avg.nai';
cfg.maskparameter = 'avg.mask';
cfg.interactive = 'yes';
cfg.atlas = which('TTatlas+tlrc.BRIK');

source.coordsys = 'mni';

ft_sourceplot(cfg, source);

%% now look at the difference between left and right...
field = 'nai';
data_relchange = data_projected.left;
data_relchange.avg.data = (data_projected.left.avg.(field) - data_projected.right.avg.(field)) ./ data_projected.left.avg.(field);

cfg = [];
cfg.parameter = 'avg.data';

source = ft_sourceinterpolate(cfg, data_relchange, mni_mri);
source.avg.mask = abs(source.avg.data) >= quantile(abs(source.avg.data(:)), .9);

%% plot..
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'avg.data';
cfg.maskparameter = 'avg.mask';
cfg.interactive = 'yes';
cfg.atlas = which('TTatlas+tlrc.BRIK');

source.coordsys = 'mni';

ft_sourceplot(cfg, source);

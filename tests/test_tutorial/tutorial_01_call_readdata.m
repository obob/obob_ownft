%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% next we add the folder in which our functions are...
addpath('functions');

%% so, lets initialize some variables. for instance, we need to specify,
% where the raw data lies and where the final data should go...

indir = '/mnt/storage/tier1/natwei/Shared/thartmann/cimec_ownft_new/tutorial/data/raw';
outdir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/01preprocessed'; % CHANGE THIS!!!

%% now we call the function to read in our data...
subject = 'rt';
tutorial_01_readdata(subject, indir, outdir);
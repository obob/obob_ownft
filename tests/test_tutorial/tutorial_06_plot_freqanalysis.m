%% It is always a good idea to start with a new environment...
clear all global
close all

%% First thing to do in a script: initialize cimec_ownft...
addpath('../');

cimec_init_ft;

%% so, lets initialize some variables. for instance, we need to specify,
% where the preprocessed data lies and where the clean data should go...

indir = '/mnt/storage/erc-win2con/thomas/experiments/tut_data/05freqanalysis';

%% load data...
subject = 'rt';
load(fullfile(indir, subject));

%% plot
plot_condition = 'left';

cfg = [];
cfg.baseline = [-1 -.2];
cfg.baselinetype = 'relchange';
cfg.interactive = 'yes';
cfg.layout = 'neuromag306mag.lay';
cfg.zlim = 'maxabs';

ft_multiplotTFR(cfg, data_tfr.(plot_condition));

%% plot relative change between the two conditions...
diff = data_tfr.left;
diff.powspctrm = (data_tfr.left.powspctrm - data_tfr.right.powspctrm) ./ data_tfr.left.powspctrm;

cfg = [];
cfg.interactive = 'yes';
cfg.layout = 'neuromag306cmb.lay';
cfg.zlim = 'maxabs';

ft_multiplotTFR(cfg, diff);

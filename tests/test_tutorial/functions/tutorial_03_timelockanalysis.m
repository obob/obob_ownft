function tutorial_03_timelockanalysis(subject, indir, outdir)

% first things first: load the data...
load(fullfile(indir, subject));

% resample the data...

cfg = [];
cfg.resamplefs = 200;
cfg.detrend = 'no';

data = ft_resampledata(cfg, data);

% as we want to see erps, we should lowpass filter the data...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 30;

data = ft_preprocessing(cfg, data);

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-.5 1];

data = ft_redefinetrial(cfg, data);

% now we have to interpolate the data to make up for the rejected channels.
% please note that we only do this in sensor space! NEVER DO THIS IN SOURCE
% SPACE!! NEVER!!!!!

cfg = [];
cfg.interp = 'nearest';

data_interp = cimec_fixchannels(cfg, data);

% now do timelockanalysis...
% first, define conditions...
conditions = [];
conditions.left.trls = find(data_interp.trialinfo < 5);
conditions.right.trls = find(data_interp.trialinfo >= 5);

all_conds = fieldnames(conditions);
cfg = [];

for i = 1:length(all_conds)
  cur_condition = conditions.(all_conds{i});
  cfg.trials = cur_condition.trls;
  
  data_timelocked.(all_conds{i}) = ft_combineplanar([], ft_timelockanalysis(cfg, data_interp));
end %for

% now lets save the data...
mkdir(outdir)
save(fullfile(outdir, subject), 'data_timelocked');
end


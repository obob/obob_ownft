function tutorial_08_lcmv_beamformer(subject, indir, headmodeldir, outdir)
% first things first: load the data...
load(fullfile(indir, subject));
load(fullfile(headmodeldir, subject));

% as we want to see erps, we should lowpass filter the data...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 30;

data = ft_preprocessing(cfg, data);

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-.5 1];

data = ft_redefinetrial(cfg, data);

% as we want to use both magnetometers and gradiometers, we have to adjust
% the grad structure...
data.hdr.grad = cimec_adjust_tra(data.hdr.grad);

% note that we do not interpolate here as this is bad when going into
% sourcespace...

% we first create the leadfield...
cfg = [];
cfg.grad = data.hdr.grad;
cfg.vol = vol;
cfg.grid = grid_subject;

lf = ft_prepare_leadfield(cfg, data);

% we now do the timelockanalysis for all trials, retaining the covariance
% matrix...
cfg = [];
cfg.covariance = 'yes';

data_avg_all = ft_timelockanalysis(cfg, data);

% so, lets calculate the spatial filter on all trials...
cfg = [];
cfg.method = 'lcmv';
cfg.grid = lf;
cfg.lcmv.keepfilter = 'yes';
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.lcmv.lambda = '5%';

spatial_filters = ft_sourceanalysis(cfg, data_avg_all);

% so, lets home in on the interesting area...
% first, define conditions...
conditions = [];
conditions.left.trls = find(data.trialinfo < 5);
conditions.left.toilim = [.08 .14];
conditions.right.trls = find(data.trialinfo >= 5);
conditions.right.toilim = [.08 .14];
conditions.baseline.trls = 'all';
conditions.baseline.toilim = [-.5 -.2];

all_conds = fieldnames(conditions);

data_avg_cond = [];

% now we do ft_timelockanalysis for each condition including the
% baseline...

for i = 1:length(all_conds)
  cond = conditions.(all_conds{i});
  cfg = [];
  cfg.trials = cond.trls;
  cfg.toilim = cond.toilim;
  
  tmp = ft_redefinetrial(cfg, data);
  
  cfg = [];
  cfg.covariance = 'yes';
  
  data_avg_cond.(all_conds{i}) = ft_timelockanalysis(cfg, tmp);
  clear tmp;
end %for

% now we project the data...

data_projected = [];
for i = 1:length(all_conds)
  cfg = [];
  cfg.method = 'lcmv';
  cfg.grid = lf;
  cfg.vol = vol;
  cfg.grad = data.hdr.grad;
  cfg.grid.filter = spatial_filters.avg.filter;
  cfg.lcmv.projectnoise = 'yes';
  cfg.lcmv.lambda = '5%';
  
  data_projected.(all_conds{i}) = ft_sourceanalysis(cfg, data_avg_cond.(all_conds{i}));
  data_projected.(all_conds{i}).pos = mni_grid.pos;
end %for

% now save it all...
mkdir(outdir);
save(fullfile(outdir, subject), 'grid_subject', 'mni_grid', 'data_projected');

end


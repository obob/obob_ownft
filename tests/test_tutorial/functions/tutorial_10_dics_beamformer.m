function tutorial_10_dics_beamformer(subject, indir, headmodeldir, outdir)
% first things first: load the data...
load(fullfile(indir, subject));
load(fullfile(headmodeldir, subject));

% as we want to use both magnetometers and gradiometers, we have to adjust
% the grad structure...
data.hdr.grad = cimec_adjust_tra(data.hdr.grad);

% note that we do not interpolate here as this is bad when going into
% sourcespace...

% we first create the leadfield...
cfg = [];
cfg.grad = data.hdr.grad;
cfg.vol = vol;
cfg.grid = grid_subject;

lf = ft_prepare_leadfield(cfg, data);

% first home in on the relevant period (.3 - 1.3) we will add 100ms on each
% side to compensate for tapering...

cfg = [];
cfg.toilim = [.2 1.4];

data = ft_redefinetrial(cfg, data);

% first do freqanalysis on all trials to get a common filter...
cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'powandcsd';
cfg.foilim = [9 13];
cfg.taper = 'hanning';

alldata_csd = ft_freqanalysis(cfg, data);

% then calculate the spatial filter..
cfg = [];
cfg.method = 'dics';
cfg.grid = lf;
cfg.dics.keepfilter = 'yes';
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.frequency = 11;
cfg.dics.lambda = '5%';

spatial_filter = ft_sourceanalysis(cfg, alldata_csd);

% define conditions...
conditions = [];
conditions.left.trls = find(data.trialinfo < 5);
conditions.right.trls = find(data.trialinfo >= 5);

all_conds = fieldnames(conditions);
data_projected = [];

for i = 1:length(all_conds)
  cond = all_conds{i};
  
  % first we do freqanalysis only on the relevant trials for the condition
  cfg = [];
  cfg.method = 'mtmfft';
  cfg.output = 'powandcsd';
  cfg.foilim = [9 13];
  cfg.taper = 'hanning';
  cfg.trials = conditions.(cond).trls;
  
  tmp = ft_freqanalysis(cfg, data);
  
  % now lets project...
  cfg = [];
  cfg.method = 'dics';
  cfg.grid = lf;
  cfg.grid.filter = spatial_filter.avg.filter;
  cfg.vol = vol;
  cfg.grad = data.hdr.grad;
  cfg.frequency = 11;
  cfg.dics.projectnoise = 'yes';
  cfg.dics.lambda = '5%';

  data_projected.(cond) = ft_sourceanalysis(cfg, tmp);
  data_projected.(cond).pos = mni_grid.pos;
end %for

% save...
mkdir(outdir);
save(fullfile(outdir, subject), 'grid_subject', 'mni_grid', 'data_projected');
end


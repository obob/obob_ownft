function tutorial_01_readdata(subject, indir, outdir)

% first thing to do is to search for the data files. we know that the files
% will be in the directory /indir/subject and have the file extension
% -.fif. so lets look for them...

all_files = dir(fullfile(indir, subject, '*.fif'));

% all_files is an array that has all the fif files in it. to access the
% first file, we would need to call all_files(1).name. to determine, how
% many fif files we found, we can use length(all_files). we use this
% information to loop over the files. the final data will be stored in a
% cell array called data that we initialize first.

data = {};

for i = 1:length(all_files)
  % to actually read the file, we need not only its name but also the
  % folder it is in. all_files(i).name only gives us the name, though so we
  % need to build the whole path from indir and the filename using the
  % matlab function fullfile...
  
  filename = fullfile(indir, subject, all_files(i).name);
  
  % first we read all the data and apply a highpass filter to get rid of
  % the DC shifts...
  cfg = [];
  cfg.dataset = filename;
  cfg.trialdef.triallength = Inf;
  
  cfg = ft_definetrial(cfg);
  
  cfg.hpfilter = 'yes';
  cfg.hpfreq = 1;
  cfg.channel = 'MEG';
  
  tmp = ft_preprocessing(cfg);
  
  % now, lets get all the events that are in the data...
  cfg = [];
  cfg.dataset = filename;
  cfg.eventformat = 'neuromag_fif_v2_flipped';
  cfg.trialdef.eventtype = 'Trigger';
  cfg.trialdef.eventvalue = [1 2 3 4 5 6 7 8];
  cfg.trialdef.prestim = 1;
  cfg.trialdef.poststim = 3;
  cfg.diode.triggers = [1 2 3 4 5 6 7 8];
  
  cfg = ft_definetrial(cfg);
  
  % and correct it for display delays...
  cfg = cimec_correct_diode(cfg);
  
  data{i} = ft_redefinetrial(cfg, tmp);
  clear tmp;
  disp('end');
end %for

% now we have to save the data...
mkdir(outdir);
save(fullfile(outdir, subject), 'data', '-v7.3');

end


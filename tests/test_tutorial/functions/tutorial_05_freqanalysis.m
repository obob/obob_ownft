function tutorial_05_freqanalysis(subject, indir, outdir)

% first things first: load the data...
load(fullfile(indir, subject));

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-1 2];

data = ft_redefinetrial(cfg, data);

% now we have to interpolate the data to make up for the rejected channels.
% please note that we only do this in sensor space! NEVER DO THIS IN SOURCE
% SPACE!! NEVER!!!!!

cfg = [];
cfg.interp = 'nearest';

data_interp = cimec_fixchannels(cfg, data);


% now we define the conditions.
conditions = [];
conditions.left.trls = find(data_interp.trialinfo < 5);
conditions.right.trls = find(data_interp.trialinfo >= 5);

all_conds = fieldnames(conditions);

% and now we configure the time-frequency transform using windows fft...
cfg = [];
cfg.method = 'mtmconvol';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.foi = 4:1:30;
cfg.toi = -1:.03:2;
cfg.t_ftimwin = 4./cfg.foi;

% now we executed it for every condition....
for i = 1:length(all_conds)
  cur_condition = conditions.(all_conds{i});
  cfg.trials = cur_condition.trls;
  
  data_tfr.(all_conds{i}) = ft_combineplanar([], ft_freqanalysis(cfg, data_interp));
end %for

% now lets save the data...
mkdir(outdir)
save(fullfile(outdir, subject), 'data_tfr');
end



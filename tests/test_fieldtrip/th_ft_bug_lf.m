%% clear
clear all global
close all

%% set variables...
testdata.datadir = '/mnt/obob/tutorial_data/data/raw/el/';
testdata.ctf_file = '121011Elisa01.fif';

%% init cimec_ft
addpath(fullfile(pwd, '..', '..'));

cfg = [];
cfg.no_override = true;
obob_init_ft(cfg);
addpath('functions');


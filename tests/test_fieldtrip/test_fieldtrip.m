% always run this file after you updated the fieldtrip version. it will check the most important things so you can be sure you do not break anything...
% please do not do any actual data analysis in this script but do it all in
% functions.
% and do not store any data in the git repo!!!

%% clear
clear all global
close all

%% set variables...
testdata.datadir = '/mnt/obob/tutorial_data/data/raw/el/';
testdata.ctf_file = '121011Elisa01.fif';

standardfile = '/mnt/obob/tutorial_data/data/ft_test_el_170227.mat';

%% init cimec_ft
addpath(fullfile(pwd, '..', '..'));

cfg = [];
cfg.no_override = true;
cfg.package.svs = true;
obob_init_ft(cfg);
addpath('functions');

%% reading data, headshapes, sensor positions and triggers from a raw ctf-file
[testdata.data, testdata.hs, testdata.sens, testdata.triggers] = test01_read_ctf(fullfile(testdata.datadir, testdata.ctf_file));

%% timelockanalysis...
testdata.data_tl = test02_timelock(testdata.data);

%% freqanalysis...
testdata.data_tfr = test03_tfr(testdata.data);

%% make headmodel...
[ testdata.vol, testdata.mni_grid, testdata.grid_subject, testdata.mri ] = test04_make_headmodel(fullfile(testdata.datadir, testdata.ctf_file));

%% make lcmv beamformer...
testdata.data_lcmv = test05_lcmv(testdata.data, testdata.grid_subject, testdata.vol);

%% make dics beamformer...
[testdata.data_dics, testdata.data_dics_bl] = test06_dics(testdata.data, testdata.grid_subject, testdata.vol);

%% test svs...
[testdata.data_svs, testdata.data_svs_nai] = test07_svs(testdata.data, testdata.grid_subject, testdata.vol);

%% test obob_reject_visual
test08_good = true;

try
  test08_reject_and_interpolate(testdata.data);
catch
  test08_good = false;
end

%% test if matches standard file....
std = load(standardfile);
if ~isequaln(remove_cfg(testdata), remove_cfg(std.testdata)) || ~test08_good
  fprintf('\n\nSOMETHING IS WRONG!!!!!\n\n');
else
  fprintf('\n\nEverything is good. Go on!\n\n');
end %if
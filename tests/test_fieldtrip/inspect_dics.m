%% load mni
load mni_mri;

%% choose data...

data = testdata.data_dics;
data.pos = std.testdata.mni_grid.pos;

%% calc nai....
data.avg.nai = data.avg.pow ./ data.avg.noise;

%% interpolate
cfg = [];
cfg.parameter = 'nai';

source = ft_sourceinterpolate(cfg, data, mni_mri);
source.mask = abs(source.nai) >= quantile(abs(source.nai(:)), .7);

%% plot..
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'nai';
cfg.maskparameter = 'mask';
cfg.interactive = 'yes';
cfg.atlas = which('TTatlas+tlrc.BRIK');

source.coordsys = 'mni';

ft_sourceplot(cfg, source);
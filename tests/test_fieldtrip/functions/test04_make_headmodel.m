function [ vol, mni_grid, grid_subject, mri ] = test04_make_headmodel( file )
%TEST04_MAKE_HEADMODEL Summary of this function goes here
%   Detailed explanation goes here

cfg = [];
cfg.headshape = file;
cfg.mrifile = [];
cfg.reslice = true;
cfg.interactive = false;
[mri, ~, vol, mri_seg] = obob_coregister(cfg);

%% load segmented standard brain...
load mni_seg

%% put segmentation back into mri....
mri.gray = mri_seg.gray;
mri.white = mri_seg.white;
mri.csf = mri_seg.csf;

%% make headmodel for segmented mni brain...
cfg = [];
cfg.method = 'singleshell';

mni_headmodel = ft_prepare_headmodel(cfg, mni_seg);

%% make grid on mni brain...
%minbnd = min(mni_headmodel.bnd.pnt);
%maxbnd = max(mni_headmodel.bnd.pnt);
minbnd = min(mni_headmodel.bnd.pos);
maxbnd = max(mni_headmodel.bnd.pos);
cfg = [];

cfg.grid.unit = 'mm';
cfg.vol = mni_headmodel;
cfg.xgrid = minbnd(1)*1.2:10:maxbnd(1)*1.2;
cfg.ygrid = minbnd(2)*1.2:10:maxbnd(2)*1.2;
cfg.zgrid = minbnd(3)*1.2:10:maxbnd(3)*1.2;
mni_grid = ft_prepare_sourcemodel(cfg);
%mni_grid = ft_convert_units(mni_grid, 'm');

%% morph grid to individual mri...
cfg = [];
cfg.grid.warpmni = 'yes';
cfg.grid.template = mni_grid;
cfg.mri = mri;
cfg.grid.nonlinear = 'yes';

grid_subject = ft_prepare_sourcemodel(cfg);
%grid_subject = ft_convert_units(grid_subject, 'm');

end



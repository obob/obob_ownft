function [data, hs, sens, triggers] = test01_read_ctf( filename )

% read in triggers...
cfg = [];
cfg.dataset = filename;
cfg.trialdef.eventtype = 'Trigger';
cfg.trialdef.eventvalue = [16 8 24 4 20 12 28 2];
cfg.trialdef.prestim = 1;
cfg.trialdef.poststim = 3;
cfg.diode.triggers = [16 8 24 4 20 12 28 2];
%cfg.flip = true;

cfg = ft_definetrial(cfg);
cfg = obob_correct_diode(cfg);

triggers = cfg.event;

% read in sensors...
sens = ft_read_sens(filename);

% read in headshape...
hs = ft_read_headshape(filename);

% read in data...
cfg.hpfilter = 'yes';
cfg.hpfreq = 1;
cfg.channel = 'MEG';

data = ft_preprocessing(cfg);

% resample data to save some memory and time...
cfg = [];
cfg.resamplefs = 200;
cfg.detrend = 'no';

data = ft_resampledata(cfg, data);


end


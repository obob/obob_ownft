function [ data_svs, data_svs_nai] = test07_svs( data, grid_subject, vol )
%TEST07_SVS Summary of this function goes here
%   Detailed explanation goes here

%% preproc the data...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 20;

data = ft_preprocessing(cfg, data);

cfg = [];
cfg.latency = [-.2 .5];

data = ft_selectdata(cfg, data);

%% do svs....
cfg = [];
cfg.grid = grid_subject;
cfg.hdm = vol;
cfg.regfac = '5%';

data_svs = obob_svs_beamtrials_lcmv(cfg, data);

cfg.nai = true;

data_svs_nai = obob_svs_beamtrials_lcmv(cfg, data);
end


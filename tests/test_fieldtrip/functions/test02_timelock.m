function [ data ] = test02_timelock( data )
%TEST02_TIMELOCK Summary of this function goes here
%   Detailed explanation goes here

% left triggers...
ltrigger = [16 8 24 4];

% as we want to see erps, we should lowpass filter the data...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 30;

data = ft_preprocessing(cfg, data);

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-.5 1];

data = ft_redefinetrial(cfg, data);

% remove channel 3...
cfg = [];
cfg.channel = {'all', '-MEG0111'};

data = ft_selectdata(cfg, data);

% interpolate missing channel...
cfg = [];
cfg.interp = 'nearest';

data = obob_fixchannels(cfg, data);

% average only left trials...
cfg = [];
cfg.trials = ismember(data.trialinfo, ltrigger);

data = ft_combineplanar([], ft_timelockanalysis(cfg, data));
end


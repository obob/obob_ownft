function [ data_dics data_dics_bl] = test06_dics_bl(data, grid_subject, vol)
%TEST06_DICS Summary of this function goes here
%   Detailed explanation goes here

% left triggers...
ltrigger = [16 8 24 4];

% note that we do not interpolate here as this is bad when going into
% sourcespace...

% we first create the leadfield...
cfg = [];
cfg.grad = data.hdr.grad;
cfg.vol = vol;
cfg.grid = grid_subject;

lf = ft_prepare_leadfield(cfg, data);

% first home in on the relevant period (.3 - 1.3) we will add 100ms on each
% side to compensate for tapering...

cfg = [];
cfg.toilim = [-.6 -.2];
data_bl = ft_redefinetrial(cfg, data);

cfg = [];
cfg.toilim = [.2 .6];

data_stim = ft_redefinetrial(cfg, data);

cfg = [];
cfg.toilim = [.2 1.4];
data = ft_redefinetrial(cfg, data);

% first do freqanalysis on all trials to get a common filter...
cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'powandcsd';
cfg.foilim = [9 13];
cfg.taper = 'hanning';

alldata_csd = ft_freqanalysis(cfg, data);

% then calculate the spatial filter..
cfg = [];
cfg.method = 'dics';
cfg.grid = lf;
cfg.dics.keepfilter = 'yes';
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.frequency = 11;
cfg.dics.lambda = '5%';

spatial_filter = ft_sourceanalysis(cfg, alldata_csd);

cfg = [];
cfg.method = 'mtmfft';
cfg.output = 'powandcsd';
cfg.foilim = [9 13];
cfg.taper = 'hanning';
cfg.trials = ismember(data.trialinfo, ltrigger);
cfg.pad = 1;

tmp = ft_freqanalysis(cfg, data_stim);
tmp_bl = ft_freqanalysis(cfg, data_bl);

% now lets project...
cfg = [];
cfg.method = 'dics';
cfg.grid = lf;
cfg.grid.filter = spatial_filter.avg.filter;
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.frequency = 11;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda = '5%';

data_dics = ft_sourceanalysis(cfg, tmp);
data_dics_bl = ft_sourceanalysis(cfg, tmp_bl);

end


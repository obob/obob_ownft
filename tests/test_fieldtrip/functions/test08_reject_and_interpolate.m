function test08_reject_and_interpolate( data )
%TEST08_REJECT_AND_INTERPOLATE Summary of this function goes here
%   Detailed explanation goes here

testCase = matlab.unittest.TestCase.forInteractiveUse;

% load data...
nchannels = length(data.label);
ntrials = length(data.trial);

% configure rejectvisual...
cfg = [];
cfg.downsample = 50;
cfg.preproc.hpfilter = 'yes';
cfg.preproc.hpfreq = 1;

fprintf('Remove channels 1, 3, 7 and 20 and trials 1, 15 and 40, please.\n');

data_rej = obob_rejectvisual(cfg, data);

% now test whether the three correct channels are really missing...
for i = 1:nchannels
  if i == 1 || i == 3 || i == 7 || i == 20
    testCase.assertTrue(isempty(match_str(data.label{i}, data_rej.label)));
  else
    testCase.assertFalse(isempty(match_str(data.label{i}, data_rej.label)));
  end %if
end %for

% now test whether the data of all trials match...
rem_channels = match_str(data.label, data_rej.label);
triallist = 1:ntrials;
triallist_rej = triallist;
triallist_rej([1 15 40]) = [];
for i = 1:length(triallist_rej)
  testCase.assertEqual(data.trial{triallist_rej(i)}(rem_channels, :), data_rej.trial{i});
end %for

% now do the fixchannels
cfg = [];

data_fixed = obob_fixchannels(cfg, data_rej);

% check whether the data still matches...
[new_channels1 new_channels2] = match_str(data_rej.label, data_fixed.label);
for i = 1:length(data_rej.trial)
  testCase.assertEqual(data_rej.trial{i}(new_channels1, :), data_fixed.trial{i}(new_channels2, :));
end %for


end


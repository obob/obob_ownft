%% load mni
load mni_mri;

%% choose data....
data = testdata.data_svs;

%% timelockanalysis....
cfg = [];

data = ft_timelockanalysis(cfg, data);

%% make source struct....
cfg = [];
cfg.sourcegrid = testdata.mni_grid;
cfg.latency = [.08 .14];
cfg.mri = mni_mri;
cfg.parameter = 'avg';

source = obob_svs_virtualsens2source(cfg, data);

%% plot....
cfg = [];
cfg.funparameter = 'avg';
%cfg.funcolorlim = [-2e-11 2e-11];

ft_sourceplot(cfg, source);

%% look at timecourse....
[idx, idx_inside] = obob_coord2voxnr([-1.5 -54.5 61.5], testdata.mni_grid);

cfg = [];
cfg.channel = idx_inside;

figure; ft_singleplotER(cfg, data);
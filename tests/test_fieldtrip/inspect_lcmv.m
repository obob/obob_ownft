%% load mni
load mni_mri;

%% set curdata
curdata = testdata;

%% calc nai....
active.left = curdata.data_lcmv.active_left;
active.left.avg.nai = active.left.avg.pow ./active.left.avg.noise;

active.right = curdata.data_lcmv.active_right;
active.right.avg.nai = active.right.avg.pow ./active.right.avg.noise;

baseline = curdata.data_lcmv.baseline;
baseline.avg.nai = baseline.avg.pow ./baseline.avg.noise;

relchange = active.left;
relchange.avg.nai = (active.left.avg.pow - active.right.avg.pow) ./ active.left.avg.pow;
relchange.pos = curdata.mni_grid.pos;

%% interpolate
cfg = [];
cfg.parameter = 'nai';

source = ft_sourceinterpolate(cfg, relchange, mni_mri);
source.mask = abs(source.nai) >= quantile(abs(source.nai(:)), .7);

%% plot..
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'nai';
cfg.maskparameter = 'mask';
cfg.interactive = 'yes';
%cfg.atlas = which('TTatlas+tlrc.BRIK');

source.coordsys = 'mni';

ft_sourceplot(cfg, source);
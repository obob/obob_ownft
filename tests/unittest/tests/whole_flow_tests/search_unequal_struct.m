function search_unequal_struct( str1, str2, tree)
%SEARCH_UNEQUAL_STRUCT Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
  tree = {};
end %if

str1 = remove_cfg(str1);
str2 = remove_cfg(str2);

fnames = fieldnames(str1);

if ~isequaln(sort(fnames), sort(fieldnames(str2)))
  fprintf('The strutures have different fieldnames\n');
  disp(tree)
  fprintf('\n');
  disp(fnames)
  fprintf('\n');
  disp(fieldnames(str2))
  return
end %if

for i = 1:length(fnames)
  if isequaln(str1.(fnames{i}), str2.(fnames{i}))
    continue;
  end %if
  
  if isstruct(str1.(fnames{i}))
    newtree = tree;
    newtree{end+1} = fnames{i};
    if length(str1.(fnames{i})) > 1
      fprintf('Something is wrong here...\n');
      disp(newtree);
      return
    else
      search_unequal_struct(str1.(fnames{i}), str2.(fnames{i}), newtree);
    end %if
  else
    newtree = tree;
    newtree{end+1} = fnames{i};
    fprintf('I think, I have found it...\n');
    disp(newtree);
    return
  end %if
  
  
end %if

end


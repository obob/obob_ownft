function [ str ] = remove_cfg( str, extra )
%REMOVE_CFG recursivly searches for cfg fields and removes them....
if ~isstruct(str) || isempty(str)
  return
end %if

if nargin == 1
  extra = [];
end %if

fnames = fieldnames(str);

for i = 1:length(fnames)
  if strcmp(fnames{i}, 'cfg')
    str = rmfield(str, 'cfg');
  elseif strcmp(fnames{i}, 'params')
    str = rmfield(str, 'params');
  elseif strcmp(fnames{i}, extra)
    str = rmfield(str, extra);
  else
    if length(str.(fnames{i})) > 1
      for j = 1:length(str.(fnames{i}))
        str.(fnames{i})(j) = remove_cfg(str.(fnames{i})(j), extra);
      end %for
    else
      str.(fnames{i}) = remove_cfg(str.(fnames{i}), extra);
    end %if
  end %if
end %for


end


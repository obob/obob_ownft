function [ data ] = test03_tfr( data )
%TEST03_TFR Summary of this function goes here
%   Detailed explanation goes here

% left triggers...
ltrigger = [16 8 24 4];

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-1 2];

data = ft_redefinetrial(cfg, data);

% now we have to interpolate the data to make up for the rejected channels.
% please note that we only do this in sensor space! NEVER DO THIS IN SOURCE
% SPACE!! NEVER!!!!!

cfg = [];
cfg.interp = 'weighted';

data = obob_fixchannels(cfg, data);

% average only left trials...
cfg = [];
cfg.method = 'mtmconvol';
cfg.output = 'pow';
cfg.taper = 'hanning';
cfg.pad = 'nextpow2';
cfg.foi = 4:1:30;
cfg.toi = -1:.03:2;
cfg.t_ftimwin = 4./cfg.foi;
cfg.trials = ismember(data.trialinfo, ltrigger);

data = ft_combineplanar([], ft_freqanalysis(cfg, data));

end


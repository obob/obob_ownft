function [ vol, mni_grid, grid_subject, mri ] = test04_make_headmodel(file, standardfile, testCase)
%TEST04_MAKE_HEADMODEL Summary of this function goes here
%   Detailed explanation goes here

cfg = [];
cfg.headshape = file;
cfg.mrifile = [];
cfg.reslice = true;
cfg.interactive = 'no';
cfg.unittest = true;
cfg.plotresults = 'no';
cfg.cleanhs = 'no';
[mri, ~, vol, mri_seg] = obob_coregister(cfg);

%% load segmented standard brain...
load mni_seg

%% put segmentation back into mri....
mri.gray = mri_seg.gray;
mri.white = mri_seg.white;
mri.csf = mri_seg.csf;

%% test generated mri
if exist(standardfile)
  std = load(standardfile);

  test_new_mri = rmfield(mri, 'hdr');
  test_old_mri = rmfield(std.testdata.mri, 'hdr');

  testCase.assertEqual(remove_cfg(test_new_mri), remove_cfg(test_old_mri), 'RelTol', 5e-2);

  mri = std.testdata.mri;
end %if

%% make headmodel for segmented mni brain...
cfg = [];
cfg.method = 'singleshell';

mni_headmodel = ft_prepare_headmodel(cfg, mni_seg);
mni_headmodel = ft_convert_units(mni_headmodel, 'm');

%% make grid on mni brain...
load mni_grid_1_5_cm_889pnts.mat
mni_grid = template_grid;

%% morph grid to individual mri...
cfg = [];
cfg.grid.warpmni = 'yes';
cfg.grid.template = mni_grid;
cfg.mri = mri;
cfg.grid.nonlinear = 'yes';
cfg.grid.unit = 'm';

grid_subject = ft_prepare_sourcemodel(cfg);

%% overwrite grid_subject
if exist(standardfile)
  grid_subject = std.testdata.grid_subject;
end %if

end



function [ data_svs, data_svs_nai] = test07_svs( data, grid_subject, vol )
%TEST07_SVS Summary of this function goes here
%   Detailed explanation goes here

%% preproc the data...
cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 20;

data = ft_preprocessing(cfg, data);

cfg = [];
cfg.latency = [-.2 .5];

data = ft_selectdata(cfg, data);

%% convert units...
data.grad = ft_convert_units(data.grad, 'm');
grid_subject = ft_convert_units(grid_subject, 'm');
vol = ft_convert_units(vol, 'm');

%% create leadfield...
cfg = [];
cfg.grid = grid_subject;
cfg.vol = vol;

lf = ft_prepare_leadfield(cfg, data);

%% do svs....
cfg = [];
cfg.covariance = 'yes';

data_cov = ft_timelockanalysis(cfg, data);

cfg = [];
cfg.grid = lf;
cfg.regfac = '5%';

spatial_filter = obob_svs_compute_spat_filters(cfg, data_cov);

cfg = [];
cfg.spatial_filter = spatial_filter;

data_svs = obob_svs_beamtrials_lcmv(cfg, data);

cfg.nai = true;

data_svs_nai = obob_svs_beamtrials_lcmv(cfg, data);
end


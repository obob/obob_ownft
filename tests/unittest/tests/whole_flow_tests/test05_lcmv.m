function [ data_lcmv ] = test05_lcmv( data, grid_subject, vol )
%TEST05_LCMV Summary of this function goes here
%   Detailed explanation goes here

% left triggers...
ltrigger = [16 8 24 4];

cfg = [];
cfg.lpfilter = 'yes';
cfg.lpfreq = 30;

data = ft_preprocessing(cfg, data);

% and we also do not need all the data...
cfg = [];
cfg.toilim = [-.5 1];

data = ft_redefinetrial(cfg, data);

% as we want to use both magnetometers and gradiometers, we have to adjust
% the grad structure...
%data.hdr.grad.tra = round(data.hdr.grad.tra);

cfg = [];
cfg.grad = ft_convert_units(data.hdr.grad, 'm');
cfg.vol = vol;
cfg.grid = ft_convert_units(grid_subject, 'm');
%cfg.grid = grid_subject;

lf = ft_prepare_leadfield(cfg, data);

% we now do the timelockanalysis for all trials, retaining the covariance
% matrix...
cfg = [];
cfg.covariance = 'yes';

data_avg_all = ft_timelockanalysis(cfg, data);

% so, lets calculate the spatial filter on all trials...
cfg = [];
cfg.method = 'lcmv';
cfg.grid = lf;
cfg.lcmv.keepfilter = 'yes';
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.lcmv.lambda = '5%';

spatial_filters = ft_sourceanalysis(cfg, data_avg_all);

% redefine to n1...
cfg = [];
cfg.trials = ismember(data.trialinfo, ltrigger);
cfg.toilim = [.08 .14];
tmp_left = ft_redefinetrial(cfg, data);

cfg.trials = ~ismember(data.trialinfo, ltrigger);
tmp_right = ft_redefinetrial(cfg, data);

cfg = [];
cfg.covariance = 'yes';

data_avg_left = ft_timelockanalysis(cfg, tmp_left);
data_avg_right = ft_timelockanalysis(cfg, tmp_right);

%project data...

cfg = [];
cfg.method = 'lcmv';
cfg.grid = lf;
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.grid.filter = spatial_filters.avg.filter;
cfg.lcmv.projectnoise = 'yes';
cfg.lcmv.lambda = '5%';

data_lcmv.active_left = ft_sourceanalysis(cfg, data_avg_left);
data_lcmv.active_right = ft_sourceanalysis(cfg, data_avg_right);

% redefine to baseline...
cfg = [];
cfg.toilim = [-.5 -.2];
tmp = ft_redefinetrial(cfg, data);

cfg = [];
cfg.covariance = 'yes';

data_avg_left = ft_timelockanalysis(cfg, tmp);

%project data...

cfg = [];
cfg.method = 'lcmv';
cfg.grid = lf;
cfg.vol = vol;
cfg.grad = data.hdr.grad;
cfg.grid.filter = spatial_filters.avg.filter;
cfg.lcmv.projectnoise = 'yes';
cfg.lcmv.lambda = '5%';

data_lcmv.baseline = ft_sourceanalysis(cfg, data_avg_left);

end


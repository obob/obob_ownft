classdef testOBOB_source_projection < matlab.unittest.TestCase
  %TESTOBOB_SOURCE_PROJECTION Summary of this class goes here
  %   Detailed explanation goes here
  
  properties(TestParameter)
    channels = {'MEG', 'MEGMAG', 'MEGGRAD'};
    headmodel = {'singlesphere'};
    method = {'lcmv', 'svs_lcmv', 'svs_lcmv_induced'};
    %dip = num2cell(1:50:2982);
    dip = num2cell(1:32);
  end %testparameter
  
  properties
    mri
    mni_mri
    sens
    template_grid
    grids
    vols
    lfs
    fsample = 200;
    ntrials = 100;
    freq = 10;
    min_distance_from_center = 0.03;
    old_warning
  end
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
     
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
      testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
      
      load(fullfile(testfolder, 'test_mri_seg.mat'));
      load('mni_mri');
            
      % convert everything to meters...
      testCase.mni_mri = ft_convert_units(mni_mri, 'm');
      testCase.mri = ft_convert_units(mri, 'm');
      testCase.sens = ft_convert_units(sens, 'm');
      %testCase.template_grid = ft_convert_units(template_grid, 'm');
      
      for i = 1:length(testCase.headmodel)
        cur_headmodel = testCase.headmodel{i};
        
        cfg = [];
        cfg.method = cur_headmodel;
        
        testCase.vols.(cur_headmodel) = ft_prepare_headmodel(cfg, testCase.mri);
        
        % make grid....
        tmp_vol = testCase.vols.(cur_headmodel);
        if strcmp(cur_headmodel, 'singlesphere')
          tmp_vol.r = tmp_vol.r - 0.01;
        end %if
        
        cfg = [];
        cfg.headmodel = tmp_vol;
        cfg.grid.resolution = 0.015;
        cfg.grid.unit = 'm';
        
        testCase.grids.(cur_headmodel) = ft_prepare_sourcemodel(cfg);
        
        testCase.grids.(cur_headmodel).inside(rms(testCase.grids.(cur_headmodel).pos - tmp_vol.o, 2) <= testCase.min_distance_from_center) = false;
        testCase.grids.(cur_headmodel).inside(testCase.grids.(cur_headmodel).pos(:, 3) < 34e-3 & testCase.grids.(cur_headmodel).pos(:, 2) < 65e-3) = false;
        testCase.grids.(cur_headmodel).inside(testCase.grids.(cur_headmodel).pos(:, 3) < 40e-3) = false;
        
        for j = 1:length(testCase.channels)
          cur_channels = testCase.channels{j};
          
          cfg = [];
          cfg.grad = testCase.sens;
          cfg.headmodel = testCase.vols.(cur_headmodel);
          cfg.channel = cur_channels;
          cfg.dip.pos = testCase.grids.(cur_headmodel).pos(min(find(cumsum(testCase.grids.(cur_headmodel).inside) == 1)), :);
          cfg.dip.mom = [1 0 0]';
          cfg.dip.frequency = testCase.freq;
          cfg.dip.phase = 0;
          cfg.dip.amplitude = 10;
          cfg.ntrials = testCase.ntrials;
          cfg.triallength = 1;
          cfg.fsample = testCase.fsample;
          cfg.relnoise = 0.1;
          
          data = ft_dipolesimulation(cfg);
          cfg = [];
          cfg.grid = testCase.grids.(cur_headmodel);
          cfg.grad = testCase.sens;
          cfg.vol = testCase.vols.(cur_headmodel);
          
          testCase.lfs.(cur_headmodel).(cur_channels) = ft_prepare_leadfield(cfg, data);
          
        end %for
      end %for
    end %function
  end
  
  methods(Test)
    function testSimple(testCase, channels, headmodel, dip, method)
      % set vars....
      
      do_plot = false;
      
      cur_grid = testCase.grids.(headmodel);
      
      if strcmp(method, 'dics') & strcmp(channels, 'MEGMAG')
        return
      end %if
      
      % simulate a dipole.....
      cfg = [];
      cfg.grad = testCase.sens;
      cfg.headmodel = testCase.vols.(headmodel);
      cfg.channel = channels;
      cfg.dip.pos = cur_grid.pos(min(find(cumsum(cur_grid.inside) == dip)), :);
      cfg.dip.mom = [1 0 0]';
      cfg.dip.frequency = testCase.freq;
      if ismember(method, {'lcmv', 'svs_lcmv'})
        cfg.dip.phase = 0;
      else
        cfg.dip.phase = randn(testCase.ntrials, 1);
        %cfg.dip.phase = 0;
      end %if
      cfg.dip.amplitude = 1;
      cfg.ntrials = testCase.ntrials;
      cfg.triallength = 1;
      cfg.fsample = testCase.fsample;
      cfg.relnoise = 0.1;
      cfg.feedback = 'none';
      
      data = ft_dipolesimulation(cfg);
      
      switch method
        case 'dics'
          cfg = [];
          cfg.method = 'mtmfft';
          cfg.output = 'fourier';
          cfg.foi = [testCase.freq-1 testCase.freq+1];
          cfg.taper = 'hanning';
          cfg.pad = 'nextpow2';
          cfg.feedback = 'none';
          
          data_fft = ft_freqanalysis(cfg, data);
          
          cfg = [];
          cfg.method = 'dics';
          cfg.grid = testCase.lfs.(headmodel).(channels);
          cfg.frequency = testCase.freq;
          cfg.headmodel = testCase.vols.(headmodel);
          cfg.dics.projectnoise = 'yes';
          cfg.feedback = 'none';
          
          data_proj = ft_sourceanalysis(cfg, data_fft);
          
        case 'lcmv'
          cfg = [];
          cfg.covariance = 'yes';
          cfg.feedback = 'none';
          
          data_tl = ft_timelockanalysis(cfg, data);
          
          cfg = [];
          cfg.method = 'lcmv';
          cfg.grid = testCase.lfs.(headmodel).(channels);
          cfg.headmodel = testCase.vols.(headmodel);
          cfg.lcmv.projectnoise = 'yes';
          cfg.feedback = 'none';
          
          data_proj = ft_sourceanalysis(cfg, data_tl);
          
        case {'svs_lcmv', 'svs_lcmv_induced'}
          cfg = [];
          cfg.covariance = 'yes';

          data_for_cov = ft_timelockanalysis(cfg, data);
          
          cfg = [];
          cfg.grid = testCase.lfs.(headmodel).(channels);
          cfg.hdm = testCase.vols.(headmodel);
          
          spatial_filter = obob_svs_compute_spat_filters(cfg, data_for_cov);
          
          cfg = [];
          cfg.spatial_filter = spatial_filter;
          cfg.nai = true;
          
          data_source = obob_svs_beamtrials_lcmv(cfg, data);
          
          cfg = [];
          cfg.method = 'mtmfft';
          cfg.output = 'pow';
          cfg.foi = [testCase.freq-1 testCase.freq+1];
          cfg.taper = 'hanning';
          cfg.pad = 'nextpow2';
          cfg.feedback = 'none';
          
          data_fft = ft_freqanalysis(cfg, data_source);
          
          cfg = [];
          cfg.sourcegrid = testCase.lfs.(headmodel).(channels);
          cfg.frequency = [testCase.freq-1 testCase.freq+1];
          cfg.parameter = 'powspctrm';
          
          data_proj = obob_svs_virtualsens2source(cfg, data_fft);
          data_proj.avg.nai = data_proj.powspctrm;
      end %switch
      
      cfg = [];
      cfg.feedback = 'none';
      data_desc = ft_sourcedescriptives(cfg, data_proj);
      
      param = 'nai';
      [~, i] = max(data_desc.avg.(param));
      
      [~, sorted_idx] = sort(data_desc.avg.(param)(data_desc.inside), 'descend');
      if ~ismember(dip, sorted_idx(1:8))
        disp('hi');
      end %if
      testCase.assertTrue(ismember(dip, sorted_idx(1:8)))
      
      %testCase.assertEqual(cur_grid.pos(i, :), cur_grid.pos(min(find(cumsum(cur_grid.inside) == dip)), :))
      
      if do_plot
        cfg = [];
        cfg.parameter = param;
        
        source_inter = ft_sourceinterpolate(cfg, data_desc, testCase.mri);
        
        source_inter = ft_convert_units(source_inter, 'mm');
        
        cfg = [];
        cfg.funparameter = param;
        cfg.location = cur_grid.pos(min(find(cumsum(cur_grid.inside) == dip)), :) * 1000;
        
        ft_sourceplot(cfg, source_inter);
      end %if
    end %function
  end %methods
  
end


classdef testOBOB_parcellation < matlab.unittest.TestCase
  %TESTOBOB_WHOLE_FLOW Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    old_warning
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
     
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
    end %function
  end %methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(Test)
    function test_parcels(testCase)
      results_fname = fullfile(obob_get_testdata_folder(), 'parcellation_compare', 'data.mat');
      testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
      
      load(fullfile(testfolder, 'data_preproc.mat'));
      load(fullfile(testfolder, 'headmodel.mat'));
      load(fullfile(testfolder, 'test_mri_seg.mat'));
      
      cfg = [];
      cfg.resolution = 15e-3;
      
      parcellation = obob_svs_create_parcellation(cfg);
      
      data.grad = ft_convert_units(data.grad, 'm');
      mni_grid = ft_convert_units(mni_grid, 'm');
      
      cfg = [];
      cfg.latency = [0 .3];
      
      data = ft_selectdata(cfg, data);
      
      cfg = [];
      cfg.mri = mri;
      cfg.grid.warpmni = 'yes';
      cfg.grid.template = parcellation.template_grid;
      cfg.grid.nonlinear = 'no';
      cfg.grid.unit = 'm';
      
      subject_grid = ft_prepare_sourcemodel(cfg);
      
      cfg = [];
      cfg.grid = subject_grid;
      cfg.vol = vol;
      
      lf = ft_prepare_leadfield(cfg, data);
      
      cfg = [];
      cfg.preproc.lpfilter = 'yes';
      cfg.preproc.lpfreq = 35;
      cfg.covariance = 'yes';
      
      data_for_filt = ft_timelockanalysis(cfg, data);
      
      cfg = [];
      cfg.grid = lf;
      cfg.parcel = parcellation;
      cfg.fixedori = 'yes';
      
      spat_filts = obob_svs_compute_spat_filters(cfg, data_for_filt);
      
      cfg = [];
      cfg.spatial_filter = spat_filts;
      
      svs_parcel_th = obob_svs_beamtrials_lcmv(cfg, data);
      
      if ~exist(results_fname)
        save(results_fname);
      end %if
      
      results = load(results_fname);
      
      testCase.assertEqual(obob_rm_cfg(svs_parcel_th), obob_rm_cfg(results.svs_parcel_th), 'RelTol', 5e-1);
    end %function
  end %methods
  
end


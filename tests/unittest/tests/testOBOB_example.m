function tests = testOBOB_example()
% this first function is needed only for matlab. just change its name
% according to the filename in your tests and leave the rest alone.

tests = functiontests(localfunctions);
end

function test_example(testCase)
% now we need some data to work on. there is already some test data in the
% 'data' folder of the tests. it should work for a lot of cases. if you
% need anything else, you can of course add your own data. but please do
% not add files that are really big....

% but lets load some preprocessed data....
testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
load(fullfile(testfolder, 'data_preproc.mat'));

% and do a simple ft_timelockanalysis on one channel....
cfg = [];
cfg.channel = 'MEG0113';

tl_data = ft_timelockanalysis(cfg, data);

% so, how do we check whether the output is correct? one possibility is to
% load data that we have already processed like above to see whether we
% still get the same results (e.g., after a fieldtrip update)
old_data = load(fullfile(testfolder, 'timelock_analysis_results.mat'));

% now we need to compare our result to the old ones we loaded. this is,
% where the testCase parameter to this function shows up. it provides you
% with so-called "assert" functions. the easiest one is assertEqual, which
% checks whether the two structures are equal here.
% you can see, that we use the function 'obob_rm_cfg' on both structures.
% this function removes all cfg structures which is necessary because cfg
% structures contain time-stamps so they will never be equal.
% there are a lot of other assert functions, you can find here: doc matlab.unittest.qualifications.Assertable
testCase.assertEqual(obob_rm_cfg(tl_data), obob_rm_cfg(old_data.tl_data), 'RelTol', 5e-3);
end %function


classdef testOBOB_whole_flow < matlab.unittest.TestCase
  %TESTOBOB_WHOLE_FLOW Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    old_warning
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
      
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
    end %function
  end %methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(Test)
    function test_whole_flow(testCase)
      %if ~isempty(getenv('GITLAB_CI'))
      %  return
      %end %if
      testfile = fullfile(obob_get_testdata_folder, 'raw', 'sample.fif');
      
      standardfile = fullfile(obob_get_testdata_folder, 'whole_flow_compare', 'data.mat');
     
      addpath('whole_flow_tests');
      
      %% reading data, headshapes, sensor positions and triggers from a raw ctf-file
      [testdata.data, testdata.hs, testdata.sens, testdata.triggers] = test01_read_ctf(testfile);
      
      %% timelockanalysis...
      testdata.data_tl = test02_timelock(testdata.data);
      
      %% freqanalysis...
      testdata.data_tfr = test03_tfr(testdata.data);
      
      %% make headmodel...
      [ testdata.vol, testdata.mni_grid, testdata.grid_subject, testdata.mri ] = test04_make_headmodel(testfile, standardfile, testCase);
      
      %% make lcmv beamformer...
      testdata.data_lcmv = test05_lcmv(testdata.data, testdata.grid_subject, testdata.vol);
      
      %% make dics beamformer...
      [testdata.data_dics, testdata.data_dics_bl] = test06_dics(testdata.data, testdata.grid_subject, testdata.vol);
      
      %% test svs...
      [testdata.data_svs, testdata.data_svs_nai] = test07_svs(testdata.data, testdata.grid_subject, testdata.vol);
      
      %% see if we need to save....
      if ~exist(standardfile)
        save(standardfile, 'testdata');
      end %if
      
      %% test if matches standard file....
      std = load(standardfile);
      
      testdata.mri = rmfield(testdata.mri, 'hdr');
      std.testdata.mri = rmfield(std.testdata.mri, 'hdr');
           
      testCase.assertEqual(remove_cfg(testdata), remove_cfg(std.testdata), 'RelTol', 5e-3);
      
      
    end %function
  end %methods
  
end


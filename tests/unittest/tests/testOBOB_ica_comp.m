function tests = testOBOB_ica_comp()
tests = functiontests(localfunctions);
end

function test_ica_comp(testCase)
testfolder = fullfile(obob_get_testdata_folder(), 'ica_comp');
load(fullfile(testfolder, 'ica_comp_data.mat'));

cfg = [];
[comp_chan, comp_trl] = obob_find_comp_pattern(cfg, comp);

testCase.assertEqual(comp_chan, old_comp_chan);


clear comp_*

cfg =  [];
cfg.normalize = 'yes';
[comp_chan, comp_trl] = obob_find_comp_pattern(cfg, comp);

testCase.assertEqual(comp_chan, old_comp_chan);
end



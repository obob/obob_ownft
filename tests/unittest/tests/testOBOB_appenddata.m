classdef testOBOB_appenddata < matlab.unittest.TestCase
  %TESTOBOB_WHOLE_FLOW Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    old_warning
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
      
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
    end %function
  end %methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(Test)
    function test_appenddata(testCase)
      
      % simulate some data...
      n_chans = 10;
      
      % create a signal....
      data = {};
      for i = 1:n_chans/2
        cfg = [];
        cfg.method = 'broadband';
        cfg.fsample = 100;
        cfg.trllen = 2;
        cfg.numtrl = 50;

        cfg.n1.ampl = 1;
        cfg.n1.bpfreq = [5 10];
        cfg.n2.ampl = 2;
        cfg.n2.bpfreq = [12 20];
        cfg.noise.ampl = 3;

        tmp_data = ft_freqsimulation(cfg);
        
        cfg = [];
        cfg.channel = {'n1', 'n2'};
        
        tmp_data = ft_selectdata(cfg, tmp_data);
        tmp_data.label{1} = sprintf('chan_%02d', (i-1)*2);
        tmp_data.label{2} = sprintf('chan_%02d', (i-1)*2 + 1);
        
        data{end+1} = tmp_data;
      end %for
      
      cfg = [];
      cfg.appenddim = 'chan';
      
      data = ft_appenddata(cfg, data{:});
      
      % create two other dataset with fewer channels...
      cfg = [];
      cfg.channel = 1:7;
      
      data_few_1 = ft_selectdata(cfg, data);
      
      cfg = [];
      cfg.channel = 5:10;
      
      data_few_2 = ft_selectdata(cfg, data);
      
      % do appenddata
      cfg = [];
      cfg.appenddim = 'rpt';
      
      data_final = ft_appenddata(cfg, data, data_few_1, data_few_2);
      
      % do selectdata to get correct amount of channels...
      cfg = [];
      
      data_selected = ft_selectdata(cfg, data, data_few_1, data_few_2);
      
      testCase.assertEqual(length(data_selected.label), 3);
      testCase.assertEqual(length(data_final.label), 3);
      
      for i = 1:length(data_selected.trial)
        testCase.assertEqual(size(data_selected.trial{i}, 1), 3);
      end %if
      
      for i = 1:length(data_final.trial)
        testCase.assertEqual(size(data_final.trial{i}, 1), 3);
      end %if
      
    end %function
  end %methods
  
end


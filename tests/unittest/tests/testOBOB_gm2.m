function tests = testOBOB_gm2()
tests = functiontests(localfunctions);
end

function test_gm2(testCase)
addpath(fullfile(fileparts(mfilename('fullpath')), '../../../packages/gm2'));

testfolder = fullfile(obob_get_testdata_folder(), 'gm2');

load(fullfile(testfolder, 'obob_gm2_fourier.mat'));
std = load(fullfile(testfolder, 'obob_gm2_results.mat'));
cfg = [];
cfg.latency = [0 .2];
cfg.frequency = [8 10];
data = ft_selectdata(cfg, data);
%
for iT = 1:numel(data.time)
  cfg = [];
  cfg.latency = data.time(iT);
  
  tmp = ft_selectdata(cfg, data);
  % selectdata is afraid of getting something wrong with the
  % fourierspctrm, thus we have to select it ourselves
  tmp.fourierspctrm =  data.fourierspctrm(:,:,:,iT);
  
  % get crossspctrm at that time
  tmp=ft_checkdata(tmp, 'cmbrepresentation', 'fullfast');
  
  cfg=[];
  cfg.method='coh';
  cfg.complex='imag';
  sconnT=ft_connectivityanalysis(cfg, tmp);
  
  if iT == 1
    sconn = sconnT;
    
    sconn = rmfield(sconn, 'cfg');
  else
    sconn.cohspctrm(:,:,:,iT) = sconnT.cohspctrm;
    sconn.time(iT) = sconnT.time;
    
  end
  
  clear tmp
end

kden = .3;
N = size(sconn.cohspctrm,1);

% n of edges
K = kden * ((N^2-N)/2);

% here be lazy and take threshold from average (with real data do at least
% freq wise)
mean_con = squeeze(mean(mean(sconn.cohspctrm,3),4));

sorted_data = sort(mean_con(:),1, 'descend');
thresh = sorted_data(ceil(K*2));

sconn.adjmatspctrm = double(sconn.cohspctrm > thresh);

measureArray = {'density' 'efficiency' 'clustering' ...
  'disconnection' 'charpath'};
% loop thru
for i = 1:numel(measureArray)
  measure = measureArray{i};
  
  cfg = [];
  cfg.measure = measure;
  gm_global{i} = obob_gm2_global_measure(cfg, sconn); 
  gm_global{i}.(measure) = squeeze(gm_global{i}.(measure));
end

% local....
measureArray = {'degrees' 'efficiency' 'clustering' 'betweenness'};

% loop thru (be aware that this can take quite long!!!)
for i = 1:numel(measureArray)
  measure = measureArray{i};

  cfg = [];
  cfg.measure = measure;
  if strcmp(measure, 'efficiency')
    cfg.frequency = [10 10];
    cfg.latency = [.09 .11];
  end %if
  gm_local{i} = obob_gm2_local_measure(cfg, sconn);
end

testCase.assertEqual(gm_local, std.gm_local, 'RelTo', 1e-13);
testCase.assertEqual(gm_global, std.gm_global, 'RelTo', 1e-13);

end

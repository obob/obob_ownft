classdef testOBOB_slurm < matlab.unittest.TestCase
  %TESTOBOB_WHOLE_FLOW Summary of this class goes here
  %   Detailed explanation goes here
  
  properties
    old_warning
    tmp_folder
    tmp_folder_jobs
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
      
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
      testCase.tmp_folder = tempname();
      testCase.tmp_folder_jobs = tempname();
      
      mkdir(testCase.tmp_folder);
      mkdir(testCase.tmp_folder_jobs);
      
    end %function
  end %methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
      
      rmdir(testCase.tmp_folder, 's');
      rmdir(testCase.tmp_folder_jobs, 's');
    end %function
  end %methods
  
  methods(Test)
    function test_slurm(testCase)
      addpath('slurm_jobs');
      % set variables...
      argument1 = {1, 3, 6, 7};
      argument2 = {2, 3};
      
      % prepare submit
      cfg = [];
      cfg.mem = '200M';
      cfg.jobsdir = testCase.tmp_folder_jobs;
      cfg.do_submit = false;
      
      slurm_struct = obob_slurm_create(cfg);
      
      slurm_struct = obob_slurm_addjob_cell(slurm_struct, 'slurm_test_job', testCase.tmp_folder, argument1, argument2);
      
      % submit
      obob_slurm_submit(slurm_struct);
      
      all_mat_files = dir(fullfile(testCase.tmp_folder_jobs, '001', 'slurm', '*.mat'));
      
      testCase.assertEqual(length(all_mat_files), 8);
      
      for idx_file = 1:8
        testCase.assertTrue(strcmp(all_mat_files(idx_file).name, sprintf('job%d.mat', idx_file)));
      end %for
      
      tmp_job = slurm_test_job();
      
      f_name = tmp_job.get_output_filename(testCase.tmp_folder, argument1{1}, argument2{1});
      
      save(f_name, 'argument1');
      
      cfg = [];
      cfg.mem = '200M';
      cfg.jobsdir = testCase.tmp_folder_jobs;
      cfg.do_submit = false;
      
      slurm_struct = obob_slurm_create(cfg);
      
      slurm_struct = obob_slurm_addjob_cell(slurm_struct, 'slurm_test_job', testCase.tmp_folder, argument1, argument2);
      
      % submit
      obob_slurm_submit(slurm_struct);
      
      all_mat_files = dir(fullfile(testCase.tmp_folder_jobs, '002', 'slurm', '*.mat'));
      
      testCase.assertEqual(length(all_mat_files), 7);
      
      for idx_file = 1:7
        testCase.assertTrue(strcmp(all_mat_files(idx_file).name, sprintf('job%d.mat', idx_file)));
      end %for
      
      disp('hi');
      
    end %function
  end %methods
  
end


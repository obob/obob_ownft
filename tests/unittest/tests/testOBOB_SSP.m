function tests = testOBOB_SSP()
% this first function is needed only for matlab. just change its name
% according to the filename in your tests and leave the rest alone.
tests = functiontests(localfunctions);
end
function test_ssp(testCase)

% load data with and without ssp
testfolder = obob_get_testdata_folder();
load(fullfile(testfolder, 'processed_data', 'testSSP.mat'));

% and do the ssp projection on the already averaged data
% Since SSP and averaging are both linear, the order on how I  
% apply them doesn't really matter, so i can take the averaged data to make
% the unitest


% big file as a placeholder, need to get few seconds of another one just
% for getting the ssp projectors 


cfg         = [];
cfg.inputfile = fullfile(testfolder, 'raw', 'shortfile.fif'); %any SBG .fif 2016/2017 would fit
new_data = obob_apply_ssp(cfg,datarandom_std_avg);

old_data = datarandom_std_avg_ssp;

% do the real test
testCase.assertEqual(obob_rm_cfg(new_data), obob_rm_cfg(old_data), 'RelTol', 1e-9);
end %function

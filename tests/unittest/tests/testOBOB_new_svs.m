classdef testOBOB_new_svs < matlab.unittest.TestCase
  %TESTOBOB_NEW_SVS Summary of this class goes here
  %   Detailed explanation goes here
  
  properties(MethodSetupParameter)
    use_parcels = {true, false};
  end %testparameters
  
  properties
    old_warning
    mni_mri
    sim_data
    parcellation
    vol
    grid
    subject_grid_parcels
    mri
    sens
    lf
    lf_parcels
    min_distance_from_center = 0.03;
    fsample = 200;
    ntrials = 100;
    freq = 10;
    cur_channels = 'MEGMAG';
    current_lf
    current_parcels
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.old_warning = warning;
      warning off;
      
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
      testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
      
      load(fullfile(testfolder, 'test_mri_seg.mat'));
      load('mni_mri');
      
      testCase.mni_mri = ft_convert_units(mni_mri, 'm');
      testCase.mri = ft_convert_units(mri, 'm');
      testCase.sens = ft_convert_units(sens, 'm');
      
      headmodel = 'singlesphere';
      cfg = [];
      cfg.method = headmodel;
      
      testCase.vol = ft_prepare_headmodel(cfg, testCase.mri);
      testCase.vol.r = testCase.vol.r - 0.01;
      
      cfg = [];
      cfg.headmodel = testCase.vol;
      cfg.grid.resolution = 0.015;
      cfg.grid.unit = 'm';
      
      testCase.grid = ft_prepare_sourcemodel(cfg);
      
      testCase.grid.inside(rms(testCase.grid.pos - testCase.vol.o, 2) <= testCase.min_distance_from_center) = false;
      testCase.grid.inside(testCase.grid.pos(:, 3) < 34e-3 & testCase.grid.pos(:, 2) < 65e-3) = false;
      testCase.grid.inside(testCase.grid.pos(:, 3) < 40e-3) = false;
      
      cfg = [];
      cfg.grad = testCase.sens;
      cfg.headmodel = testCase.vol;
      cfg.channel = testCase.cur_channels;
      cfg.dip.pos = testCase.grid.pos(min(find(cumsum(testCase.grid.inside) == 1)), :);
      cfg.dip.mom = [1 0 0]';
      cfg.dip.frequency = testCase.freq;
      cfg.dip.phase = 0;
      cfg.dip.amplitude = 10;
      cfg.ntrials = testCase.ntrials;
      cfg.triallength = 1;
      cfg.fsample = testCase.fsample;
      cfg.relnoise = 0.1;
      
      testCase.sim_data = ft_dipolesimulation(cfg);
      
      cfg = [];
      cfg.sourcemodel = testCase.grid;
      cfg.grad = testCase.sens;
      cfg.headmodel = testCase.vol;
      
      testCase.lf = ft_prepare_leadfield(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.resolution = 15e-3;
      
      testCase.parcellation = obob_svs_create_parcellation(cfg);
      
      cfg = [];
      cfg.mri = testCase.mri;
      cfg.grid.warpmni = 'yes';
      cfg.grid.template = testCase.parcellation.template_grid;
      cfg.grid.nonlinear = 'no';
      cfg.grid.unit = 'm';
      
      testCase.subject_grid_parcels = ft_prepare_sourcemodel(cfg);
      
      cfg = [];
      cfg.grid = testCase.subject_grid_parcels;
      cfg.vol = testCase.vol;
      
      testCase.lf_parcels = ft_prepare_leadfield(cfg, testCase.sim_data);
      
      
    end %function
  end %methods
  
  methods(TestMethodSetup)
    function handle_parcels(testCase, use_parcels)
      if use_parcels
        testCase.current_lf = testCase.lf_parcels;
        testCase.current_parcels = testCase.parcellation;
      else
        testCase.current_lf = testCase.lf;
        testCase.current_parcels;
      end %if
    end %function
  end %testsetup methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(Test)
    function test_fail_without_cov(testCase)
      cfg = [];
      
      data = ft_timelockanalysis(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.grid = testCase.current_lf;
      
      testCase.assertError(@() obob_svs_compute_spat_filters(cfg, data), 'obob:svs:nocov');
    end %function
    
    function test_fail_without_lf(testCase)
      cfg = [];
      
      data = ft_timelockanalysis(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.grid = testCase.grid;
      
      testCase.assertError(@() obob_svs_compute_spat_filters(cfg, data), 'obob:svs:nolf');
    end %function
    
    function test_pass_with_good_input(testCase)
      cfg = [];
      cfg.covariance = 'yes';
      
      data = ft_timelockanalysis(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.grid = testCase.current_lf;
      cfg.parcellation = testCase.current_parcels;
      
      filt = obob_svs_compute_spat_filters(cfg, data);
      
      cfg = [];
      cfg.spatial_filter = filt;
      
      source_data = obob_svs_beamtrials_lcmv(cfg, testCase.sim_data);
    end %function
    
    function test_deprecation_warning(testCase)
      cfg = [];
      cfg.grid = testCase.current_lf;
      cfg.hdm = testCase.vol;
      cfg.parcellation = testCase.current_parcels;
      
      testCase.assertError(@() obob_svs_beamtrials_lcmv(cfg, testCase.sim_data), 'obob:svs:deprecated');
      
    end %function
    
    function test_wrong_channels1(testCase)
      cfg = [];
      cfg.channel = 2:length(testCase.sim_data.label);
      
      data_for_cov = ft_selectdata(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.covariance = 'yes';
      
      data = ft_timelockanalysis(cfg, data_for_cov);
      
      cfg = [];
      cfg.grid = testCase.current_lf;
      cfg.parcellation = testCase.current_parcels;
      
      filt = obob_svs_compute_spat_filters(cfg, data);
      
      cfg = [];
      cfg.spatial_filter = filt;
      
      testCase.assertError(@() obob_svs_beamtrials_lcmv(cfg, testCase.sim_data), 'obob:svs:chan_mismatch');
    end %function
    
    function test_wrong_channels2(testCase)
      cfg = [];
      cfg.channel = 2:length(testCase.sim_data.label);
      
      data_for_beam = ft_selectdata(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.covariance = 'yes';
      
      data = ft_timelockanalysis(cfg, testCase.sim_data);
      
      cfg = [];
      cfg.grid = testCase.current_lf;
      cfg.parcellation = testCase.current_parcels;
      
      filt = obob_svs_compute_spat_filters(cfg, data);
      
      cfg = [];
      cfg.spatial_filter = filt;
      
      testCase.assertError(@() obob_svs_beamtrials_lcmv(cfg, data_for_beam), 'obob:svs:chan_mismatch');
    end %function
  end %methods
end


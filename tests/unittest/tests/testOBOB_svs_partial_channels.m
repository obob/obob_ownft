classdef testOBOB_svs_partial_channels < matlab.unittest.TestCase
  %TESTOBOB_WHOLE_FLOW Summary of this class goes here
  %   Detailed explanation goes here
  
  properties(TestParameter)
    channels = {'MEG', 'MEGMAG', 'MEGGRAD'};
    use_prepare_leadfield_channel = {true, false};
  end %testparameters
  
  properties
    old_warning
    mri
    mni_mri
    sens
    template_grid
    grid
    vol
    testfile
    raw_data
    
    compare_data;
  end %properties
  
  methods(TestClassSetup)
    function loadData(testCase)
      testCase.compare_data.MEG.value = 8.4935e-07;
      testCase.compare_data.MEG.idx = 486066;
      testCase.compare_data.MEGMAG.value = 1.65681371314799e-06;
      testCase.compare_data.MEGMAG.idx = 414308;
      testCase.compare_data.MEGGRAD.value = 7.84998829359942e-07;
      testCase.compare_data.MEGGRAD.idx = 521134;
      
      testCase.old_warning = warning;
      warning off;
      
      global ft_default
      ft_default.showcallinfo = 'no';
      ft_default.trackcallinfo = 'no';
      
      testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
      
      load(fullfile(testfolder, 'test_mri_seg.mat'));
      load('mni_mri');
      
      testCase.mni_mri = ft_convert_units(mni_mri, 'm');
      testCase.mri = ft_convert_units(mri, 'm');
      testCase.sens = ft_convert_units(sens, 'm');
      
      headmodel = 'singlesphere';
      cfg = [];
      cfg.method = headmodel;
      
      testCase.vol = ft_prepare_headmodel(cfg, testCase.mri);
      testCase.vol.r = testCase.vol.r - 0.01;
      
      cfg = [];
      cfg.headmodel = testCase.vol;
      cfg.grid.resolution = 0.015;
      cfg.grid.unit = 'm';
      
      testCase.grid = ft_prepare_sourcemodel(cfg);
      
      testCase.testfile = fullfile(obob_get_testdata_folder, 'raw', 'sample.fif');
      
      cfg = [];
      cfg.dataset = testCase.testfile;
      cfg.trialdef.eventtype = 'Trigger';
      cfg.trialdef.eventvalue = [16 8 24 4 20 12 28 2];
      cfg.trialdef.prestim = 0.5;
      cfg.trialdef.poststim = 2;
      
      cfg = ft_definetrial(cfg);
      
      testCase.raw_data = ft_preprocessing(cfg);
      testCase.raw_data.grad = ft_convert_units(testCase.raw_data.grad, 'm');
      testCase.grid = ft_convert_units(testCase.grid, 'm');
      testCase.vol = ft_convert_units(testCase.vol, 'm');
      
    end %function
  end %methods
  
  methods(TestClassTeardown)
    function restore_warning(testCase)
      warning(testCase.old_warning);
    end %function
  end %methods
  
  methods(Test)
    function test_partial_svs(testCase, channels, use_prepare_leadfield_channel)
      
      cfg = [];
      cfg.channel = {channels};
      
      data = ft_selectdata(cfg, testCase.raw_data);
      
      cfg = [];
      cfg.grid = testCase.grid;
      cfg.vol = testCase.vol;
      if use_prepare_leadfield_channel
        cfg.channel = {channels};
      end %if
      
      lf = ft_prepare_leadfield(cfg, data);
      
      cfg = [];
      cfg.covariance = 'yes';
      
      data_cov = ft_timelockanalysis(cfg, data);
      
      cfg = [];
      cfg.grid = lf;
      cfg.regfac = '5%';
      
      spatial_filter = obob_svs_compute_spat_filters(cfg, data_cov);
      
      cfg = [];
      cfg.spatial_filter = spatial_filter;
      
      data_svs = obob_svs_beamtrials_lcmv(cfg, data);
      
      all_data = data_svs.trial{:};
      all_data = all_data(:);
      
      [value, idx] = max(all_data);
      
      testCase.assertEqual(idx, testCase.compare_data.(channels).idx);
      testCase.assertEqual(value, testCase.compare_data.(channels).value, 'RelTol', 5e-3);
      
      
    end %function
  end %methods
  
end


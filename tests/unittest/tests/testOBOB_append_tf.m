function tests = testOBOB_append_tf()
tests = functiontests(localfunctions);
end

function test_append_tf(testCase)
%% set vars...
foi = 2:50;
foi_parts = {2:25, 26:50};
toi = 0:.05:2;
toi_parts = {toi(1:20), toi(21:end)};

methods = {'mtmconvol', 'wavelet', 'mtmfft'};
keeptrials = {'yes', 'no'};
output = {'fourier', 'pow', 'powandcsd'};
%% create a signal....
cfg = [];
cfg.method = 'broadband';
cfg.fsample = 100;
cfg.trllen = 2;
cfg.numtrl = 50;

cfg.n1.ampl = 1;
cfg.n1.bpfreq = [5 10];
cfg.n2.ampl = 2;
cfg.n2.bpfreq = [12 20];
cfg.noise.ampl = 3;

data = ft_freqsimulation(cfg);

%% do whole tf analysis...
for idx_method = 1:length(methods)
  for idx_keeptrials = 1:length(keeptrials)
    for idx_output = 1:length(output)
      if strcmp(keeptrials{idx_keeptrials}, 'no') && strcmp(output{idx_output}, 'fourier')
        continue;
      end %if
      cfg = [];
      cfg.method = methods{idx_method};
      cfg.foi = foi;
      cfg.toi = toi;
      cfg.tapsmofrq = ones(size(cfg.foi))*6;
      cfg.t_ftimwin = ones(size(cfg.foi))*.4;
      cfg.output = output{idx_output};
      cfg.keeptrials = keeptrials{idx_keeptrials};
      
      if strcmp(output{idx_output}, 'fourier')
        cfg.keeptapers = cfg.keeptrials;
      end %if

      data_tf_whole = ft_freqanalysis(cfg, data);

      data_tf_parts = {};

      for i = 1:length(foi_parts)
        for j = 1:length(toi_parts)
          cfg.foi = foi_parts{i};
          cfg.toi = toi_parts{j};
          cfg.tapsmofrq = ones(size(cfg.foi))*6;
          cfg.t_ftimwin = ones(size(cfg.foi))*.4;

          data_tf_parts{end+1} = ft_freqanalysis(cfg, data);
        end %for
      end %for

      data_tf_allparts = obob_append_tf(data_tf_parts{:});

      data_tf_whole = rmfield(data_tf_whole, 'cfg');

      testCase.assertEqual(data_tf_allparts, data_tf_whole)
      
    end %for
  end %for
end %for

end

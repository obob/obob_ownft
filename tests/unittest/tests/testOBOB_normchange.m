function tests = testOBOB_normchange()
tests = functiontests(localfunctions);
end

function test_normchange(testCase)
testfolder = fullfile(obob_get_testdata_folder(), 'processed_data');
load(fullfile(testfolder, 'normchange_data.mat'));
%%
cfg=[];
cfg.method='montecarlo';
cfg.statistic='obob_statfun_normchange';
cfg.clusterthreshold = 'nonparametric_common';
cfg.design=[ones(1,length(allLHit)), ones(1,length(allLHit))*2; 1:length(allLHit), 1:length(allLHit)];
cfg.ivar=1;
cfg.uvar=2;
cfg.numrandomization=10;
cfg.latency=[-.6 0];
cfg.computecritval= 'no';
cfg.lognorm='yes';


statL=ft_freqstatistics(cfg, allLHit{:}, allLMiss{:});

testCase.assertEqual(statL.stat, statL_orig.stat);


cfg=[];
cfg.method='montecarlo';
cfg.statistic='obob_statfun_dbchange';
cfg.clusterthreshold = 'nonparametric_common';
cfg.design=[ones(1,length(allLHit)), ones(1,length(allLHit))*2; 1:length(allLHit), 1:length(allLHit)];
cfg.ivar=1;
cfg.uvar=2;
cfg.numrandomization=10;
cfg.latency=[-.6 0];
cfg.computecritval= 'no';


statL_db=ft_freqstatistics(cfg, allLHit{:}, allLMiss{:});

testCase.assertEqual(statL_db.stat, statL_db_orig.stat);
end
